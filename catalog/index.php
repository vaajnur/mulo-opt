<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Купить активы для косметики: пептиды, гиалуроновая кислота, аргирелин, гидролизаты, коллаген.");
$APPLICATION->SetTitle("Каталог товаров");
?><?$APPLICATION->IncludeComponent(
	"bitrix:catalog",
	".default",
	Array(
		"ACTION_VARIABLE" => "action",
		"ADD_ELEMENT_CHAIN" => "Y",
		"ADD_PICT_PROP" => "MORE_PHOTO",
		"ADD_PROPERTIES_TO_BASKET" => "Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"ADD_SECTION_CHAIN" => "Y",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "N",
		"ALSO_BUY_ELEMENT_COUNT" => "4",
		"ALSO_BUY_MIN_BUYES" => "1",
		"BASKET_URL" => "/personal/cart/",
		"BIG_DATA_RCM_TYPE" => "any",
		"CACHE_FILTER" => "Y",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "2592000",
		"CACHE_TYPE" => "Y",
		"COMMON_ADD_TO_BASKET_ACTION" => "ADD",
		"COMMON_SHOW_CLOSE_POPUP" => "N",
		"COMPATIBLE_MODE" => "Y",
		"COMPONENT_TEMPLATE" => ".default",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO",
		"CONVERT_CURRENCY" => "Y",
		"CURRENCY_ID" => "UAH",
		"DETAIL_ADD_DETAIL_TO_SLIDER" => "N",
		"DETAIL_ADD_TO_BASKET_ACTION" => array(0=>"BUY",),
		"DETAIL_BACKGROUND_IMAGE" => "-",
		"DETAIL_BLOG_EMAIL_NOTIFY" => "N",
		"DETAIL_BLOG_URL" => "catalog_comments",
		"DETAIL_BLOG_USE" => "Y",
		"DETAIL_BRAND_PROP_CODE" => array(0=>"",1=>"BRAND_REF",2=>"",),
		"DETAIL_BRAND_USE" => "Y",
		"DETAIL_BROWSER_TITLE" => "-",
		"DETAIL_CHECK_SECTION_ID_VARIABLE" => "N",
		"DETAIL_DETAIL_PICTURE_MODE" => "IMG",
		"DETAIL_DISPLAY_NAME" => "Y",
		"DETAIL_DISPLAY_PREVIEW_TEXT_MODE" => "E",
		"DETAIL_FB_APP_ID" => "",
		"DETAIL_FB_USE" => "Y",
		"DETAIL_META_DESCRIPTION" => "-",
		"DETAIL_META_KEYWORDS" => "-",
		"DETAIL_OFFERS_FIELD_CODE" => array(0=>"NAME",1=>"TAGS",2=>"PREVIEW_PICTURE",3=>"",),
		"DETAIL_OFFERS_PROPERTY_CODE" => array(0=>"",1=>"COLOR",2=>"SIZE_CLOTHES",3=>"COLOR_REF",4=>"MATERIAL",5=>"MORE_PHOTO",6=>"ARTNUMBER",7=>"SIZES_SHOES",8=>"SIZES_CLOTHES",9=>"",),
		"DETAIL_PROPERTY_CODE" => array(0=>"ATT_BRAND",1=>"PRODUCT_DAY",2=>"PRESERVATIVE",3=>"COLLECTION",4=>"HLB",5=>"INCI",6=>"PH",7=>"ACTIVE_INGREDIENTS",8=>"AROMA",9=>"BRAND",10=>"WEIGHT_SOAP",11=>"FORM_WEIGHT",12=>"VIEW",13=>"PROTEIN_TYPE",14=>"TYPE_EMULSION",15=>"VIDEO",16=>"APPEARANCE",17=>"OUTER_O2_DIAMETER",18=>"ATTENTION",19=>"VISCOSITY",20=>"GLB",21=>"BEST_BEFORE",22=>"DIAMETER",23=>"DOSAGE",24=>"OTHER_NAMES",25=>"INSTRUCTIONS",26=>"USING",27=>"USED_PART_MATERIALS",28=>"QUALITY",29=>"NUMBER_SET",30=>"QUANTITY_SHEET",31=>"QUANTITY_TABLETS",32=>"SET",33=>"CONCENTRATION",34=>"MATERIAL",35=>"METHOD_PREPARATION",36=>"MOLECULAR_MASS",37=>"NAZNAZHENIE",38=>"NAME",39=>"APPLICATION_AREA",40=>"AMOUNT",41=>"BULK",42=>"MFD",43=>"DESCRIPTIO_SMELL",44=>"FEATURES",45=>"DENSITY",46=>"INDICATIONS_APPLICATION",47=>"PROVIDE",48=>"INTENDED_USE",49=>"ADVANTAGES",50=>"APPLICATION",51=>"INPUT_PERCENTAGE",52=>"SIZE_IN_DIAMETER",53=>"SIZE_FINISHED_PRODUCT",54=>"GRANULES_SIZE",55=>"SIZE_STICKER",56=>"PACTICLE_SIZE",57=>"DIMENSIONS",58=>"SOLUBILITY",59=>"RECOMMENDATION_INPUT",60=>"RECOMMENDATION_OF_USE",61=>"PROPERTIES",62=>"SYNONYMS",63=>"COMPATIBILITY",64=>"COMPOSITION",65=>"METHOD_OF_OBTAINING",66=>"MODE_OF_APPLICATION",67=>"SHELF_LIFE",68=>"RAW_MATERIALS",69=>"ROOM_TEMPERATURE",70=>"MELTING_TEMPERATURE",71=>"TEMPERATURE_CONDITIONS",72=>"THERMAL_STABILITY",73=>"TYPE",74=>"SKIN_TYPE",75=>"SAFETY_REQUIREMENTS",76=>"PACKAGING",77=>"STORAGE_CONDITIONS",78=>"FACTOR_PROTECTION",79=>"PACKING",80=>"FORM",81=>"FORMAT",82=>"FORMULA",83=>"STORAGE",84=>"NUMBER_OF_WASHING",85=>"DELIVERY",86=>"CML2_ARTICLE",87=>"USER_ID",88=>"BLOG_POST_ID",89=>"BLOG_COMMENTS_CNT",90=>"VOTE_COUNT",91=>"OFFERS",92=>"SHOW_MENU",93=>"SIMILAR_PRODUCT",94=>"RATING",95=>"RELATED_PRODUCT",96=>"VOTE_SUM",97=>"TIMER_START_DATE",98=>"TIMER_LOOP",99=>"TIMER_DATE",100=>"TIME_OF_TANGING",101=>"INITIAL_RAW_MATERIALS",102=>"CALORIUM_PRODUCT",103=>"EDUCATION_OF_GEL",104=>"SAPONIFICATION",105=>"FOOD_DOSAGE",106=>"APPROXIMATE_WEIGHT_CHOCOLATE",107=>"HARDNESS",108=>"TYPE_PROTECTION_FACTOR",109=>"STICKER_FORM",110=>"COLOR_FASTNESS",111=>"NUMBER_GELLING",112=>"ENERGY_VALUE",113=>"ENERGY_PORTION",114=>"PICKUP",115=>"SKU_COLOR",116=>"TOTAL_OUTPUT_POWER",117=>"PROTEIN",118=>"WEIGHT",119=>"VID_ZASTECHKI",120=>"VID_SUMKI",121=>"TASTE",122=>"BOTTLE_HEIGHT",123=>"VYSOTA_RUCHEK",124=>"HEIGHT_OF_THE_FORM",125=>"WARRANTY",126=>"PLUGS",127=>"IMPACT_ASSESSMENT",128=>"ACID_NUMBER",129=>"OTSEKOV",130=>"CONVECTION",131=>"LINEAR_SHRINK",132=>"MAX_TEMPERATURE",133=>"MARKING",134=>"MASS_WATER_SHARE",135=>"NATURAL_SUPPLEMENT",136=>"NOTE",137=>"RESTRICTIONS_USE",138=>"OLYFACTOR_GROUP",139=>"BASIS",140=>"THE_OCEAN",141=>"CLEANING",142=>"FOOD_SUPPLEMENT",143=>"DENSITY_PLASTIC",144=>"PODKLADKA",145=>"COATING",146=>"MIX_PROPORTIONS",147=>"REGION_OF_ORIGIN",148=>"SEASON",149=>"CERTIFICATE",150=>"SECTION",151=>"FORCE",152=>"RATIO_COMPOSITION",153=>"COMPARISON_WEIGHT_COMPONENTS",154=>"REF",155=>"STABILITY",156=>"RESISTANCE",157=>"COUNTRY_BRAND",158=>"THEMES",159=>"PLASTIC_HOLDING_TEMPERATURE",160=>"HAIR_TYPE",161=>"ODOR_TYPE",162=>"EXTENSION_RUPTURE",163=>"DESCRIPTION_AND_SERT",164=>"ZOOM2",165=>"BATTERY_LIFE",166=>"SWITCH",167=>"GRAF_PROC",168=>"LENGTH_OF_CORD",169=>"DISPLAY",170=>"LOADING_LAUNDRY",171=>"FULL_HD_VIDEO_RECORD",172=>"INTERFACE",173=>"COMPRESSORS",174=>"Number_of_Outlets",175=>"MAX_RESOLUTION_VIDEO",176=>"MAX_BUS_FREQUENCY",177=>"MAX_RESOLUTION",178=>"FREEZER",179=>"POWER_SUB",180=>"POWER",181=>"HARD_DRIVE_SPACE",182=>"MEMORY",183=>"OS",184=>"ZOOM",185=>"PAPER_FEED",186=>"SUPPORTED_STANDARTS",187=>"VIDEO_FORMAT",188=>"SUPPORT_2SIM",189=>"MP3",190=>"ETHERNET_PORTS",191=>"MATRIX",192=>"CAMERA",193=>"PHOTOSENSITIVITY",194=>"DEFROST",195=>"SPEED_WIFI",196=>"SPIN_SPEED",197=>"PRINT_SPEED",198=>"SOCKET",199=>"IMAGE_STABILIZER",200=>"GSM",201=>"SIM",202=>"MEMORY_CARD",203=>"TYPE_BODY",204=>"TYPE_MOUSE",205=>"TYPE_PRINT",206=>"CONNECTION",207=>"TYPE_OF_CONTROL",208=>"TYPE_DISPLAY",209=>"TYPE2",210=>"REFRESH_RATE",211=>"RANGE",212=>"AMOUNT_MEMORY",213=>"MEMORY_CAPACITY",214=>"VIDEO_BRAND",215=>"DIAGONAL",216=>"RESOLUTION",217=>"TOUCH",218=>"CORES",219=>"LINE_PROC",220=>"PROCESSOR",221=>"CLOCK_SPEED",222=>"TYPE_PROCESSOR",223=>"PROCESSOR_SPEED",224=>"HARD_DRIVE",225=>"HARD_DRIVE_TYPE",226=>"Number_of_memory_slots",227=>"MAXIMUM_MEMORY_FREQUENCY",228=>"TYPE_MEMORY",229=>"BLUETOOTH",230=>"FM",231=>"GPS",232=>"HDMI",233=>"SMART_TV",234=>"USB",235=>"WIFI",236=>"FLASH",237=>"ROTARY_DISPLAY",238=>"SUPPORT_3D",239=>"SUPPORT_3G",240=>"WITH_COOLER",241=>"FINGERPRINT",242=>"PROFILE",243=>"GAS_CONTROL",244=>"GRILL",245=>"GENRE",246=>"INTAKE_POWER",247=>"SURFACE_COATING",248=>"brand_tyres",249=>"DUST_COLLECTION",250=>"DRYING",251=>"REMOVABLE_TOP_COVER",252=>"CONTROL",253=>"FINE_FILTER",254=>"FORM_FAKTOR",255=>"HTML",256=>"199",257=>"ATT_BRAND2",258=>"NEWPRODUCT",259=>"MANUFACTURER",260=>"DETAIL_TEXT_UA",261=>"PREVIEW_TEXT_UA"),
		"DETAIL_SET_CANONICAL_URL" => "N",
		"DETAIL_SET_VIEWED_IN_COMPONENT" => "N",
		"DETAIL_SHOW_BASIS_PRICE" => "Y",
		"DETAIL_SHOW_MAX_QUANTITY" => "N",
		"DETAIL_STRICT_SECTION_CHECK" => "N",
		"DETAIL_USE_COMMENTS" => "Y",
		"DETAIL_USE_VOTE_RATING" => "Y",
		"DETAIL_VK_USE" => "N",
		"DETAIL_VOTE_DISPLAY_AS_RATING" => "rating",
		"DISABLE_INIT_JS_IN_COMPONENT" => "Y",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_CHEAPER" => "N",
		"DISPLAY_OFFERS_TABLE" => "N",
		"DISPLAY_TOP_PAGER" => "N",
		"ELEMENT_SORT_FIELD" => "sort",
		"ELEMENT_SORT_FIELD2" => "name",
		"ELEMENT_SORT_ORDER" => "asc",
		"ELEMENT_SORT_ORDER2" => "desc",
		"FIELDS" => array(0=>"TITLE",1=>"ADDRESS",2=>"DESCRIPTION",3=>"PHONE",4=>"SCHEDULE",5=>"EMAIL",6=>"IMAGE_ID",7=>"COORDINATES",8=>"",),
		"FILE" => "",
		"FILE_404" => "",
		"FILTER_FIELD_CODE" => array(0=>"TAGS",1=>"PREVIEW_PICTURE",2=>"",),
		"FILTER_NAME" => "",
		"FILTER_OFFERS_FIELD_CODE" => array(0=>"TAGS",1=>"PREVIEW_PICTURE",2=>"DETAIL_PICTURE",3=>"",),
		"FILTER_OFFERS_PROPERTY_CODE" => array(0=>"",1=>"COLOR",2=>"SIZE_CLOTHES",3=>"MATERIAL",4=>"",),
		"FILTER_PRICE_CODE" => array(0=>"BASE",),
		"FILTER_PROPERTY_CODE" => array(0=>"",1=>"",),
		"FILTER_VIEW_MODE" => "VERTICAL",
		"FORUM_ID" => "1",
		"GIFTS_DETAIL_BLOCK_TITLE" => "Выберите один из подарков",
		"GIFTS_DETAIL_HIDE_BLOCK_TITLE" => "N",
		"GIFTS_DETAIL_PAGE_ELEMENT_COUNT" => "4",
		"GIFTS_DETAIL_TEXT_LABEL_GIFT" => "Подарок",
		"GIFTS_MAIN_PRODUCT_DETAIL_BLOCK_TITLE" => "Выберите один из товаров, чтобы получить подарок",
		"GIFTS_MAIN_PRODUCT_DETAIL_HIDE_BLOCK_TITLE" => "N",
		"GIFTS_MAIN_PRODUCT_DETAIL_PAGE_ELEMENT_COUNT" => "4",
		"GIFTS_MESS_BTN_BUY" => "Выбрать",
		"GIFTS_SECTION_LIST_BLOCK_TITLE" => "Подарки к товарам этого раздела",
		"GIFTS_SECTION_LIST_HIDE_BLOCK_TITLE" => "N",
		"GIFTS_SECTION_LIST_PAGE_ELEMENT_COUNT" => "4",
		"GIFTS_SECTION_LIST_TEXT_LABEL_GIFT" => "Подарок",
		"GIFTS_SHOW_DISCOUNT_PERCENT" => "Y",
		"GIFTS_SHOW_IMAGE" => "Y",
		"GIFTS_SHOW_NAME" => "Y",
		"GIFTS_SHOW_OLD_PRICE" => "Y",
		"HIDE_AVAILABLE_TAB" => "N",
		"HIDE_DELIVERY_CALC" => "N",
		"HIDE_MEASURES" => "N",
		"HIDE_NOT_AVAILABLE" => "L",
		"HIDE_NOT_AVAILABLE_OFFERS" => "N",
		"IBLOCK_ID" => "10",
		"IBLOCK_TYPE" => "catalog",
		"INCLUDE_SUBSECTIONS" => "N",
		"LABEL_PROP" => "-",
		"LINE_ELEMENT_COUNT" => "3",
		"LINK_ELEMENTS_URL" => "link.php?PARENT_ELEMENT_ID=#ELEMENT_ID#",
		"LINK_IBLOCK_ID" => "",
		"LINK_IBLOCK_TYPE" => "",
		"LINK_PROPERTY_SID" => "",
		"LIST_BROWSER_TITLE" => "-",
		"LIST_META_DESCRIPTION" => "-",
		"LIST_META_KEYWORDS" => "-",
		"LIST_OFFERS_FIELD_CODE" => array(0=>"NAME",1=>"TAGS",2=>"PREVIEW_PICTURE",3=>"DETAIL_PICTURE",4=>"",),
		"LIST_OFFERS_LIMIT" => "0",
		"LIST_OFFERS_PROPERTY_CODE" => array(0=>"",1=>"COLOR",2=>"SIZE_CLOTHES",3=>"SHIRINA_SHINY",4=>"PROFILE",5=>"Diameter",6=>"MATERIAL",7=>"MORE_PHOTO",8=>"SIZES_SHOES",9=>"SIZES_CLOTHES",10=>"ARTNUMBER",11=>"",),
		"LIST_PROPERTY_CODE" => array(0=>"ATT_BRAND",1=>"COLLECTION",2=>"VIDEO",3=>"BULK",4=>"TYPE",5=>"DELIVERY",6=>"CML2_ARTICLE",7=>"OFFERS",8=>"SKU_COLOR",9=>"CONVECTION",10=>"COUNTRY_BRAND",11=>"brand_tyres",12=>"HTML",13=>"199",14=>"ATT_BRAND2",15=>"NEWPRODUCT",16=>"SALELEADER",17=>"SPECIALOFFER",18=>"",),
		"MAIN_TITLE" => "Наличие на складах",
		"MESSAGES_PER_PAGE" => "10",
		"MESSAGE_404" => "",
		"MESS_BTN_ADD_TO_BASKET" => "В корзину",
		"MESS_BTN_BUY" => "Купить",
		"MESS_BTN_COMPARE" => "Сравнение",
		"MESS_BTN_DETAIL" => "Подробнее",
		"MESS_NOT_AVAILABLE" => "Нет в наличии",
		"MIN_AMOUNT" => "10",
		"OFFERS_CART_PROPERTIES" => array(),
		"OFFERS_SORT_FIELD" => "sort",
		"OFFERS_SORT_FIELD2" => "id",
		"OFFERS_SORT_ORDER" => "asc",
		"OFFERS_SORT_ORDER2" => "desc",
		"OFFERS_TABLE_DISPLAY_PICTURE_COLUMN" => "Y",
		"OFFERS_TABLE_PAGER_COUNT" => "10",
		"OFFER_ADD_PICT_PROP" => "MORE_PHOTO",
		"OFFER_TREE_PROPS" => array(0=>"COLOR",1=>"SIZE_CLOTHES",2=>"MATERIAL",),
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "0",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => "round",
		"PAGER_TITLE" => "Товары",
		"PAGE_ELEMENT_COUNT" => "30",
		"PARTIAL_PRODUCT_PROPERTIES" => "N",
		"PATH_TO_SMILE" => "/bitrix/images/forum/smile/",
		"PRICE_CODE" => array(0=>"BASE",),
		"PRICE_VAT_INCLUDE" => "Y",
		"PRICE_VAT_SHOW_VALUE" => "N",
		"PRODUCT_DISPLAY_MODE" => "Y",
		"PRODUCT_ID_VARIABLE" => "id",
		"PRODUCT_PROPERTIES" => array(0=>"PRODUCT_DAY",1=>"VIDEO",2=>"OTHER_NAMES",3=>"DIMENSIONS",4=>"USER_ID",5=>"OFFERS",6=>"SHOW_MENU",7=>"SIMILAR_PRODUCT",8=>"RELATED_PRODUCT",),
		"PRODUCT_PROPS_VARIABLE" => "prop",
		"PRODUCT_QUANTITY_VARIABLE" => "quantity",
		"QUANTITY_FLOAT" => "N",
		"REVIEW_AJAX_POST" => "Y",
		"REVIEW_IBLOCK_ID" => "7",
		"REVIEW_IBLOCK_TYPE" => "1c_catalog",
		"SALE_IBLOCK_ID" => "",
		"SALE_IBLOCK_TYPE" => "catalog",
		"SECTIONS_SHOW_PARENT_NAME" => "N",
		"SECTIONS_VIEW_MODE" => "TEXT",
		"SECTION_ADD_TO_BASKET_ACTION" => "ADD",
		"SECTION_BACKGROUND_IMAGE" => "-",
		"SECTION_COUNT_ELEMENTS" => "Y",
		"SECTION_ID_VARIABLE" => "SECTION_ID",
		"SECTION_TOP_DEPTH" => "4",
		"SEF_FOLDER" => "/",
		"SEF_MODE" => "Y",
		"SEF_URL_TEMPLATES" => array("sections"=>"","section"=>"#SECTION_CODE_PATH#/","element"=>"#ELEMENT_CODE#.html","compare"=>"compare/","smart_filter"=>"#SECTION_CODE_PATH#/filter/#SMART_FILTER_PATH#/apply/",),
		"SERVICES_IBLOCK_ID" => "",
		"SERVICES_IBLOCK_TYPE" => "catalog",
		"SET_LAST_MODIFIED" => "N",
		"SET_STATUS_404" => "Y",
		"SET_TITLE" => "Y",
		"SHOW_404" => "Y",
		"SHOW_DEACTIVATED" => "Y",
		"SHOW_DISCOUNT_PERCENT" => "Y",
		"SHOW_EMPTY_STORE" => "Y",
		"SHOW_GENERAL_STORE_INFORMATION" => "N",
		"SHOW_LINK_TO_FORUM" => "N",
		"SHOW_OLD_PRICE" => "Y",
		"SHOW_PRICE_COUNT" => "1",
		"SHOW_SECTION_BANNER" => "N",
		"SHOW_SERVICES" => "N",
		"SHOW_TOP_ELEMENTS" => "N",
		"STORES" => array(0=>"1",),
		"STORE_PATH" => "/store/#store_id#",
		"TEMPLATE_THEME" => "site",
		"TOP_ADD_TO_BASKET_ACTION" => "ADD",
		"URL_TEMPLATES_READ" => "",
		"USER_FIELDS" => array(0=>"",1=>"",),
		"USE_ALSO_BUY" => "N",
		"USE_BIG_DATA" => "Y",
		"USE_CAPTCHA" => "N",
		"USE_COMMON_SETTINGS_BASKET_POPUP" => "N",
		"USE_COMPARE" => "N",
		"USE_ELEMENT_COUNTER" => "Y",
		"USE_FILTER" => "Y",
		"USE_GIFTS_DETAIL" => "N",
		"USE_GIFTS_MAIN_PR_SECTION_LIST" => "N",
		"USE_GIFTS_SECTION" => "N",
		"USE_MAIN_ELEMENT_SECTION" => "N",
		"USE_MIN_AMOUNT" => "N",
		"USE_PRICE_COUNT" => "N",
		"USE_PRODUCT_QUANTITY" => "Y",
		"USE_REVIEW" => "Y",
		"USE_SALE_BESTSELLERS" => "Y",
		"USE_STORE" => "N",
		"USE_STORE_PHONE" => "Y",
		"USE_STORE_SCHEDULE" => "Y"
	),
false,
Array(
	'ACTIVE_COMPONENT' => 'Y'
)
);?>&nbsp;<br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>