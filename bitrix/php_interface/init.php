<?
// const
if (file_exists(__DIR__ . "/include/constants.php")) {
    require_once(__DIR__ . "/include/constants.php");
}

// custom function
if (file_exists(__DIR__ . "/include/function.php")) {
    require_once(__DIR__ . "/include/function.php");
}

// handlers
if (file_exists(__DIR__ . "/handlers/handlers.php")) {

    require_once(__DIR__ . "/handlers/handlers.php");
}

AddEventHandler("sale", "OnOrderRemindSendEmail", "MailRemindPay");
function MailRemindPay($ID, &$eventName, &$arFields){
  
    $arOrder = CSaleOrder::GetByID($ID);
// в этом массив разместим те id  платежных систем при которых необходимо отсылать сообщение пользователю
    $paySystem = array('9');
    if(!in_array($arOrder['PAY_SYSTEM_ID'] ,$paySystem)) {
  
        return false;    
    }      
}

include_once($_SERVER['DOCUMENT_ROOT']."/bitrix/modules/wsrubi.smtp/classes/general/wsrubismtp.php");

function deb($a){
    echo "<pre>";
    print_r($a);
    echo "</pre>";
}

function writeInfoToLog($info = array())
{
    if (!empty($info['LOG_FILE'])) {
        $debugFile = $info['LOG_FILE'];
        unset($info['LOG_FILE']);
    } else {
        if (!defined('LOG_ORDERS_PATH')) return false;

        $debugFile = LOG_ORDERS_PATH;
    }

    if (!empty($debugFile) && count($info)) {
        //помещаем файл в папку /upload/script_log/ и следим чтоб файл не раздувался
        //deb($debugFile);
        $fileName = basename($debugFile);
        //deb($fileName);
        //из файла log_bp.txt получим имя и расширение
        $tmpArr = explode(".", $fileName);
        //deb($tmpArr);
        if (!is_dir($_SERVER['DOCUMENT_ROOT'] . "/upload/script_log/")) {
            mkdir($_SERVER['DOCUMENT_ROOT'] . "/upload/script_log/");
        }
        $logFile = $_SERVER['DOCUMENT_ROOT'] . "/upload/script_log/" . $fileName;
        //deb($logFile);

        if (file_exists($logFile)) {

            $fileSize = filesize($logFile);
            if ($fileSize > 2 * 1024 * 1024) {
                rename($logFile, $_SERVER['DOCUMENT_ROOT'] . "/upload/script_log/" . $tmpArr[0] . "_" . date("d_m_Y-H_i_s") . "." . $tmpArr[1]);
            }
        }
        //$mode = "r+"; //пишем в начало файла
        $mode = "a+"; //пишем в конец файла
        //если файла нет - создаем его
        if (!file_exists($logFile)) file_put_contents($logFile, '');

        if (isset($info['invert'])) {
            //if ($info['invert'] == 1) $mode = "a+"; //пишем в конец файла
            unset($info['invert']);
        }

        $text = getStringFromArray($info);
        //Открыть для чтения и записи; поместить указатель в конец файла. Если файл не
        // существует, делается попытка создать его.
        $fp = fopen($logFile, $mode);
        fwrite($fp, $text);
        fclose($fp);

    }
}

function getStringFromArray(array $info)
{
    $text = "\r\n\r\n======= Массив с данными " . date("d.m.Y H:i.s") . " =======";
    foreach ($info as $key => $val) {
        //$text .= "\r\n$key => $val";
        if (is_array($val)) {
            $text .= "\r\n   $key";
            foreach ($val as $key2 => $val2) {
                $text .= "\r\n$key2 => $val2";
                if (is_array($val2)) {
                    foreach ($val2 as $key3 => $val3) {
                        $text .= "\r\n$key3 => $val3";
                        if (is_array($val3)) {
                            foreach ($val3 as $key4 => $val4) {
                                $text .= "\r\n$key4 => $val4";
                                if (is_array($val4)) {
                                    foreach ($val4 as $key5 => $val5) {
                                        $text .= "\r\n$key5 => $val5";
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } else {
            $text .= "\r\n$key => $val";
        }
    }
    return $text;
}

function custom_mail($to,$subject,$body,$headers) {
$f=fopen($_SERVER["DOCUMENT_ROOT"]."/maillog.txt", "a+");
fwrite($f, print_r(array('TO' => $to, 'SUBJECT' => $subject, 'BODY' => $body, 'HEADERS' => $headers),1)."\n========\n");
fclose($f);
return mail($to,$subject,$body,$headers);
}

function getFinalPriceInCurrency($item_id, $cnt = 1, $getName="N", $sale_currency = 'UAH') {
    CModule::IncludeModule("iblock");
    CModule::IncludeModule("catalog");
    CModule::IncludeModule("sale");
    global $USER;

    // Проверяем, имеет ли товар торговые предложения?
    if(CCatalogSku::IsExistOffers($item_id)) {

        // Пытаемся найти цену среди торговых предложений
        $res = CIBlockElement::GetByID($item_id);

        if($ar_res = $res->GetNext()) {
            $productName = $ar_res["NAME"];
            if(isset($ar_res['IBLOCK_ID']) && $ar_res['IBLOCK_ID']) {

                // Ищем все тогровые предложения
                $offers = CIBlockPriceTools::GetOffersArray(array(
                    'IBLOCK_ID' => $ar_res['IBLOCK_ID'],
                    'HIDE_NOT_AVAILABLE' => 'Y',
                    'CHECK_PERMISSIONS' => 'Y'
                ), array($item_id), null, null, null, null, null, null, array('CURRENCY_ID' => $sale_currency), $USER->getId(), null);

                foreach($offers as $offer) {

                    $price = CCatalogProduct::GetOptimalPrice($offer['ID'], $cnt, array(0 => '2'), 'N');
                    if(isset($price['PRICE'])) {

                        $final_price = $price['PRICE']['PRICE'];
                        $currency_code = $price['PRICE']['CURRENCY'];

                        // Ищем скидки и высчитываем стоимость с учетом найденных
                        $arDiscounts = CCatalogDiscount::GetDiscountByProduct($item_id, array(0 => '2'), "N");
                        if(is_array($arDiscounts) && sizeof($arDiscounts) > 0) {
                            $final_price = CCatalogProduct::CountPriceWithDiscount($final_price, $currency_code, $arDiscounts);
                        }

                        // Конец цикла, используем найденные значения
                        break;
                    }

                }
            }
        }

    } else {

        // Простой товар, без торговых предложений (для количества равному $cnt)
        $price = CCatalogProduct::GetOptimalPrice($item_id, $cnt, array(0 => '2'), 'N');

        // Получили цену?
        if(!$price || !isset($price['PRICE'])) {
            return false;
        }

        // Меняем код валюты, если нашли
        if(isset($price['CURRENCY'])) {
            $currency_code = $price['CURRENCY'];
        }
        if(isset($price['PRICE']['CURRENCY'])) {
            $currency_code = $price['PRICE']['CURRENCY'];
        }

        // Получаем итоговую цену
        $final_price = $price['PRICE']['PRICE'];

        // Ищем скидки и пересчитываем цену товара с их учетом
        $arDiscounts = CCatalogDiscount::GetDiscountByProduct($item_id, array(0 => '2'), "N", 2, 's2');

        if(is_array($arDiscounts) && sizeof($arDiscounts) > 0) {
            $final_price = CCatalogProduct::CountPriceWithDiscount($final_price, $currency_code, $arDiscounts);
        }

        if($getName=="Y"){
            $res = CIBlockElement::GetByID($item_id);
            $ar_res = $res->GetNext();
            $productName = $ar_res["NAME"];
        }

    }

    // Если необходимо, конвертируем в нужную валюту
    if($currency_code != $sale_currency) {
        $final_price = CCurrencyRates::ConvertCurrency($final_price, $currency_code, $sale_currency);
    }

    $arRes = array(
        "PRICE"=>CurrencyFormat($price['PRICE']['PRICE'], 'UAH'),
        "FINAL_PRICE"=>CurrencyFormat($final_price, 'UAH'),
        "CURRENCY"=>$sale_currency,
        "DISCOUNT"=>$arDiscounts,
    );

    if($productName!="")
        $arRes['NAME']= $productName;

    return $arRes;

}
// добавляем обработчик события при регистрации
AddEventHandler("main", "OnBeforeUserRegister", "OnBeforeUserRegisterHandler");
AddEventHandler("main", "OnBeforeUserUpdate", "OnBeforeUserRegisterHandler");

// описываем саму функцию
function OnBeforeUserRegisterHandler($args)
{
	if (strstr($args['NAME'], '//') || strstr($args['NAME'], 'https:') || strstr($args['NAME'], 'http:') || strstr($args['NAME'], 'оплатил') || strstr($args['NAME'], '$$$') || strstr($args['NAME'], 'BONUS') ||  strstr($args['LAST_NAME'], 'https:') || strstr($args['LAST_NAME'], '//') || strstr($args['LAST_NAME'], 'http:') || strstr($args['LAST_NAME'], 'купит') || strstr($args['LAST_NAME'], 'казино') || strstr($args['NAME'], 'казино') || strstr($args['LOGIN'], '//') || strstr($args['LOGIN'], 'https:') || strstr($args['LOGIN'], 'http:') || strstr($args['NAME'], 'ремонт') || strstr($args['LAST_NAME'], 'ремонт') || strstr($args['NAME'], 'Займы') || strstr($args['LAST_NAME'], 'Займы')) {
		$GLOBALS['APPLICATION']->ThrowException('Ошибка. Обратитесь к администратору!');
		return false;
	}
	return true;
}
?>