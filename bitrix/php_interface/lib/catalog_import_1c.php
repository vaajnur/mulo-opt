<?

class catalogImport1c
{
    protected static $handlerDisallow = false;

    public static function loger($arRes){

        $log_file = $_SERVER['DOCUMENT_ROOT'] . "/log_update.txt";
        $handle = fopen($log_file,"a+");

        fwrite($handle,$arRes['XML_ID'] . "\n");
        fclose($handle);

    }

    public function updateElementCode(&$arFields)
    {

        if (self::$handlerDisallow) {
            return;
        }

        if ($arFields['IBLOCK_ID'] == IBLOCK_ID_1C_CATALOG) {

//                self::loger($arFields);

                $getElemData = \Bitrix\Iblock\ElementTable::getList(array(
                    'select' => array('ID', 'XML_ID'),
                    'filter' => array('IBLOCK_ID' => IBLOCK_ID_SOAP_CATALOG, 'NAME' => $arFields["NAME"]),
                ))->Fetch();

                if (!empty($getElemData)) {

                    if ($getElemData['XML_ID'] != $arFields['XML_ID']) {

                        self::$handlerDisallow = true;

                        $el = new CIBlockElement();
                        $el->Update($getElemData['ID'], array('XML_ID' => $arFields['XML_ID']));

                        self::$handlerDisallow = false;

                    }

                }

        }


    }

}
