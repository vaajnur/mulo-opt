<?
if (!function_exists('pr')) {
    function pr($o, $showEveryone = false)
    {
        global $USER;
        if (!$USER->IsAdmin() and !$showEveryone) {
            return;
        }
        echo '<pre style="font-size: 10pt; background-color: #fff; color: #000; margin: 10px; padding: 10px; border: 1px solid red; text-align: left; max-width: 800px; max-height: 600px; overflow: scroll">';
        echo htmlspecialcharsEx(print_r($o, true));
        echo '</pre>';
    }
}
if(!function_exists('getar')) {
    function getar($ar)
    {
        //global $USER;
        //if ($USER->GetID() == 1) {
            echo '<script type="text/javascript">console.log(' . json_encode($ar) . ');</script>';
        //}
    }
}