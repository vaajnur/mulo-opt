<?
use Bitrix\Main\EventManager;
$handler = EventManager::getInstance();

// update elemet xml_id
$handler->addEventHandler(
    "iblock",
    "OnAfterIBlockElementUpdate ",
    array(
        "catalogImport1c",
        "updateElementCode"
    ),
    dirname(__DIR__) . "/lib/catalog_import_1c.php"
);
$handler->addEventHandler(
    "iblock",
    "OnAfterIBlockElementAdd",
    array(
        "catalogImport1c",
        "updateElementCode"
    ),
    dirname(__DIR__) . "/lib/catalog_import_1c.php"
);


/*AddEventHandler('catalog', 'OnSuccessCatalogImport1C', 'OnSuccessCatalogImport1CHandler');
function OnSuccessCatalogImport1CHandler($arParams, $fileName) {
    copy($fileName,str_replace('1c_catalog','1c_catalog_debug',$fileName));

}*/

//отправляем письмо админу о выполнении заказа
/*
внимание, чтобы всё работало правлен ещё файл:
/bitrix/modules/api.orderstatus/tools/send_message.php - чтобы передавался статус $arOrderFields['STATUS_MESSAGE_ID'] = $STATUS_ID; (если сообщение передано до изменения статуса заказа)
 */
AddEventHandler('main', 'OnBeforeEventSend', "my_OnBeforeEventSend");
function my_OnBeforeEventSend(&$arFields, &$arTemplate){
    if( $arTemplate['EVENT_NAME'] == 'API_ORDERSTATUS' && $arFields['STATUS_MESSAGE_ID'] == 'F' ){
        $arFields['EMAIL'] .= ','.$arFields['SITE_EMAIL'];
    }
}

AddEventHandler("search", "BeforeIndex", "BeforeIndexHandler");
// ������� ���������� ������� "BeforeIndex"
function BeforeIndexHandler($arFields)
{
    if (!CModule::IncludeModule("iblock")) // ���������� ������
        return $arFields;
    if ($arFields["MODULE_ID"] == "iblock") {
        $db_props = CIBlockElement::GetProperty(                        // �������� �������� �������������� ��������
            $arFields["PARAM2"],         // BLOCK_ID �������������� ��������
            $arFields["ITEM_ID"],          // ID �������������� ��������
            array("sort" => "asc"),       // ���������� (����� ��������)
            Array("CODE" => "CML2_ARTICLE")); // CODE �������� (� ������ ������ �������)
        if ($ar_props = $db_props->Fetch())
            $arFields["TITLE"] .= " " . $ar_props["VALUE"];   // ������� �������� � ����� ��������� �������������� ��������
    }
    return $arFields; // ����� ���������
}

AddEventHandler("iblock", "OnBeforeIBlockElementUpdate", "OnBeforeIBlockElementUpdateHandler");

//перезаписываем поля в b_sale_basket
function OnBeforeIBlockElementUpdateHandler(&$arFields)
{
    if( $arFields['IBLOCK_ID'] == 10 && $arFields['ID'] > 0 ){
        $obj = CIBlockElement::GetList(
            array(),
            array('IBLOCK_ID'=>$arFields['IBLOCK_ID'],'ID'=>$arFields['ID']),
            false,
            false,
            array('XML_ID')
        );
        $res = $obj->Fetch();
        $xml_id_old = $res['XML_ID'];
        $xml_id_new = $arFields['XML_ID'];

        if( $xml_id_new != $xml_id_old ){
            //перезаписываем значения ячеек в таблице b_sale_basket
            global $DB;
            $strSql = "UPDATE b_sale_basket SET PRODUCT_XML_ID = \"".$xml_id_new."\" WHERE PRODUCT_XML_ID = \"".$xml_id_old."\"";
            $DB->Query($strSql);
        }
    }
}

//отмена алертов в админке
AddEventHandler("main", "OnAdminTabControlBegin", "MyOnAdminTabControlBegin");
function MyOnAdminTabControlBegin(&$form){
    global $APPLICATION;
    if( $APPLICATION->GetCurPage() == '/bitrix/admin/sale_order_view.php' ){
        $APPLICATION->AddHeadString('<script>window.alert = function(){};</script>',true);
    }
}



AddEventHandler("catalog", "OnBeforeProductUpdate", "OnBeforeProductUpdateHandler");
function OnBeforeProductUpdateHandler($id, $arFields) {
	global $DB;
	$arMail = array();
	
	// Если кол-во товара больше 0
	if ($arFields["QUANTITY"] > 0) {
		
		// Есть ли подписчики
		$tableName = \Bitrix\Catalog\SubscribeTable::getTableName();
		$results = $DB->Query("SELECT `USER_CONTACT` FROM `" . $tableName . "` WHERE `ITEM_ID`='" . $arFields["ID"] . "'");
		while ($row = $results->Fetch()) {
			$arMail[] = $row["USER_CONTACT"];
		}
		
		// Если нет подписчиков, не продолжаем
		if (empty($arMail))
			return true;
		
		// Смотрим, сколько товара в наличии было до обновления
		CModule::IncludeModule('iblock');
		$objElement = CIblockElement::GetList(
			array(), array("ID" => $arFields["ID"]), false, false, 
			array("CATALOG_GROUP_" . PRICE_BASE_ID, "DETAIL_PAGE_URL", "NAME")
		);
		$arElement = $objElement->GetNext();
		$arElement["PAGE_URL"] = "https://" . $_SERVER["SERVER_NAME"] . $arElement["DETAIL_PAGE_URL"];
		
		// Если ранее было больше 0, не продолжаем
		if ((integer)$arElement["CATALOG_QUANTITY"] > 0)
			return true;
		
		// Отправим уведомления всем подписавшимся пользователям
		foreach ($arMail as $mail) {
			Bitrix\Main\Mail\Event::send(array(
				"EVENT_NAME" => "SALE_SUBSCRIBE_PRODUCT",
				"LID" => "s2",
				"C_FIELDS" => array(
					"EMAIL" => $mail,
					"NAME" => $arElement["NAME"],
					"PAGE_URL" => $arElement["PAGE_URL"]
				),
			));
		}
	}
	return true;
}
