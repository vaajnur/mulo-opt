<?
$MESS ["FAST_VIEW_HEADING"] = " Швидкий перегляд";
	$MESS ["FAST_VIEW_PRODUCT_PROPERTIES_HEADING"] = " Характеристики";
	$MESS ["FAST_VIEW_PRODUCT_MORE_LINK"] = " більше інформації про товар";
	$MESS ["FAST_VIEW_SKU_PROPERTIES_TITLE"] = " інші варіанти товару";
	$MESS ["FAST_VIEW_REVIEWS_COUNT"] = " відгуків";
	$MESS ["FAST_VIEW_ADDCART_LABEL"] = " у кошик";
	$MESS ["FAST_VIEW_FASTBACK_LABEL"] = " купити в 1 клік";
	$MESS ["FAST_VIEW_WISHLIST_LABEL"] = " у вибране";
	$MESS ["FAST_VIEW_COMPARE_LABEL"] = " до порівняння";
	$MESS ["FAST_VIEW_AVAILABLE"] = " в наявності";
	$MESS ["FAST_VIEW_NOAVAILABLE"] = " Недоступно";
	$MESS ["FAST_VIEW_NO_AVAILABLE"] = " Недоступно";
	$MESS ["FAST_VIEW_ON_ORDER"] = " під замовлення";
	$MESS ["FAST_VIEW_ADDCART_LABEL"] = " у кошик";
	$MESS ["FAST_VIEW_ARTICLE_LABEL"] = " Артикул:";
	$MESS ["FAST_VIEW_OLD_PRICE_LABEL"] = " Стара ціна:";
	$MESS ["FAST_VIEW_REQUEST_PRICE_LABEL"] = " Ціна за запитом";
	$MESS ["FAST_VIEW_REQUEST_PRICE_BUTTON_LABEL"] = " запитати ціну";
	$MESS ["FAST_VIEW_DESCRIPTION_TITLE"] = " опис товару";
	$MESS ["FAST_VIEW_REVIEWS_LABEL"] = " Рейтинг";
	$MESS ["FAST_VIEW_QUANTITY_LABEL"] = " кількість:";
	$MESS ["FAST_VIEW_TIMER_DAY_LABEL"] = " днів";
	$MESS ["FAST_VIEW_TIMER_HOUR_LABEL"] = " годин";
	$MESS ["FAST_VIEW_TIMER_MINUTE_LABEL"] = " хвилин";
	$MESS ["FAST_VIEW_TIMER_SECOND_LABEL"] = " Секунд";
	$MESS ["FAST_VIEW_SUBSCRIBE_LABEL"] = " підписатися";