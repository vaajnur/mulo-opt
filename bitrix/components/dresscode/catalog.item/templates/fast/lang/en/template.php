<?
$MESS ["FAST_VIEW_HEADING"] = " Quick view";
$MESS["FAST_VIEW_PRODUCT_PROPERTIES_HEADING"] = "Characteristics";
$MESS ["FAST_VIEW_PRODUCT_MORE_LINK"] = " More product information";
$MESS["FAST_VIEW_SKU_PROPERTIES_TITLE"] = "Other product options";
$MESS["FAST_VIEW_REVIEWS_COUNT"] = "Reviews";
$MESS["FAST_VIEW_ADDCART_LABEL"] = "Add to cart";
$MESS ["FAST_VIEW_FASTBACK_LABEL"] = " Buy in 1 click";
$MESS["FAST_VIEW_WISHLIST_LABEL"] = "To favorites";
$MESS ["FAST_VIEW_COMPARE_LABEL"] = " To compare";
$MESS["FAST_VIEW_AVAILABLE"] = "In stock";
$MESS["FAST_VIEW_NOAVAILABLE"] = "Not available";
$MESS["FAST_VIEW_NO_AVAILABLE"] = "Not available";
$MESS ["FAST_VIEW_ON_ORDER"] = " Custom made";
$MESS["FAST_VIEW_ADDCART_LABEL"] = "Add to cart";
$MESS ["FAST_VIEW_ARTICLE_LABEL"] = " SKU:";
$MESS ["FAST_VIEW_OLD_PRICE_LABEL"] = " Old price:";
$MESS ["FAST_VIEW_REQUEST_PRICE_LABEL"] = "Price on request";
$MESS["FAST_VIEW_REQUEST_PRICE_BUTTON_LABEL"] = "Request a price";
$MESS["FAST_VIEW_DESCRIPTION_TITLE"] = "Product Description";
$MESS["FAST_VIEW_REVIEWS_LABEL"] = "Rating";
$MESS ["FAST_VIEW_QUANTITY_LABEL"] = " Qty:";
$MESS ["FAST_VIEW_TIMER_DAY_LABEL"] = "Days";
$MESS["FAST_VIEW_TIMER_HOUR_LABEL"] = "Hours";
$MESS ["FAST_VIEW_TIMER_MINUTE_LABEL"] = " Minutes";
$MESS ["FAST_VIEW_TIMER_SECOND_LABEL"] = " Seconds";
$MESS ["FAST_VIEW_SUBSCRIBE_LABEL"] = "Subscribe";
?>