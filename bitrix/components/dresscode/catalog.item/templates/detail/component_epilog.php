<?$APPLICATION->SetAdditionalCSS($templateFolder."/css/review.css");?>
<?$APPLICATION->SetAdditionalCSS($templateFolder."/css/media.css");?>
<?$APPLICATION->SetAdditionalCSS($templateFolder."/css/set.css");?>

<?$APPLICATION->AddHeadScript($templateFolder."/js/morePicturesCarousel.js");?>
<?$APPLICATION->AddHeadScript($templateFolder."/js/pictureSlider.js");?>
<?$APPLICATION->AddHeadScript($templateFolder."/js/zoomer.js");?>
<?$APPLICATION->AddHeadScript($templateFolder."/js/plus.js");?>
<?$APPLICATION->AddHeadScript($templateFolder."/js/tabs.js");?>
<?$APPLICATION->AddHeadScript($templateFolder."/js/sku.js");?>
<?
// getar($arResult['PROPERTIES']['NAMEUA']);
getar($arResult['SECTION']);
// метатеги для мультиязыка
if(LANGUAGE_ID != 'ru'){
	$postfix = LANGUAGE_ID == 'ua' ? 'UA' : (LANGUAGE_ID == 'en' ? 'EN' : '');
	// pr($arResult['PROPERTIES']);
	//  ======================== TITLE
	if($arResult['PROPERTIES']['META_TITLE_'.$postfix]['VALUE']['TEXT'] != false){
		$APPLICATION->SetPageProperty("title", $arResult['PROPERTIES']['META_TITLE_'.$postfix]['VALUE']['TEXT']);
	}elseif($arResult['PROPERTIES']['NAME'.$postfix] != false){
		if(LANGUAGE_ID == 'ua')
			$APPLICATION->SetPageProperty("title", $arResult['PROPERTIES']['NAME'.$postfix]['VALUE'] . ', ціна, відгуки, властивості.');
		if(LANGUAGE_ID == 'en')
			$APPLICATION->SetPageProperty("title", $arResult['PROPERTIES']['NAME'.$postfix]['VALUE'] . ', price, reviews, properties.');		
	}

	//  ======================== KEYWORDS
	if($arResult['PROPERTIES']['META_KEYWORDS_'.$postfix]['VALUE']['TEXT'] != false)
		$APPLICATION->SetPageProperty("keywords", $arResult['PROPERTIES']['META_KEYWORDS_'.$postfix]['VALUE']['TEXT']);

	//  ======================== DESCRIPTION
	if($arResult['PROPERTIES']['META_DESCRIPTION_'.$postfix]['VALUE']['TEXT'] != false){
		$APPLICATION->SetPageProperty("description", $arResult['PROPERTIES']['META_DESCRIPTION_'.$postfix]['VALUE']['TEXT']);		
	}elseif($arResult['PROPERTIES']['NAME'.$postfix] != false){
		if(LANGUAGE_ID == 'ua')
			$APPLICATION->SetPageProperty("description", 'Купити '.$arResult['PROPERTIES']['NAME'.$postfix]['VALUE']. ' ' . $arResult['SECTION']['NAME'] . ', вигідна ціна, опис характеристик.');			
		if(LANGUAGE_ID == 'en')
			$APPLICATION->SetPageProperty("description", 'Buy '.$arResult['PROPERTIES']['NAME'.$postfix]['VALUE']. ' ' . $arResult['SECTION']['NAME'] . ',  price, characteristics.');		
	}

}
?>