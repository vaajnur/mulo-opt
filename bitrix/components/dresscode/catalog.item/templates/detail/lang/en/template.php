<?

$MESS["ADDCART_LABEL"] = "Add to cart";
$MESS ["ADDCART_PRED"] = " Pre-order";
$MESS ["FASTBACK_LABEL"] = " Buy in 1 click";
$MESS["WISHLIST_LABEL"] = "To favorites";
$MESS["DELIVERY_LABEL"] = "Calculate delivery";
$MESS ["BIND_ACTION_LABEL"] = " Product participates in the promotion:";
$MESS ["COMPARE_LABEL"] = " To compare";
$MESS["AVAILABLE"] = "In stock";
$MESS["NO_AVAILABLE"] = "Not available";
$MESS ["CATALOG_NO_AVAILABLE"] = "Not available";
$MESS ["ON_ORDER"] = " Under order";
$MESS ["CATALOG_ART_LABEL"] = " SKU: ";
$MESS["CATALOG_PRICE_LABEL"] = "Price: ";
$MESS["CATALOG_ELEMENT_BACK"] = "Go back to the section";
$MESS ["CATALOG_ELEMENT_OVERVIEW"] = "Product Overview";
$MESS["CATALOG_ELEMENT_SET"] = "Set";
$MESS ["CATALOG_ELEMENT_COMPLECT"] = " Package";
$MESS["CATALOG_ELEMENT_REVIEW"] = "Reviews";
$MESS ["CATALOG_ELEMENT_ACCEESSORIES"] = " Related products";
$MESS["CATALOG_ELEMENT_SIMILAR"] = "Other options";
$MESS["CATALOG_ELEMENT_AVAILABILITY"] = "Availability";
$MESS["CATALOG_ELEMENT_DESCRIPTION"] = "Description";
$MESS["CATALOG_ELEMENT_CHARACTERISTICS"] = "Characteristics";
$MESS["CATALOG_ELEMENT_VIDEO"] = "Video";
$MESS["CATALOG_ELEMENT_FILES"] = "Documents";
$MESS["CATALOG_ELEMENT_CHARACTERISTICS_SHORT"] = "Characteristics: ";
$MESS["CATALOG_ELEMENT_MORE_PROPERTIES"] = "All features";
$MESS["CATALOG_ELEMENT_PREVIEW_TEXT_LABEL"] = "Product Description: ";
$MESS["CATALOG_ELEMENT_PREVIEW_TEXT_HEADING"] = "Product Description";
$MESS["CATALOG_ELEMENT_DETAIL_TEXT_HEADING"] = "Product Description";
$MESS["CATALOG_ELEMENT_ZOOM"] = "Zoom in";
$MESS["SKU_VARIANT_LABEL"] = "Other product options: ";
$MESS ["OLD_PRICE_LABEL"] = " Old price:";
$MESS ["OLD_PRICE_DIFFERENCE_LABEL"] = " Savings:";
$MESS["SPECS"] = "Characteristics:";
$MESS ["OTHERITEMS"] = "Other products";
$MESS ["CHARACTERISTICS"] = " Other";
$MESS ["ELEMENT_COMPLECT_HEADING"] = " Set composition";
$MESS["CATALOG_ELEMENT_COMPLECT_PRICE_RESULT"] = " Kit cost:";
$MESS["CATALOG_ELEMENT_COMPLECT_ECONOMY"] = "Your savings:";
$MESS["READ_MORE"] = "Read more";
$MESS["USERRATINGS"] = "User rating:";
$MESS["DIGNIFIED"] = "Advantages:";
$MESS ["FAULT"] = "Disadvantages:";
$MESS ["IMPRESSION"] = "General impressions:";
$MESS["EXPERIENCE"] = "User experience:";
$MESS["REVIEWSUSEFUL"] = "Is the review useful?";
$MESS ["YES"] = "Yes";
$MESS ["NO"] = " No";
$MESS ["SHOWALLREVIEWS"] = " Show all reviews";
$MESS["ADDAREVIEW"] = "Add a review";
$MESS["YOURRATING"] = "Your score:";
$MESS ["YOURREVIEW"] = "Your review:";
$MESS ["INTRODUCEYOURSELF"] = " Introduce yourself:";
$MESS ["SENDFEEDBACK"] = "Send feedback";
$MESS["REVIEW"] = "Reviews";
$MESS ["REVIEW_OK"] = "Your review was published on the site or is being moderated.";
$MESS ["REVIEW_BAD"] = " Sorry, you can't leave more than one review for the product.";
$MESS ["ERROR"] = "Error";
$MESS["CLOSE"] = "Close window";
$MESS ["ZOOM"] = "Zoom in";
$MESS["PRICE"] = "Price:";
$MESS ["RATING"] = "Rating:";
$MESS ["RATING_PRODUCT"] = "Product rating:";
$MESS ["REVIEWS_COUNT"] = "Reviews: ";
$MESS ["REVIEWS_HIDE"] = " Hide reviews";
$MESS ["REVIEWS_SHOW"] = " Show all reviews";
$MESS["REVIEWS_DATE"] = "Date:";
$MESS ["REVIEWS_AUTHOR"] = " Author:";
$MESS["REVIEW"] = "Review";
$MESS["REVIEWS_ADD"] = "Add a review";
$MESS["FILES_HEADING"] = "Files";
$MESS["VIDEO_HEADING"] = "Video";
$MESS ["REQUEST_PRICE_LABEL"] = "Price on request";
$MESS["REQUEST_PRICE_BUTTON_LABEL"] = "Request a price";
$MESS ["SHARE_LABEL"] = " Share";
$MESS["CHEAPER_LABEL"] = "Found cheaper";
$MESS ["QUANTITY_LABEL"] = " Qty:";
$MESS ["TIMER_DAY_LABEL"] = "Days";
$MESS["TIMER_HOUR_LABEL"] = "Hours";
$MESS ["TIMER_MINUTE_LABEL"] = " Minutes";
$MESS ["TIMER_SECOND_LABEL"] = "Seconds";
$MESS ["ELEMENT_SKU_OFFERS_TABLE_HEADING"] = "Other product options";
$MESS["ELEMENT_SKU_OFFERS_TABLE_TITLE"] = "Name";
$MESS ["ELEMENT_SKU_OFFERS_TABLE_PROPERTY"] = " Property";
$MESS ["ELEMENT_SKU_OFFERS_TABLE_PRICE"] = " Cost";
$MESS["ELEMENT_SKU_OFFERS_TABLE_QUANTITY"] = "Availability";
$MESS["ELEMENT_SKU_OFFERS_TABLE_BASKET"] = "Add to cart";
$MESS["SUBSCRIBE_LABEL"] = "Out of stock, report an appearance";
$MESS["PRODUCT_WISH_LIST_TITLE"] = "Add to Favorites";
$MESS["PRODUCT_COMPARE_TITLE"] = "Add to compare";
?>