	//global function
	var startElementTabs;
	
	$(function(){

		//vars
		var $window = $(window);
		var arCoordinates;
		var	$mainScrollObj;
		var	$tabElements;
		var	$tabs;

		//after page load
		$(window).on("load", function(){

			var scrollToElement = function(event){
				var $this = $(this).parents(".tab");
				var toElementID = $this.data("id");

				if(toElementID){

					$tabElements.removeClass("active");

					$mainScrollObj.stop().animate({
						scrollTop: getElementOffset("#" + toElementID) + "px"
					}, 250);

					return event.preventDefault($this.addClass("active"));
				}
			};

			var calcCloseElement = function(coordinate){
				var copyCoordinate = [];

				$.each(arCoordinates, function(i, val) {
					if(coordinate <= val){
						copyCoordinate.push({
							id: i, value: val
						})
					}
				});

				copyCoordinate.sort(function(obj1, obj2) {
				  if (obj1.value < obj2.value) return -1;
				  if (obj1.value > obj2.value) return 1;
				  return 0;
				});

				return copyCoordinate[0];
			};

			var scrollControl = function(event){
				var curScrollValueY = (event.currentTarget.scrollY) ? event.currentTarget.scrollY : $(window).scrollTop()
				var arCurrentTab = calcCloseElement(curScrollValueY);
				if(arCurrentTab != undefined){
					$tabElements.removeClass("active");
					$tabElements.filter('[data-id="' + arCurrentTab["id"] + '"]').addClass("active");
				}
			};

			var getElementOffset = function(getElement){

				//get jquery element
				var $curElement = $(getElement);

				//get element offset
				var elementOffset = $curElement.offset();

				//if not empty
				if(typeof(elementOffset) == "object"){
					return elementOffset.top;
				}
				
			}
			
			startElementTabs = function(){

				$tabs = $("#catalogElement .tabs");
				$tabElements = $tabs.find('.tab:not(".disabled")');
				$mainScrollObj = $("html, body");
				arCoordinates = {};

				$tabElements.each(function(i, nextElement){
					var $nextElement = $(nextElement);
					if($nextElement.data("id")){
						arCoordinates[$nextElement.data("id")] = getElementOffset("#" + $nextElement.data("id"));
					}
				});

			}

			//calc tabs
			startElementTabs();

			$(document).on("click", "#elementNavigation .tab a, #elementSmallNavigation .tab a", scrollToElement);
			$(window).on("scroll", scrollControl);

		});
	});
/*function addLink() {
    var body_element = document.getElementsByTagName('detailText')[0];
    var selection;
    selection = window.getSelection();
    var pagelink = "<br><br> Права на текст принадлежат мыло-опт.com.ua : " + document.location.href + "";
    var copytext = selection + pagelink;
    var newdiv = document.createElement('div');
    newdiv.style.position = 'absolute';
    newdiv.style.left = '-99999px';
    body_element.appendChild(newdiv);
    newdiv.innerHTML = copytext;
    selection.selectAllChildren(newdiv);
    window.setTimeout(function () {
        body_element.removeChild(newdiv);
    }, 0);
}
document.oncopy = addLink;*/


/*function addLink() {
var b_element = document.getElementsByTagName('body')[0];
var selection_txt = window.getSelection();
var to_string = window.getSelection().toString();
var sel_count = to_string.length;
//Здесь находится содержание добавляемого линка
var pagelink = "<br><br> Права на текст принадлежат мыло-опт.com.ua : " + document.location.href + "";
//Максимальное количество символов не вызывающее уведомлений и внедрений ссылки
if (sel_count > 300){
	var copytext = selection_txt + pagelink;
	alert('Вы СКОПИРОВАЛИ КОНТЕНТ данного сайта! Если вы ПУБЛИКУЕТЕ текст для своих читателей - НЕОБХОДИМА ОБРАТНАЯ ССЫЛКА!');
}
else{
	var copytext = selection_txt;
	return true;
}
var newdiv = document.createElement('div');
newdiv.style.position = 'absolute';
newdiv.style.left = '-99999px';
b_element.appendChild(newdiv);
newdiv.innerHTML = copytext;
selection_txt.selectAllChildren(newdiv);
window.setTimeout( function() {
	b_element.removeChild(newdiv);
}, 0);
}*/

/*function addLink() {
    var body_element = document.getElementsByTagName('detailText')[0];
    var selection;
    selection = window.getSelection();
    var pagelink = "<br><br> Права на текст принадлежат мыло-опт.com.ua : " + document.location.href + "";
if (sel_count > 100){
	var copytext = selection_txt + pagelink;
	
}

    var newdiv = document.createElement('div');
    newdiv.style.position = 'absolute';
    newdiv.style.left = '-99999px';
    body_element.appendChild(newdiv);
    newdiv.innerHTML = copytext;
    selection.selectAllChildren(newdiv);
    window.setTimeout(function () {
        body_element.removeChild(newdiv);
    }, 0);
}
document.oncopy = addLink;*/

function addTrans() {
 var d = document.getElementsByTagName('~DETAIL_TEXT')[0];
 var e = window.getSelection();
 var c = e + "";
 var f = "С с Е е Т О о р Р А а Н К Х х В М y З 3".split(/ +/g);
 var g = "C c E e T O o p P A a H K X x B M у 3 З".split(/ +/g);
 var b;
 for (var b = 0; b < f.length; b++) {
 c = c.split(f[b]).join(g[b])
 }
 var a = document.createElement('div');
 a.style.position = 'absolute';
 a.style.left = '-99999px';
 d.appendChild(a);
 a.innerHTML = c;
 e.selectAllChildren(a);
 window.setTimeout(function () {
 d.removeChild(a)
 }, 0)
}
document.oncopy = addTrans;