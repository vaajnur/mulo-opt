<?if(!empty($arResult["PROPERTIES"])):?>
	<?foreach ($arResult["PROPERTIES"] as $index => &$arProp):?>
		<?
			// скрываю св-ва с наличием языкового постфикса и метатеги
        if( (LANGUAGE_ID != 'ru' &&  substr($arProp["CODE"], -2) != strtoupper(LANGUAGE_ID) ) ||  strpos($arProp['CODE'], 'META' ) !== false   ) unset($arResult["PROPERTIES"][$index]);
        if(LANGUAGE_ID == 'ru' && in_array( substr($arProp["CODE"], -2) , ['UA', 'EN']  )  ) unset($arResult["PROPERTIES"][$index]);
		?>
	<?endforeach;?>
<?endif;?>


<?if(!empty($arResult["ITEMS"])):?>
	<?foreach ($arResult["ITEMS"] as &$item):?>
	<?foreach ($item["PROPERTIES"] as $index => &$arProp):?>
		<?
			// скрываю значения св-в с наличием языкового постфикса и метатеги
        if( (LANGUAGE_ID != 'ru' &&  substr($arProp["CODE"], -2) != strtoupper(LANGUAGE_ID) ) ||  strpos($arProp['CODE'], 'META' ) !== false   ) unset($item["PROPERTIES"][$index]);
        if(LANGUAGE_ID == 'ru' && in_array( substr($arProp["CODE"], -2) , ['UA', 'EN']  )  ) unset($item["PROPERTIES"][$index]);
		?>
	<?endforeach;?>
	<?endforeach;?>
<?endif;?>
<?

//getar($arResult["ITEMS"]);