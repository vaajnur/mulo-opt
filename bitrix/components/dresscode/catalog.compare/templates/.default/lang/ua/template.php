<?
	$MESS ["HIDE"] = " приховати характеристики";
	$MESS ["SHOW"] = " повернути приховані характеристики";
	$MESS ["ALLFEATURES"] = " всі характеристики";
	$MESS ["DISTINGUISHED"] = " тільки розрізняються";
	$MESS ["ADDCART"] = " в кошик";
	$MESS ["EMPTY_HEADING"] = " у списку порівняння поки порожньо";
	$MESS ["EMPTY_TEXT"] = 'скористайтеся пошуком або <a href="'.SITE_DIR.'catalog/" >каталогом</a>, виберіть потрібні товари і додайте їх в список порівняння.';
	$MESS ["MAIN_PAGE"] = " Головна сторінка";
	$MESS ["RUB"] = " руб.";
	$MESS ["FROM"] = " від ";
	$MESS ["ADDSKU"] = " в кошик";
	$MESS ["REQUEST_PRICE_LABEL"] = " Ціна за запитом";
	$MESS ["REQUEST_PRICE_BUTTON_LABEL"] = " запитати ціну";

?>