<?
$MESS ["HIDE"] = " Hide characteristics";
	$MESS["SHOW"] = "Return hidden characteristics";
	$MESS["ALLFEATURES"] = "All features";
	$MESS ["DISTINGUISHED"] = " Distinguished only";
	$MESS["ADDCART"] = "Add to cart";
	$MESS ["EMPTY_HEADING"] = " The comparison list is still empty";
	$MESS["EMPTY_TEXT"]      = 'Use the search or <a href="'. SITE_DIR.'catalog/"> catalog</a>, select the desired products and add them to the comparison list.';
	$MESS["MAIN_PAGE"]       = "Home page";
	$MESS["RUB"] = "rub";
$MESS ["FROM"] = "from ";
$MESS ["ADDSKU"] = "Add to cart";
	$MESS["REQUEST_PRICE_LABEL"] = "Price on request";
	$MESS["REQUEST_PRICE_BUTTON_LABEL"] = "Request a price";

?>