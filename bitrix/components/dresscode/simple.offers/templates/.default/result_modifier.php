<?
$postfix = LANGUAGE_ID == 'ua' ? 'UA' : (LANGUAGE_ID == 'en' ? 'EN' : '');	
if(LANGUAGE_ID != 'ru'){
	if(!empty($arResult["MENU_SECTIONS"]) && count($arResult["MENU_SECTIONS"]) > 1):
		foreach ($arResult["MENU_SECTIONS"] as $ic => &$arSection):
			$res1 = ciblocksection::getlist([], ['IBLOCK_ID' => 10, 'ID' => $arSection['ID']], false, ['UF_*']);
			if($ob_sect = $res1->getnext()){
				// getar($ob_sect);
				$arSection["NAME"] = $ob_sect['UF_NAME'.$postfix];
			}

		endforeach;
	endif;
}