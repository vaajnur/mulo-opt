<?
$MESS["NEW_HEADING"] = "New arrivals";
$MESS["ADDCART"] = "Add to cart";
$MESS["ADDSKU"] = "Specify";
$MESS ["ADDCOMPARE"] = " Compare";
$MESS["ADDCOMPARED"] = "In the comparison list";
$MESS["AVAILABLE"] = "In stock";
$MESS["NOAVAILABLE"] = "Not available";
$MESS ["FROM"] = "from ";
$MESS ["ON_ORDER"] = " Under order";
$MESS["GET_ALL_PRODUCT"] = "All offers";
$MESS["ADDCART_LABEL"] = "Add to cart";
$MESS ["FASTBACK_LABEL"] = " Buy in 1 click";
$MESS["WISHLIST_LABEL"] = "To favorites";
$MESS ["COMPARE_LABEL"] = " To compare";

$MESS ["SHOW_MORE"] = " Show more";
$MESS ["SHOWS"] = "Shown";
$MESS ["FROM"] = "from";
$MESS ["CATALOG_ART_LABEL"] = " SKU: ";
$MESS["CATALOG_PRICE_LABEL"] = "Price: ";

$MESS ["EMPTY_HEADING"] = " This section is still empty";
$MESS["EMPTY_TEXT"] = 'You can go back to <a href="'. SITE_DIR.'catalog/">the catalog page</a> or use the navigation or search on the site.';
$MESS["MAIN_PAGE"] = "Home page";
$MESS["TABLE_REQUEST_PRICE_LABEL"] = "Price on request";
$MESS["TABLE_REQUEST_PRICE_BUTTON_LABEL"] = "Request";
?>