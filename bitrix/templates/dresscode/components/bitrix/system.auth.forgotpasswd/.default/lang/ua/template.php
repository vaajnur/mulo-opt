<?
$MESS ['AUTH_FORGOT_PASSWORD_1'] = " якщо ви забули пароль, введіть логін або E-Mail. Контрольний рядок для зміни пароля, а також ваші реєстраційні дані, будуть вислані вам по E-Mail.";
$MESS ['AUTH_GET_CHECK_STRING'] = " надіслати контрольний рядок";
$MESS ['AUTH_SEND'] = " надіслати контрольний рядок";
$MESS ['AUTH_AUTH'] = " Авторизація";
$MESS ["AUTH_LOGIN_EMAIL"] = " логін або E-mail";
$MESS ["AUTH_TITLE"] = " Авторизація";
$MESS ["AUTH_REGISTER"] = " зареєструватися";
$MESS ["AUTH_FORGOT_PASSWORD_2"] = " забули свій пароль?";
$MESS ["FORGOT_TITLE"] = " Забули пароль?";
?>