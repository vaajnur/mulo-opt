<?
$MESS ['AUTH_FORGOT_PASSWORD_1'] = " If you forgot your password, enter your username or E-Mail. The control string for changing the password, as well as your registration data, will be sent to you by E-Mail.";
$MESS ['AUTH_GET_CHECK_STRING'] = " Send the control string";
$MESS ['AUTH_SEND'] = " Send control string";
$MESS ['AUTH_AUTH'] = " Authorization";
$MESS ["AUTH_LOGIN_EMAIL"] = " Login or E-mail";
$MESS ["AUTH_TITLE"] = " Authorization";
$MESS["AUTH_REGISTER"] = "Register";
$MESS["AUTH_FORGOT_PASSWORD_2"] = "Forgot your password?";
$MESS ["FORGOT_TITLE"] = "Forgot your password?";
?>