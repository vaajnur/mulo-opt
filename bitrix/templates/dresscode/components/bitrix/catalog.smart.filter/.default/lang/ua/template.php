<?
$MESS["CT_BCSF_FILTER_TITLE"] = " Фільтр за параметрами";
$MESS["CT_SF_FILTER_FROM"] = " від";
$MESS["CT_SF_FILTER_TO"] = " До";
$MESS["CT_BCSF_SET_FILTER"] = " показати";
$MESS["CT_BCSF_DEL_FILTER"] = " Скинути";
$MESS["CT_SF_FILTER_COUNT"] = " вибрано: #ELEMENT_COUNT#";
$MESS["CT_BCSF_FILTER_SHOW"] = " показати";
$MESS["CT_SF_FILTER_ALL"] = " Все";
$MESS ['CT_SF_FILTER_SHOW_ALL'] = " показати ще";
$MESS ["FILTER_HIDE_ALL"] = " приховати";
$MESS ["FILTER_SHOW_ALL"] = " показати ще";