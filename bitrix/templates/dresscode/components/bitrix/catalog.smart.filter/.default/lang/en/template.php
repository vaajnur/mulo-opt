<?
$MESS ["CT_BCSF_FILTER_TITLE"] = "Filter by parameters";
$MESS ["CT_BCSF_FILTER_FROM"] = "From";
$MESS ["CT_BCSF_FILTER_TO"] = " Until";
$MESS ["CT_BCSF_SET_FILTER"] = " Show";
$MESS ["CT_BCSF_DEL_FILTER"] = " Reset";
$MESS ["CT_BCSF_FILTER_COUNT"] = "Selected: #ELEMENT_COUNT#";
$MESS ["CT_BCSF_FILTER_SHOW"] = " Show";
$MESS ["CT_BCSF_FILTER_ALL"] = "All";
$MESS ['CT_BCSF_FILTER_SHOW_ALL'] = " Show more";
$MESS ["FILTER_HIDE_ALL"] = " Hide";
$MESS ["FILTER_SHOW_ALL"] = "Show more";
?>