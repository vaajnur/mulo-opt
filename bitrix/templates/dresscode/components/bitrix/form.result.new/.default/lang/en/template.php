<?
$MESS ['FORM_REQUIRED_FIELDS'] = "Required Fields";
$MESS ['FORM_APPLY'] = "Apply";
$MESS['FORM_RESET'] = "Reset";
$MESS ['FORM_ADD'] = "Send";
$MESS ['FORM_ACCESS_DENIED'] = "Not enough access rights to the web form.";
$MESS ['FORM_DATA_SAVED1'] = "Thank you! <br> <br> Your application number";
$MESS ['FORM_DATA_SAVED2'] = "accepted.";
$MESS ['FORM_MODULE_NOT_INSTALLED'] = "The web forms module is not installed.";
$MESS ['FORM_NOT_FOUND'] = "The web form was not found.";
$MESS ["WEB_FORM_SENDED_HEADING"] = "Message sent";
$MESS ["WEB_FORM_SENDED_DESCRIPTION"] = "Your message has been successfully sent. Our specialist will contact you shortly";
$MESS ["WEB_FORM_SENDED_CLOSE"] = "Close Window";
$MESS ["PERSONAL_INFO_REQUIRED"] = 'I agree to the <a href="/en/personal-info/" class="pilink"> processing of personal data. </a>';

?>