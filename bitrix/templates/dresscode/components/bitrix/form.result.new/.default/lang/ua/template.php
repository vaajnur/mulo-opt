<?
$MESS['FORM_REQUIRED_FIELDS'] = "Поля, обов'язкові для заповнення";
$MESS['FORM_APPLY'] = "Застосувати";
$MESS['FORM_RESET'] = "Скинувши";
$MESS['FORM_ADD'] = "Відправити";
$MESS['FORM_ACCESS_DENIED'] = "Бракує прав доступу до веб-формі.";
$MESS['FORM_DATA_SAVED1'] = "Спасибі! <br> <br> Ваша заявка №";
$MESS['FORM_DATA_SAVED2'] = "прийнята до розгляду.";
$MESS['FORM_MODULE_NOT_INSTALLED'] = "Модуль веб-форм не встановлено.";
$MESS['FORM_NOT_FOUND'] = "Веб-форма не знайдена.";
$MESS["WEB_FORM_SENDED_HEADING"] = "Повідомлення надіслано";
$MESS["WEB_FORM_SENDED_DESCRIPTION"] = "Ваше повідомлення успішно відправлено. Найближчим часом з Вами зв'яжеться наш фахівець";
$MESS["WEB_FORM_SENDED_CLOSE"] = "Закрити вікно";
$MESS["PERSONAL_INFO_REQUIRED"] = 'Я згоден на <a href="/ua/personal-info/" class="pilink"> обробку персональних даних. </a>';

?>