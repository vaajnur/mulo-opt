<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

/**
 * @var array $arParams
 * @var array $arResult
 * @var SaleOrderAjax $component
 */


// foreach($arResult['JS_DATA']['GRID']['ROWS'][2768671]['columns']['PROPERTY_NAMEUA_VALUE'][0]['value']);
if(LANGUAGE_ID != 'ru'){
	$postfix = LANGUAGE_ID == 'ua' ? 'UA' : (LANGUAGE_ID == 'en' ? 'EN' : '');
	foreach($arResult['JS_DATA']['GRID']['ROWS'] as &$row){
		$row['data']['NAME'] = $row['columns']['PROPERTY_NAME'.$postfix.'_VALUE'][0]['value'];
	}
}

$component = $this->__component;
$component::scaleImages($arResult['JS_DATA'], $arParams['SERVICES_IMAGES_SCALING']);