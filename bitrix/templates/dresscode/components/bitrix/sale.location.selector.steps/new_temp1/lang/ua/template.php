<?
$MESS ["SALE_SLS_EDIT"] = " редагувати";
$MESS ["SALE_SEC_ERROR_OCCURRED"] = " на жаль, сталася Внутрішня помилка";
$MESS ["SALE_XLS_NOTHING_FOUND"] = " не вдалося виявити місце розташування";
$MESS ["SALE_SLS_SELECTOR_PROMPT"] = " виберіть місце розташування ...";
$MESS ["SALE_SLS_OPEN_CLOSE_POPUP"] = " відкрити / закрити";
$MESS ["SALE_SLS_OTHER_CANCEL_SELECTION"] = " скасувати вибір";