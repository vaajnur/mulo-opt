<?
$postfix = LANGUAGE_ID == 'ua' ? 'UA' : (LANGUAGE_ID == 'en' ? 'EN' : '');

foreach($arResult as &$arElement){
	// перевод пунктов меню 
	if(LANGUAGE_ID != 'ru' && !empty($arElement['PARAMS'])){
		$res1 = ciblocksection::getlist([], ['ID' => $arElement['PARAMS']['ID'], 'IBLOCK_ID' => $arElement['PARAMS']['IBLOCK_ID']], false, ["ID","IBLOCK_ID","IBLOCK_SECTION_ID","NAME", 'UF_*']);
		if($name_alt = $res1->fetch()){
			// getar('name_alt');
			// getar($name_alt);
			$arElement["TEXT"] = $name_alt['UF_NAME'.$postfix];
		}
	}
}