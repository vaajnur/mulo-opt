<?
	$MESS["ADDCART"] = "Add to cart";
	$MESS["ADDSKU"] = "Specify";
	$MESS ["ADDCOMPARE"] = " Compare";
	$MESS["ADDCOMPARED"] = "In the comparison list";
	$MESS["AVAILABLE"] = "In stock";
	$MESS["FROM"] = "from ";
$MESS ["NOAVAILABLE"] = "Not available";
$MESS ["ON_ORDER"] = " Under order";
	$MESS["GET_ALL_PRODUCT"] = "All offers";
	$MESS["ADDCART_LABEL"] = "Add to cart";
	$MESS ["FASTBACK_LABEL"] = " Buy in 1 click";
	$MESS["WISHLIST_LABEL"] = "To favorites";
	$MESS ["COMPARE_LABEL"] = " To compare";
	$MESS ["FAST_VIEW_PRODUCT_LABEL"] = " Quick View";
	$MESS["REQUEST_PRICE_LABEL"] = "Price on request";
	$MESS["REQUEST_PRICE_BUTTON_LABEL"] = "Request a price";
	$MESS ["CONVERT_CURRENCY"] = " Show prices in the same currency";
	$MESS ["CURRENCY_ID"] = "Currency";
?>