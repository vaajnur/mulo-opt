<?
$MESS["SEARCH_TEXT"] = "Пошук по каталогу магазину";
$MESS["SEARCH_HEADING"] = "Результати пошуку";
$MESS["SEARCH_ALL_RESULT"] = "Переглянути всі результати";
$MESS["SEARCH_ERROR_FOR_EMPTY_RESULT"] = "По вашому пошуковому запиту нічого не знайдено";
$MESS["SEARCH_CLOSE_BUTTON"] = "Закрити вікно";
?>