<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
if(!empty($arResult["CATEGORIES"])):?>
<table class="title-search-result" style="width:300px;">
		<?foreach($arResult["CATEGORIES"] as $category_id => $arCategory):?>
			<tr>
				<th class="title-search-separator">&nbsp;</th>
				<td class="title-search-separator">&nbsp;</td>
			</tr>
			<?foreach($arCategory["ITEMS"] as $i => $arItem):?>
			<tr>
				<?if($i == 0):?>
				<th style="width:0px;"><?else:?>
<th style="width:0px;"></th>
				<?endif?>

				<?if($category_id === "all"):?>
				<td class="title-search-all"><a href="<?echo $arItem["URL"]?>" style="text-decoration:none; color:#000;"><?echo $arItem["NAME"]?></td>
				<?elseif(isset($arResult["ELEMENTS"][$arItem["ITEM_ID"]])):
					$arElement = $arResult["ELEMENTS"][$arItem["ITEM_ID"]];
				?>
					<td class="title-search-item" style="font-size:13px; text-decoration:none; color:#000;"><a href="<?echo $arItem["URL"]?>" style="text-decoration:none; color:#000;"><?
						if (is_array($arElement["PICTURE"])):?>
						<img align="left" src="<?echo $arElement["PICTURE"]["src"]?>" width="<?echo $arElement["PICTURE"]["width"]?>" height="<?echo $arElement["PICTURE"]["height"]?>" style="padding-right:5px;">
						<?endif;?>

<?
if(strlen($arItem["NAME"])>39){
?>
<?=substr($arItem["NAME"],0,40)?>...

<?}else{?>
<?=$arItem["NAME"];?>

<?}?>

</a>
						<?/*?>
<p class="title-search-preview" style="font-size:10px;"><font style="font-size:12px;"><?echo $arElement["PREVIEW_TEXT"];?></font></p>
<?*/?>
						<?foreach($arElement["PRICES"] as $code=>$arPrice):?>
							<?if($arPrice["CAN_ACCESS"]):?>
						<p class="title-search-price" style="padding-top:0px;">
								<?if($arPrice["DISCOUNT_VALUE"] < $arPrice["VALUE"]):?>
									<s><?=$arPrice["PRINT_VALUE"]?></s> <span class="catalog-price"><?=$arPrice["PRINT_DISCOUNT_VALUE"]?></span>
								<?else:?><span class="catalog-price" style="padding-top:0px;"><?=$arPrice["PRINT_VALUE"]?></span><?endif;?>
								</p>
							<?endif;?>
						<?endforeach;?>
					</td>
				<?elseif(isset($arItem["ICON"])):?>
					<td class="title-search-item"><a href="<?echo $arItem["URL"]?>" style="text-decoration:none; color:#000;">
<?
if(strlen($arItem["NAME"])>39){
?>
<?=substr($arItem["NAME"],0,40)?>...

<?}else{?>
<?=$arItem["NAME"];?>

<?}?>

</td>
				<?else:?>
					<td class="title-search-more"><a href="<?echo $arItem["URL"]?>" style="text-decoration:none; color:#000;">
<?
if(strlen($arItem["NAME"])>39){
?>
<?=substr($arItem["NAME"],0,40)?>...

<?}else{?>
<?=$arItem["NAME"];?>

<?}?>

</td>
				<?endif;?>
			</tr>
			<?endforeach;?>
		<?endforeach;?>

<tr>
			<th class="title-search-separator">&nbsp;</th>
			<td class="title-search-separator">&nbsp;</td>
		</tr>


	</table><div class="title-search-fader"></div>
<?endif;
?>