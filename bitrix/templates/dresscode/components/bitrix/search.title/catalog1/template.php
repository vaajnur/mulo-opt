<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);?>
	

<?
$INPUT_ID = trim($arParams["~INPUT_ID"]);
if(strlen($INPUT_ID) <= 0)
	$INPUT_ID = "title-search-input";
$INPUT_ID = CUtil::JSEscape($INPUT_ID);

$CONTAINER_ID = trim($arParams["~CONTAINER_ID"]);
if(strlen($CONTAINER_ID) <= 0)
	$CONTAINER_ID = "title-search";
$CONTAINER_ID = CUtil::JSEscape($CONTAINER_ID);

if($arParams["SHOW_INPUT"] !== "N"):?>

<div >

<div id="topSearch2">
	<form action="<?echo $arResult["FORM_ACTION"]?>" method="GET" id="topSearchForm">
		<div class="searchContainerInner">
			<div class="searchContainer">
				<div class="searchColumn" id="<?echo $CONTAINER_ID?>">
					<input type="text" name="q" value="" id="<?echo $INPUT_ID?>" autocomplete="off" placeholder="<?=GetMessage('SEARCH_TEXT');?>"  style="background: url(/bitrix/templates/dresscode/components/dresscode/search.line/version4/images/searchH2.png) 14px 50% no-repeat transparent;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
    padding-left: 42px;
    height: 50px;
    width: 100%;
    border: 0;">
				</div>
				<div class="searchColumn">
					<input type="submit" name="s"  value="Y" id="goSearch">
					<input type="hidden" name="r" value="Y">
				</div>
			</div>
		</div>
	</form>
</div>
	</div>
		<?/*?>



<?*/?>
<?endif?>
<script>
	BX.ready(function(){
		new JCTitleSearch({
			'AJAX_PAGE' : '<?echo CUtil::JSEscape(POST_FORM_ACTION_URI)?>',
			'CONTAINER_ID': '<?echo $CONTAINER_ID?>',
			'INPUT_ID': '<?echo $INPUT_ID?>',
			'MIN_QUERY_LEN': 2
		});
	});
</script>
