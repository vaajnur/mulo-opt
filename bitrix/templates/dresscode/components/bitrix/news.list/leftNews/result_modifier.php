<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */

$postfix = LANGUAGE_ID == 'ua' ? 'UA' : (LANGUAGE_ID == 'en' ? 'EN' : '');
if(LANGUAGE_ID != 'ru'){
	switch (LANGUAGE_ID) {
		case 'ua':
			$arResult['NAME'] = 'Майстер класи, рецепти, поради';
			break;
		case 'en':
			$arResult['NAME'] = 'Master classes, recipes, tips';
			break;
	}
	foreach ($arResult["ITEMS"] as &$arItem)
	{
		$arItem['NAME'] = $arItem['PROPERTIES']['NAME'.$postfix]['VALUE'];
		$arItem['PREVIEW_TEXT'] = html_entity_decode($arItem['PROPERTIES']['PREVIEW_TEXT'.$postfix]['VALUE']['TEXT']);
		$arItem['DETAIL_TEXT'] = html_entity_decode($arItem['PROPERTIES']['DETAIL_TEXT'.$postfix]['VALUE']['TEXT']);
		// getar($arResult);
	}
}