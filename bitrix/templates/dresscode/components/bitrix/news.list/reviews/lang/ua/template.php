<?
	$MESS ["SHOP_REVIEW_HEADING"] = " Загальний рейтинг магазину";
	$MESS ["SHOP_REVIEW_NEW_BUTTON"] = " залишити відгук";
	$MESS ["SHOP_REVIEW_COUNT_LABEL"] = " відгуків:";
	$MESS ["SHOP_REVIEW_AUTHOR"] = " Автор:";
	$MESS ["SHOP_REVIEW_AUTHOR_DEFAULT"] = " Анонім";
	$MESS ["SHOP_REVIEW_UTILE"] = " відгук корисний?";
	$MESS ["SHOP_REVIEW_ANSWER"] = " відповідь магазину";
	$MESS ["SHOP_REVIEW_UTILE_YES"] = " так";
	$MESS ["SHOP_REVIEW_UTILE_NO"] = " ні";
	$MESS ["SHOP_REVIEW_FORM_HEADING"] = " додати відгук";
	$MESS ["SHOP_REVIEW_FORM_RATING"] = " Ваша оцінка:";
	$MESS ["SHOP_REVIEW_FORM_NAME"] = " Представтеся:";
	$MESS ["SHOP_REVIEW_FORM_SUBMIT"] = " залишити відгук";
	$MESS ["SHOP_REVIEW_FORM_TEXT"] = " текст відкликання:";
	$MESS ["SHOP_REVIEW_FORM_RATING_SELECT"] = " виберіть оцінку:";
	$MESS ["SHOP_REVIEW_FORM_RATING_VALUE_5"] = " відмінний магазин";
	$MESS ["SHOP_REVIEW_FORM_RATING_VALUE_4"] = " Хороший магазин";
	$MESS ["SHOP_REVIEW_FORM_RATING_VALUE_3"] = " нормальний магазин";
	$MESS ["SHOP_REVIEW_FORM_RATING_VALUE_2"] = " поганий магазин";
	$MESS ["SHOP_REVIEW_FORM_RATING_VALUE_1"] = " жахливий магазин";
	$MESS ["SHOP_REVIEW_WINDOW_EXIT"] = " закрити вікно";
	$MESS ["SHOP_REVIEW_WINDOWS_ERROR_HEADING"] = " помилка";
	$MESS ["SHOP_REVIEW_WINDOWS_SUCCESS_HEADING"] = " відгук успішно доданий";
	$MESS ["SHOP_REVIEW_WINDOWS_SUCCESS_MESSAGE"] = " спасибі, ваш відгук буде додано після модерації!";
	$MESS ["SHOP_REVIEW_WINDOWS_ERROR_TYPE_1"] = " ви вже залишали відгук!";
	$MESS ["SHOP_REVIEW_WINDOWS_ERROR_TYPE_2"] = " заповніть всі поля!";
	$MESS ["SHOP_REVIEW_WINDOWS_ERROR_TYPE_3"] = " не вибрано інфоблок!";
	$MESS ["SHOP_REVIEW_WINDOWS_ERROR_TYPE_4"] = " для відправки відгуку необхідно авторизуватися!";
	$MESS ["SHOP_REVIEW_WINDOWS_ERROR_TYPE_5"] = " не передано ID відгуку!";
	$MESS ["SHOP_REVIEW_WINDOWS_ERROR_TYPE_6"] = " не передано ID інфоблоку!";
	$MESS ["SHOP_REVIEW_WINDOWS_ERROR_TYPE_7"] = " відгук не знайдено!";
	$MESS ["SHOP_REVIEW_WINDOWS_ERROR_TYPE_8"] = " ви вже голосували!";
	$MESS ["SHOP_REVIEW_HEADING_TEXT"] = " ви можете оцінити якість роботи нашого магазину і залишити відгук про нашу роботу.  Зверніть увагу, що відгуки можуть залишати Тільки авторизовані користувачі.  
Перш ніж залишити відгук, ознайомтеся будь ласка з правилами публікації відгуків на сайті";
?>