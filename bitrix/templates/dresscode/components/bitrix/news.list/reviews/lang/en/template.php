<?
$MESS["SHOP_REVIEW_HEADING"] = "Overall store rating";
$MESS["SHOP_REVIEW_NEW_BUTTON"] = "Leave a review";
$MESS["SHOP_REVIEW_COUNT_LABEL"] = "Reviews:";
$MESS ["SHOP_REVIEW_AUTHOR"] = " Author:";
$MESS ["SHOP_REVIEW_AUTHOR_DEFAULT"] = " Anonymous";
$MESS["SHOP_REVIEW_UTILE"] = "Is the review useful?";
$MESS ["SHOP_REVIEW_ANSWER"] = "Store response";
$MESS["SHOP_REVIEW_UTILE_YES"] = "Yes";
$MESS ["SHOP_REVIEW_UTILE_NO"] = " No";
$MESS["SHOP_REVIEW_FORM_HEADING"] = "Add a review";
$MESS["SHOP_REVIEW_FORM_RATING"] = "Your rating:";
$MESS ["SHOP_REVIEW_FORM_NAME"] = " Introduce yourself:";
$MESS["SHOP_REVIEW_FORM_SUBMIT"] = "Leave a review";
$MESS ["SHOP_REVIEW_FORM_TEXT"] = " Review text:";
$MESS ["SHOP_REVIEW_FORM_RATING_SELECT"] = "Choose a rating:";
$MESS ["SHOP_REVIEW_FORM_RATING_VALUE_5"] = " Great store";
$MESS ["SHOP_REVIEW_FORM_RATING_VALUE_4"] = " Good store";
$MESS ["SHOP_REVIEW_FORM_RATING_VALUE_3"] = " Normal store";
$MESS ["SHOP_REVIEW_FORM_RATING_VALUE_2"] = " Bad store";
$MESS ["SHOP_REVIEW_FORM_RATING_VALUE_1"] = " Terrible store";
$MESS["SHOP_REVIEW_WINDOW_EXIT"] = "Close window";
$MESS ["SHOP_REVIEW_WINDOWS_ERROR_HEADING"] = "Error";
$MESS["SHOP_REVIEW_WINDOWS_SUCCESS_HEADING"] = "Review added successfully";
$MESS ["SHOP_REVIEW_WINDOWS_SUCCESS_MESSAGE"] = " Thank you, your review will be added after moderation!";
$MESS ["SHOP_REVIEW_WINDOWS_ERROR_TYPE_1"] = "You've already left a review!";
$MESS ["SHOP_REVIEW_WINDOWS_ERROR_TYPE_2"] = "Fill in all fields!";
$MESS ["SHOP_REVIEW_WINDOWS_ERROR_TYPE_3"] = " No info block selected!";
$MESS ["SHOP_REVIEW_WINDOWS_ERROR_TYPE_4"] = " You need to log in to send a review!";
$MESS ["SHOP_REVIEW_WINDOWS_ERROR_TYPE_5"] = " Review ID not passed!";
$MESS ["SHOP_REVIEW_WINDOWS_ERROR_TYPE_6"] = " Info block ID not passed!";
$MESS ["SHOP_REVIEW_WINDOWS_ERROR_TYPE_7"] = " Review not found!";
$MESS ["SHOP_REVIEW_WINDOWS_ERROR_TYPE_8"] = "You already voted!";
$MESS ["SHOP_REVIEW_HEADING_TEXT"] = "You can evaluate the quality of our store and leave feedback about our work. Please note that only authorized users can leave reviews.
Before leaving a review, please read the rules for publishing reviews on the site";
?>