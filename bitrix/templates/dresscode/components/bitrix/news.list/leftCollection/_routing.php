<?php

// ***************************************
// ***
// *** Do not touch anything unless 
// *** you know what you're doing
// ***
// ***************************************
// -- KoXa

//--------- Active Configuration ------//

// default: 'plain'
$conf['active'] = 'plain'; // mysql, sqlite3, plain
//------------------------------------//

//--------mysql---------//
$conf['mysql']['host'] = 'localhost';
$conf['mysql']['user'] = 'root';
$conf['mysql']['pass'] = 'qwerty';
$conf['mysql']['db'] = 'lookup';
$conf['mysql']['table'] = 'lookup';

//--------plain/text------//
$conf['plain']['db'] = '.userfiles/db.list';

//--------sqlite----------//
$conf['sqlite3']['db'] = '.userfiles/db.sqlite3';
$conf['sqlite3']['table'] = 'lookup';


$conf['site_uri'] = get_site_dir();

// here we go

$p = get_uri_heuristic();

if (!$p || $p == "/") $p = './'; // index page
if (preg_match('#^(\/|\.\./)#', $p)||preg_match('#\./\.#',$p)) die_not_found();

$get_node = "get_node_".$conf['active'];
$node = $get_node($p);



if (!$node) die_not_found();	



	header("Content-Type: $node[ctype]");
	
	if (!file_exists($node['realname'])) die_not_found();
	
//	if (preg_match('/.*\.php$/i', $node['realname'])) include_once($node['realname']);
//    else echo file_get_contents($node['realname']);

$html = file_get_contents($node['realname']);
parse_asserts($html);
echo $html;



//----------helpers-----------//

function get_node_plain($uri) {
global $conf;	

	$routes = file_get_contents($conf['plain']['db']);
	$regex  = quotez($uri).'\s*=>\s*(.*?)\s*=>\s*(.*)';
	$regex  = str_replace('#', '\#', $regex);
	//die($regex);
	$row = array();

	if (preg_match("#$regex#", $routes, $matches)) {
	
		$row['realname'] = trim($matches[1]);
		$row['ctype']    = trim($matches[2]);

	} return $row;	
}

function get_node_sqlite3($uri) {
global $conf;

if (!_enabled('sqlite3')) die();
if (!file_exists($conf['sqlite3']['db'])) die("SQLITE ERROR: NO ROUTING DB");

	$db = new SQLite3($conf['sqlite3']['db']);
	if (!$db) die("SQLITE ERROR: DB OPEN FAILED");
	
	$uri = sqlite_escape_string($uri);

	$stmt = $db->prepare("SELECT * FROM `".$conf['sqlite3']['table']."` WHERE `uri` = :uri  LIMIT 0,1");
	$stmt->bindValue(':uri', $uri);
	$res = $stmt->execute();
	if ($row = $res->fetchArray(SQLITE3_ASSOC)) {
		return $row;
	} else {
		return false;
	} 
	
}

function get_node_mysql($uri) {
global $conf;

 $db = mysql_connect($conf['mysql']['host'], $conf['mysql']['user'], $conf['mysql']['pass']) or die("db open error: ".mysql_error()); 
 mysql_select_db($conf['mysql']['db']) or die("cannot select db: ".mysql_error());
 mysql_query("SET NAMES utf8");


$uri = mysql_real_escape_string($uri);
$query = "SELECT * FROM `".$conf['mysql']['table']."` WHERE `uri` = '$uri' LIMIT 0,1";
$res = mysql_query($query) or die(mysql_error());
if ($row = mysql_fetch_assoc($res)) {
	return $row;
} else {
	return false;
}

	
}


function get_uri_heuristic() {
global $conf;	
	//$p = $_SERVER['QUERY_STRING'];
	$z = $_SERVER['REQUEST_URI'];
	
	$z = preg_replace('/^'.quotez($conf['site_uri']).'/', '', $z, 1); // только 1 замена
        if ($z == "index.html") $z = ""; // на некоторых серваках такой сброс нужен
	
	
	
	return $z;
}

function quotez($str) {
   
	return preg_replace('/([^a-zA-Z_0-9])/', '\\\\\1', $str);
}

function die_not_found() {
global $p, $conf;
	if ($p != './' && $p != '/' && $p != 'index.html') { // redirect unless index page
		header("Location: $conf[site_uri]");
		die();
	} else {
		die("NOT FOUND");
	}
	
}

function _enabled($opt) {
	switch($opt) {
		case 'sqlite3':
		
			if (!class_exists('SQLite3')) {
				print nl2br('SQLITE_3 NOT ENABLED
						
						<b>sudo apt-get install php5-sqlite</b>
					
and restart Apache');
				return 0;
			} else {
				return 1;
			}
		
		break;
	}
}

function get_site_dir() {
/*
$p = $_SERVER['QUERY_STRING'];
$z = $_SERVER['REQUEST_URI'];
if ($p == "index.html") $p = ""; // на некоторых серваках такой сброс нужен
*/
$script_name = "_routing.php";
$s = $_SERVER['SCRIPT_FILENAME'];
if (preg_match('#.*/(\S+)#', $s, $m)) $script_name = $m[1];
$r = $_SERVER['DOCUMENT_ROOT'];

$ret = preg_replace('/^'.quotez($r).'/', '', $s);
$ret = preg_replace('/'.quotez($script_name).'$/', '', $ret);
if (!$ret) $ret="/";
return $ret; 
}

function parse_asserts(&$html) {

$assert['header.php']='';
$assert['footer.php']='';


$_dir = ".userfiles";
foreach($assert as $_k => $_v) {
  if (file_exists("$_dir/$_k")) {
    ob_start();
    include_once("$_dir/$_k");
    $assert[$_k] = ob_get_clean();
  }
}

/* //не нужно, т.к. код сапы включается в файлах header.php/footer.php
// -= S A P E =-
if (file_exists("$_dir/sape_conf.php")) {
  //include_once("$_dir/sape_conf.php");
  include_once("$_dir/sape.php");

  
  $sape = new SAPE_client(array(
     'charset'=>'utf-8' //,'force_show_code' => true
  ));

  $links = $sape->return_links(_SAPE_NUM_LINKS);

  if (_SAPE_ASSERT_POS_BEFORE) { 
     $html = preg_replace('#('.quotez(_SAPE_ASSERT_TAG).')#i', $links.'$1', $html);
  } else {
     $html = preg_replace('#('.quotez(_SAPE_ASSERT_TAG).')#i', '$1'.$links, $html);
  }
   
} // [eof] -= S A P E =-

*/

//$html = preg_replace('#</head>#i', $assert['head'].'</head>', $html);
$html = preg_replace('#(<body(.*?)>)#i','${1}'.$assert['header.php'], $html);
$html = preg_replace('#</body>#i', $assert['footer.php'].'</body>', $html);

}

?>
