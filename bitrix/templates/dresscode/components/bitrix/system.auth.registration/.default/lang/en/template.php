<?
$MESS["AUTH_REGISTER"] = "Registration";
$MESS ["AUTH_NAME"] = " Name";
$MESS["AUTH_LAST_NAME"] = "Last name";
$MESS ["AUTH_LOGIN_MIN"] = " Login (minimum 3 characters)";
$MESS ["AUTH_CONFIRM"] = " Confirm password";
$MESS["CAPTCHA_REGF_PROMT"] = "Enter the word in the picture";
$MESS["AUTH_REQ"] = "Required fields.";
$MESS ["AUTH_AUTH"] = " Authorization";
$MESS ["AUTH_PASSWORD_REQ"] = " Password";
$MESS["AUTH_EMAIL_WILL_BE_SENT"] = "You will receive a request for confirmation of registration to the e-mail specified in the form.";
$MESS["AUTH_EMAIL_SENT"] = "An email with information about the registration confirmation was sent to the e-mail specified in the form.";
$MESS ["AUTH_EMAIL"] = "E-Mail";
$MESS ["AUTH_SECURE_NOTE"] = " Before submitting the form, the password will be encrypted in the browser. This will avoid sending the password in clear text.";
$MESS ["AUTH_FORGOT_PASSWORD_2"] = "Forgot your password?";
$MESS ["REGISTER_TITLE"] = "Registration";
$MESS["REGISTER_TEXT"] = "By registering on our website, you will be able to track the status of your orders, be aware of promotions and discounts, and receive individual offers. 
It is enough to specify your data once during registration, and in the future they will be substituted automatically.";
$MESS ["USER_PERSONAL_INFO"] = 'I agree to the <a href="/personal-info/" class="pilink">processing of personal data.</a>';
?>