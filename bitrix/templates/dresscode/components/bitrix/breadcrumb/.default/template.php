<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$postfix = LANGUAGE_ID == 'ua' ? 'UA' : (LANGUAGE_ID == 'en' ? 'EN' : '');
// getar($arResult);

if(empty($arResult))
	return "";
	
$strReturn = '<div> <ul itemscope itemtype="https://schema.org/BreadcrumbList" id="breadcrumbs"><ul>';

$num_items = count($arResult);
for($index = 0, $itemSize = $num_items; $index < $itemSize; $index++)
{
    if($index > 0)
        $strReturn .= '<li class="arrow"><span> • </span></li>';

$title = htmlspecialcharsex($arResult[$index]["TITLE"]);

// замена для мультиязыка
if(LANGUAGE_ID != 'ru'){
	if($arResult[$index]['LINK'] != '' && $title != false){
		$sect1 = ciblocksection::getlist([], ['NAME' => $title , 'IBLOCK_ID' => 10 ], false, ['UF_*']);
		if($ob_sect = $sect1->getnext()){
			$title = $ob_sect['UF_NAME'.$postfix];
		}
	}elseif($arResult[$index]['LINK'] == '' && $title != false){
		$res1 = ciblockelement::getlist([], ['IBLOCK_ID' => [9,10], 'NAME' => $title], false, false, ['PROPERTY_NAME'.$postfix]);
		if($ob_elem = $res1->getnext()){
			// getar($ob_elem);
			$title = $ob_elem['PROPERTY_NAME'.$postfix.'_VALUE'];
		}
	}
}

$strReturn .= '<li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"';
$strReturn .= '>';
$strReturn .= '<a href="'.$arResult[$index]["LINK"].'" title="'.$title.'" itemprop="item">';
$strReturn .= '<span itemprop="name">'.$title.'</span>';
$strReturn .= '<meta itemprop="position" content="'.$index.'" />';
$strReturn .= '</a>';
$strReturn .= '</li>';
}

$strReturn .= '</ul></div>';

return $strReturn;

?>
