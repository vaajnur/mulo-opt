<?$APPLICATION->SetAdditionalCSS($templateFolder."/css/review.css");?>
<?$APPLICATION->SetAdditionalCSS($templateFolder."/css/media.css");?>
<?$APPLICATION->SetAdditionalCSS($templateFolder."/css/set.css");?>

<?$APPLICATION->AddHeadScript($templateFolder."/js/morePicturesCarousel.js");?>
<?$APPLICATION->AddHeadScript($templateFolder."/js/pictureSlider.js");?>
<?$APPLICATION->AddHeadScript($templateFolder."/js/zoomer.js");?>
<?$APPLICATION->AddHeadScript($templateFolder."/js/tabs.js");?>
<?$APPLICATION->AddHeadScript($templateFolder."/js/sku.js");?>

<?/*
	if($arResult['DETAIL_PAGE_URL'] !== $arResult['DETAIL_PAGE_URL_TMP']){
		$APPLICATION->AddHeadString('<link href="'.$arResult['DETAIL_PAGE_URL'].'" rel="canonical" />', true);
	}
*/?>

<?$APPLICATION->IncludeComponent(
	"coffeediz:schema.org.Product", 
	".default", 
	array(
		"SHOW" => "Y",
		"NAME" => $arResult["NAME"],
		"DESCRIPTION" => $arResult["description"],
		"PRICE" => $arResult["PRICE"],
		"PRICECURRENCY" => "UAH",
		"ITEMAVAILABILITY" => "InStock",
		"ITEMCONDITION" => "NewCondition",
		"PAYMENTMETHOD" => array(
			0 => "VISA",
			1 => "MasterCard",
			2 => "Cash",
			3 => "CheckInAdvance",
			4 => "COD",
		),
		"PARAM_RATING_SHOW" => "Y",
		"COMPONENT_TEMPLATE" => ".default",
		"AGGREGATEOFFER" => "N",
		"RATING_SHOW" => "N",
		"RATINGVALUE" => $arResult["rating"]["VALUE"],
		"RAITINGCOUNT" => $arResult["vote_count"]["VALUE"],
		"REVIEWCOUNT" => "",
		"BESTRATING" => "5",
		"WORSTRATING" => "1",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO"
	),
	false
);
//$APPLICATION->AddHeadString('<link href="https://'.SITE_SERVER_NAME.$arResult['DETAIL_PAGE_URL'].'" rel="canonical" />',true);
?>