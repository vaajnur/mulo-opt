<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
$this->setFrameMode(true);?>
<?if(!empty($arResult["SECTIONS"])):
$postfix = LANGUAGE_ID == 'ua' ? 'UA' : (LANGUAGE_ID == 'en' ? 'EN' : '');
?>
	<div class="catalog-section-list-pictures clearfix">
		<?foreach($arResult["SECTIONS"] as $arElement):
			if(LANGUAGE_ID != 'ru'){
				$arElement['NAME'] = $arElement['UF_NAME'.$postfix];
			}
			// getar($arElement);
		?>
			<div class="catalog-section-list-item">
				<div class="catalog-section-list-item-wp">
					<?if(!empty($arElement["PICTURE"])):?>
						<?$picture = CFile::ResizeImageGet($arElement["PICTURE"], array("width" => 280, "height" => 220), BX_RESIZE_IMAGE_PROPORTIONAL, true);?>
						<a href="<?=$arElement["SECTION_PAGE_URL"]?>" class="catalog-section-list-picture"><img src="<?=$picture["src"]?>" alt="<?=$arElement["NAME"]?>" title="<?=$arElement["NAME"]?>"></a>
					<?endif;?>
					<a href="<?=$arElement["SECTION_PAGE_URL"]?>" class="catalog-section-list-link"><span><?=$arElement["NAME"]?></span></a>
				</div>
			</div>
		<?endforeach;?>
	</div>
<?
// getar($arResult["SECTION"]);
if(LANGUAGE_ID != 'ru'){
	$arResult["SECTION"]["DESCRIPTION"] = $arResult["SECTION"]["UF_TEXT".$postfix];	
}
echo $arResult["SECTION"]["DESCRIPTION"];
?>

<?endif;?>
