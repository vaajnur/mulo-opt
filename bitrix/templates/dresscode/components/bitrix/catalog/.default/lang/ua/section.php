<?
$MESS ["CATALOG_SPORT_LABEL"] = " Сортувати за:";
$MESS ["CATALOG_SORT_TO_LABEL"] = " показати по:";
$MESS ["CATALOG_SORT_FIELD_NAME"] = " алфавіту";
$MESS ["CATALOG_SORT_FIELD_SHOWS"] = " популярності";
$MESS ["CATALOG_SORT_FIELD_PRICE_ASC"] = " збільшення ціни";
$MESS ["CATALOG_SORT_FIELD_PRICE_DESC"] = " зменшенню ціни";
$MESS ["CATALOG_SORT_FIELD_AVAILABLE_DESC"] ="";
$MESS ["CATALOG_VIEW_LABEL"] = " Вид каталогу:";
$MESS ["CATALOG_FILTER"] = " фільтр";