<?
$MESS ["AUTH_TITLE"] = " Authorization";
$MESS ["AUTH_PLEASE_AUTH"] = " Please log in";
$MESS ["AUTH_LOGIN"] = " Login";
$MESS ["AUTH_PASSWORD"] = " Password";
$MESS["AUTH_REMEMBER_ME"] = "Remember me on this computer";
$MESS["AUTH_AUTHORIZE"] = "Log in";
$MESS["AUTH_REGISTER"] = "Register";
$MESS["AUTH_FIRST_ONE"] = "If this is your first time on the site, please fill out the registration form.";
$MESS ["AUTH_FORGOT_PASSWORD_2"] = "Forgot your password?";
$MESS ["AUTH_CAPTCHA_PROMT"] = "Enter the word in the picture";
$MESS["AUTH_TITLE"] = "Log in to the site";
$MESS ["AUTH_SECURE_NOTE"] = " Before submitting the authorization form, the password will be encrypted in the browser. This will avoid sending the password in clear text.";
$MESS ["AUTH_NONSECURE_NOTE"] = " The password will be sent in clear text. Enable JavaScript in the browser to encrypt the password before sending it.";
$MESS ["AUTH_SERVICES_TITLE"] = "Log in as a user";
$MESS["REGISTER_TEXT"] = "<a href=\"".SITE_DIR."auth/?register=yes&backurl=%2F\" class=\"registerLink\">By registering</a> on our website, you will be able to track the status of your orders, be aware of promotions and discounts, and receive individual offers. 
It is enough to specify your data once during registration, and in the future they will be substituted automatically.";
