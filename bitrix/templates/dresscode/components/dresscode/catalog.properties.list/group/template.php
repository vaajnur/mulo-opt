<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?$this->setFrameMode(true);?>
<? //pr(LANGUAGE_ID);?>  
<?if(!empty($arResult["DISPLAY_PROPERTIES"])):?>
    <div id="elementProperties">
        <span class="heading"><?=GetMessage("SPECS")?></span>
		<table class="stats">
			<tbody>
                <?foreach($arResult["DISPLAY_PROPERTIES_GROUP"] as $arProp):?>
                    <?if((!empty($arProp["VALUE"]) || !empty($arProp["LINK"])) && $arProp["SORT"] <= 5000):?>
                        <?
                            $set++;
                            $PROP_NAME = $arProp["NAME"];

                            // скрываю св-ва с наличием языкового постфикса
                            if(LANGUAGE_ID != 'ru' &&  substr($arProp["CODE"], -2) != strtoupper(LANGUAGE_ID) &&  !in_array($arProp["CODE"], ['INCI', 'SHELF_LIFE', 'BEST_BEFORE', 'PH']) ) continue;
                            if(LANGUAGE_ID == 'ru' && in_array( substr($arProp["CODE"], -2) , ['UA', 'EN']  )  ) continue;
                            // name
                            if(LANGUAGE_ID == 'ua'){
                                switch ($arProp['CODE']) {
                                    case 'SHELF_LIFE':
                                        $arProp['NAME'] = 'Термін придатності';
                                        break;
                                    case 'BEST_BEFORE':
                                        $arProp['NAME'] = 'Придатний до';
                                        break;
                                }
                            }elseif(LANGUAGE_ID == 'en'){
                                switch ($arProp['CODE']) {
                                    case 'SHELF_LIFE':
                                        $arProp['NAME'] = 'Shelf life';
                                        break;
                                    case 'BEST_BEFORE':
                                        $arProp['NAME'] = 'Valid until';
                                        break;
                                }                    
                            }
                        ?>
                        <?if(!empty($arProp["VALUE"]) && gettype($arProp["VALUE"]) == "array"){$arProp["VALUE"] = implode(" / ", $arProp["VALUE"]); $arProp["FILTRABLE"] = false;}?>
                        <?if(preg_match("/\[.*\]/", trim($arProp["NAME"]), $res, PREG_OFFSET_CAPTURE)):?>
                            <?$arProp["NAME"] = substr($arProp["NAME"], 0, $res[0][1]);?>
                            <?if($res[0][0] != $arResult["OLD_CAP"]):?>
                                <?
                                    $arResult["OLD_CAP"] = $res[0][0];
                                    $set = 1;
                                ?>
                                <tr class="cap">
                                    <td colspan="2"><?=substr($res[0][0], 1, -1)?></td>
                                    <td class="right"></td>
                                </tr>
                            <?endif;?>
                            <tr<?if($set % 2):?> class="gray"<?endif;?>>
                                <td class="name"><span><?=preg_replace("/\[.*\]/", "", trim($PROP_NAME))?></span><?if(!empty($arProp["HINT"])):?><a href="#" class="question" title="<?=$arProp["HINT"]?>" data-description="<?=$arProp["HINT"]?>"></a><?endif;?></td>
                                <td>
                                    <?=$arProp["DISPLAY_VALUE"]?>
                                </td>
                                <td class="right">
                                    <?if($arProp["FILTRABLE"] =="Y" && !is_array($arProp["VALUE"]) && !empty($arProp["VALUE_ENUM_ID"]) && !empty($arResult["LAST_SECTION"])):?><a href="<?=$arResult["LAST_SECTION"]["SECTION_PAGE_URL"]?>?arrFilter_<?=$arProp["ID"]?>_<?=abs(crc32($arProp["VALUE_ENUM_ID"]))?>=Y&amp;set_filter=Y" class="analog"><?=GetMessage("OTHERITEMS")?></a><?endif;?>
                                </td>
                            </tr>
                        <?else:?>
                            <? 
                            // убираю SEO метатеги
                            if(!in_array($arProp['CODE'], array('META_TITLE_UA', 'META_KEYWORDS_UA', 'META_DESCRIPTION_UA', 'META_HEADER_UA', 'PREVIEW_TEXT_UA', 'DETAIL_TEXT_UA',
                                'META_TITLE_EN', 'META_KEYWORDS_EN', 'META_DESCRIPTION_EN', 'META_HEADER_EN', 'PREVIEW_TEXT_EN', 'DETAIL_TEXT_EN'))){
                                $arSome[] = $arProp;
                            }
                            ?> 
                        <?endif;?>
                    <?endif;?>
                <?endforeach;?>
                <?if(!empty($arSome)):?>
                <?$set = 0;?>
                     <tr class="cap">
                        <td colspan="3"><?=GetMessage("CHARACTERISTICS")?></td>
                    </tr>
                    <?foreach($arSome as $arProp):?>
                        <?$set++?>
                        <tr<?if($set % 2 ):?> class="gray"<?endif;?>>
                            <td class="name"><span><?=$arProp["NAME"]?></span><?if(!empty($arProp["HINT"])):?><a href="#" class="question" title="<?=$arProp["HINT"]?>" data-description="<?=$arProp["HINT"]?>"></a><?endif;?></td>
                            <td>
                                <?if($arProp["PROPERTY_TYPE"] == "E" || $arProp["PROPERTY_TYPE"] == "S"):?>
                                    <?=$arProp["DISPLAY_VALUE"]?>
                                <?else:?>
                                    <?=$arProp["VALUE"]?>
                                <?endif;?>
                            </td>
                            <td class="right">
                                <?if($arProp["FILTRABLE"] =="Y" && !is_array($arProp["VALUE"]) && !empty($arProp["VALUE_ENUM_ID"]) && !empty($arResult["LAST_SECTION"])):?>
                                    <a href="<?=$arResult["LAST_SECTION"]["SECTION_PAGE_URL"]?>?arrFilter_<?=$arProp["ID"]?>_<?=abs(crc32($arProp["VALUE_ENUM_ID"]))?>=Y&amp;set_filter=Y" class="analog"><?=GetMessage("OTHERITEMS")?></a>
                                <?endif;?>
                            </td>
                        </tr>
                    <?endforeach;?>
                <?endif;?>
            </tbody>
		</table>
	</div>
<?endif;?>				