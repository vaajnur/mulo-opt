<?
$MESS["CATALOG_SORT_LABEL"] = " Sort by:";
$MESS["CATALOG_SORT_TO_LABEL"] = " Show by:";	
	$MESS["CATALOG_SORT_FIELD_NAME"] = "to the alphabet";
	$MESS["CATALOG_SORT_FIELD_SHOWS"] = "popularity";
	$MESS["CATALOG_SORT_FIELD_PRICE_ASC"] = "price increase";
$MESS ["CATALOG_SORT_FIELD_PRICE_DESC"] = "price reduction";
$MESS ["CATALOG_VIEW_LABEL"] = "Folder view:";
$MESS ["EMPTY_HEADING"] = "My favorites are still empty";
	$MESS["EMPTY_TEXT"] = 'Use the search or <a href="'. SITE_DIR.'catalog/">catalog</a>, select the desired products and add them to your favorites.';
	$MESS ["MAIN_PAGE"] = " Home page";
	$MESS["WISHLIST_MAIL_LABEL"] = "Send this list of products to the mail";
	$MESS["WISHLIST_MAIL_TEXT"] = "Add to your favorites all the products you are interested in and in order not to lose them, we will send them to your email so that you do not have to look for them when you want to return to the purchase";
	$MESS["WISHLIST_MAIL_FORM_LABEL"] = "Enter your email address";
	$MESS["WISHLIST_MAIL_FORM_SEND"] = "Send to email";
?>