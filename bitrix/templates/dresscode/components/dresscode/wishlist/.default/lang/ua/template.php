<?
	$MESS ["CATALOG_SORT_LABEL"] = " Сортувати за:";
	$MESS ["CATALOG_SORT_TO_LABEL"] = " показати по:";	
	$MESS ["CATALOG_SORT_FIELD_NAME"] = " алфавіту";
	$MESS ["CATALOG_SORT_FIELD_SHOWS"] = " популярності";
	$MESS ["CATALOG_SORT_FIELD_PRICE_ASC"] = " збільшення ціни";
	$MESS ["CATALOG_SORT_FIELD_PRICE_DESC"] = " зменшенню ціни";
	$MESS ["CATALOG_VIEW_LABEL"] = " Вид каталогу:";
	$MESS ["EMPTY_HEADING"] = " в обраному поки порожньо";
	$MESS ["EMPTY_TEXT"] = 'скористайтеся пошуком або <a href="'.SITE_DIR.'catalog/" >каталогом</a>, виберіть потрібні товари і додайте їх в обране.';
	$MESS ["MAIN_PAGE"] = " Головна сторінка";
	$MESS ["WISHLIST_MAIL_LABEL"] = " відправити даний список товарів на пошту";
	$MESS ["WISHLIST_MAIL_TEXT"] = " додайте в обране всі товари, які Вас цікавлять, і щоб їх не втратити, ми відправимо Вам їх на електронну пошту, щоб Вам не довелося їх шукати, коли ви захочете повернутися до покупки";
	$MESS ["WISHLIST_MAIL_FORM_LABEL"] = " введіть ваш email";
	$MESS ["WISHLIST_MAIL_FORM_SEND"] = " надіслати на пошту";
?>