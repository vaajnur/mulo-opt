<?
$MESS ["PROFILE_HEAD"] = " Personal data";
	$MESS ["CHANGE_PASS"] = " Change password";
	$MESS["PASS"] = "Enter your new password";
	$MESS ["REPASS"] = " Confirm password";
	$MESS ["SAVE"] = " Save changes";
	$MESS["SETTINGS"] = "Profile setup";
	$MESS ["CART"] = " Cart";
	$MESS ["COMPARE"] = " Products in comparison";
	$MESS["HISTORY"] = " Order history";
	$MESS["CALLBACK"] = "Feedback";
	$MESS["ERROR"] = "Error";
	$MESS["CLOSE"] = "Close window";
?>