<?
	$MESS ["PROFILE_HEAD"] = " персональні дані";
	$MESS ["CHANGE_PASS"] = " змінити пароль";
	$MESS ["PASS"] = " Введіть новий пароль";
	$MESS ["REPASS"] = " підтвердження пароля";
	$MESS ["SAVE"] = " Зберегти зміни";
	$MESS ["SETTINGS"] = " Налаштування профілю";
	$MESS ["CART"] = " кошик";
	$MESS ["COMPARE"] = " товари в порівнянні";
	$MESS ["HISTORY"] = " Історія замовлень";
	$MESS ["CALLBACK"] = " зворотній зв'язок";
	$MESS ["ERROR"] = " Помилка";
	$MESS ["CLOSE"] = " закрити вікно";
?>