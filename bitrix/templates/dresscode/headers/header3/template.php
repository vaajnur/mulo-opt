<?$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/headers/header3/css/style.css");?>
<?$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/headers/header3/css/types/".$TEMPLATE_HEADER_TYPE.".css");?>
<div id="headerLayout"<?if($TEMPLATE_SUBHEADER_COLOR != "default"):?> class="color_<?=$TEMPLATE_SUBHEADER_COLOR?>"<?endif;?>>
	<div id="subHeader3"<?if($TEMPLATE_SUBHEADER_COLOR != "default"):?> class="color_<?=$TEMPLATE_SUBHEADER_COLOR?>"<?endif;?>>
		<div class="limiter">
		<div class="img-love">
		<!--<img src="/bitrix/templates/dresscode/images/podsneg.png" style="
    height: 120px;
    position: absolute;
    right: 0;
"></img>

<img src="/bitrix/templates/dresscode/images/ciplenok.png" style="
    height: 120px;
    position: absolute;
    left:250px;
"></img>
<img src="/bitrix/templates/dresscode/images/buket.png" style="
    height: 120px;
    position: absolute;
    right:300px;
"></img>-->
   <!--  <div class="language">
        <div  data-google-lang="ru" class="language__img">РУС</div>
        <div   data-google-lang="uk" class="language__img">УКР</div>

    </div> -->
   <style type="text/css">
    	.skiptranslate {
    display: none !important;
}

/* Убираем подсветку ссылок */

.goog-text-highlight {
    background-color: inherit;
    box-shadow: none;
    box-sizing: inherit;
}

/* language */

.language {
    position: absolute;
    left: 10px;
    top: 50%;
    transform: translateY(-50%);
    z-index: 999;
    display: flex;
    /*flex-direction: column;*/
}

.language__img {
    margin: 2px;
    cursor: pointer;
    opacity: .5;
}

.language__img:hover,
.language__img_active {
    opacity: 1;
}
    </style> 
</div>
			<div class="subTable">
				<div class="subTableColumn">
					<div class="subTableContainer">
						<div id="logo">
							<?$APPLICATION->IncludeComponent("bitrix:main.include", "template2", Array(
	"AREA_FILE_SHOW" => "sect",	// Показывать включаемую область
		"AREA_FILE_SUFFIX" => "logo",	// Суффикс имени файла включаемой области
		"AREA_FILE_RECURSIVE" => "Y",	// Рекурсивное подключение включаемых областей разделов
		"EDIT_TEMPLATE" => "",	// Шаблон области по умолчанию
	),
	false
);?>
						</div>
						<div id="geoPosition">
							<ul>
								<?$APPLICATION->IncludeComponent("dresscode:sale.geo.positiion", "", array(
	
	),
	false,
	array(
	"ACTIVE_COMPONENT" => "N"
	)
);?>
							</ul>
						</div>
					</div>
				</div>
			
				
				
				<div class="subTableColumn">
					<div class="subTableContainer">
						<div id="topHeading">
							<div class="vertical">
								<?$APPLICATION->IncludeComponent(
									"bitrix:main.include",
									".default",
									array(
										"AREA_FILE_SHOW" => "sect",
										"AREA_FILE_SUFFIX" => "heading",
										"AREA_FILE_RECURSIVE" => "Y",
										"EDIT_TEMPLATE" => ""
									),
									false
								);?>
							</div>
						</div>
					</div>
				</div>
				<div class="subTableColumn">
					<div class="subTableContainer">
						
						<div id="topTools">
							<select class="custom-select" name="language" id="language">
								<option value="ru" <?=LANGUAGE_ID=='ru'?' selected ':''?>>Русский</option>
								<option value="ua" <?=LANGUAGE_ID=='ua'?' selected ':''?>>Український</option>
								<option value="en" <?=LANGUAGE_ID=='en'?' selected ':''?>>English</option>
							</select>
								<!-- <div class="language">
									<div data-google-lang="ru"  class="language__img">РУС</div>
									<div  data-google-lang="uk" class="language__img">УКР</div>
							</div> -->

							<?/*if(SITE_ID == 'uk') {?>
							<div id="topTools">
								<div class="language">
									<div   class="language__img"><a href="/">РОС</a></div>
									<div   class="language__img"><a href="/ua/">УКР</a></div>
							</div>
						<?}else {?>
							<div id="topTools">
								<div class="language">
									<div   class="language__img"><a href="/">РУС</a></div>
									<div   class="language__img"><a href="/ua/">УКР</a></div>
							</div>
<?}*/?>

							<div id="topToolsLeft">
								<ul>
									<?$APPLICATION->IncludeComponent("bitrix:system.auth.form", "top", Array(
										"REGISTER_URL" => "",
											"FORGOT_PASSWORD_URL" => "",
											"PROFILE_URL" => "",
											"SHOW_ERRORS" => "N",
										),
										false
									);?>
								</ul>
							</div>
							<div id="topToolsRight">
								<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
	"AREA_FILE_SHOW" => "sect",
		"AREA_FILE_SUFFIX" => "phone2",
		"AREA_FILE_RECURSIVE" => "Y",
		"EDIT_TEMPLATE" => ""
	),
	false,
	array(
	"ACTIVE_COMPONENT" => "N"
	)
);?>
								<?$APPLICATION->IncludeComponent(
									"bitrix:main.include",
									".default",
									array(
										"AREA_FILE_SHOW" => "sect",
										"AREA_FILE_SUFFIX" => "phone",
										"AREA_FILE_RECURSIVE" => "Y",
										"EDIT_TEMPLATE" => ""
									),
									false
								);?>
							</div>
						</div>
						<div id="topSearchLine">
							<?$APPLICATION->IncludeComponent(
								"bitrix:main.include",
								".default",
								array(
									"AREA_FILE_SHOW" => "sect",
									"AREA_FILE_SUFFIX" => "searchLine2",
									"AREA_FILE_RECURSIVE" => "Y",
									"EDIT_TEMPLATE" => ""
								),
								false
							);?>
						</div>
					</div>
				</div>
				<div class="subTableColumn">
					<div class="subTableContainer">
						<div class="cart">
							<div id="flushTopCart">
								<?$APPLICATION->IncludeComponent(
									"bitrix:sale.basket.basket.line",
									"topCart3",
									array(
										"HIDE_ON_BASKET_PAGES" => "N",
										"PATH_TO_BASKET" => SITE_DIR."personal/cart/",
										"PATH_TO_ORDER" => SITE_DIR."personal/order/make/",
										"PATH_TO_PERSONAL" => SITE_DIR."personal/",
										"PATH_TO_PROFILE" => SITE_DIR."personal/",
										"PATH_TO_REGISTER" => SITE_DIR."login/",
										"POSITION_FIXED" => "N",
										"SHOW_AUTHOR" => "N",
										"SHOW_EMPTY_VALUES" => "Y",
										"SHOW_NUM_PRODUCTS" => "Y",
										"SHOW_PERSONAL_LINK" => "N",
										"SHOW_PRODUCTS" => "Y",
										"SHOW_TOTAL_PRICE" => "Y",
										"COMPONENT_TEMPLATE" => "topCart",
										"SHOW_DELAY" => "N",
										"SHOW_NOTAVAIL" => "N",
										"SHOW_SUBSCRIBE" => "N",
										"SHOW_IMAGE" => "Y",
										"SHOW_PRICE" => "Y",
										"SHOW_SUMMARY" => "Y"
									),
									false
								);?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="subHeaderLine"<?if($TEMPLATE_HEADER_COLOR != "default"):?> class="color_<?=$TEMPLATE_HEADER_COLOR?>"<?endif;?>>
		<div class="limiter">
			<div class="subLineContainer">
				<div class="subLineLeft">
					<?$APPLICATION->IncludeComponent(
	"bitrix:menu", 
	"topMenu2", 
	array(
		"ROOT_MENU_TYPE" => "top",
		"MENU_CACHE_TYPE" => "Y",
		"MENU_CACHE_TIME" => "3600000",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"MENU_CACHE_GET_VARS" => array(
		),
		"MAX_LEVEL" => "1",
		"CHILD_MENU_TYPE" => "top",
		"USE_EXT" => "N",
		"DELAY" => "N",
		"ALLOW_MULTI_SELECT" => "N",
		"CACHE_SELECTED_ITEMS" => "N",
		"COMPONENT_TEMPLATE" => "topMenu2"
	),
	false
);?>
				</div>
				<div class="subLineRight">
					<div class="topWishlist">
						<div id="flushTopwishlist">
							<?$APPLICATION->IncludeComponent("dresscode:favorite.line", "new_temp", Array(
	
	),
	false
);?>
						</div>
					</div>
					<div class="topCompare">
						<div id="flushTopCompare">
							<?$APPLICATION->IncludeComponent("dresscode:compare.line", "new_temp", Array(
	"COMPONENT_TEMPLATE" => "version3",
		"IBLOCK_TYPE" => "catalog",	// Тип инфоблока
		"IBLOCK_ID" => "1",	// Инфоблок
		"CACHE_TYPE" => "Y",	// Тип кеширования
		"CACHE_TIME" => "3600000",	// Время кеширования (сек.)
	),
	false
);?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>