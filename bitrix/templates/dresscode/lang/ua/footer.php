<?
$MESS["BASKET_ADDED"] = "В кошику";
$MESS["DRESS_CATALOG"] = "Каталог товарів";
$MESS["ADD_COMPARE_ADDED"] = "Додано";
$MESS["WISHLIST_ADDED"] = "Додано";
$MESS["ADD_CART_LOADING"] = "Завантаження";
$MESS["ADDED_CART_SMALL"] = "В кошику";
$MESS["FOOTER_CALLBACK_LABEL"] = "Зворотній зв'язок";
$MESS["ADD_BASKET_DEFAULT_LABEL"] = "В кошик";
$MESS["CATALOG_AVAILABLE"] = "В наявності";
$MESS["CATALOG_ON_ORDER"] = "Під замовлення";
$MESS["CATALOG_NO_AVAILABLE"] = "Немає доступу";
$MESS["COLOR_SWITCHER"] = "Колірна схема:";
$MESS["COLOR_SWITCHER_CLOSE"] = "Закрити";
$MESS["BACKGROUND_SWITCHER"] = "Колір фону";
$MESS["FAST_VIEW_PRODUCT_LABEL"] = "Швидкий перегляд";
$MESS["REQUEST_PRICE_LABEL"] = "Договір з власником";
$MESS["REQUEST_PRICE_BUTTON_LABEL"] = "Оцініть";
$MESS["TOP_MENU_HEADING"] = "Верхнє меню";
$MESS["LEFT_MENU_HEADING"] = "Ліве меню";
$MESS["COLOR_SWITCHER_MENU"] = "Тип шаблону";
$MESS["SECT_FOOTER_COUNTERS"] = "лічильники";
$MESS["GIFT_PRICE_LABEL"] = "Безкоштовно";
$MESS["CATALOG_ECONOMY"] = "Економія:";
$MESS["WISHLIST_SENDED"] = "Відправлено";
$MESS["ADD_SUBSCRIBE_LABEL"] = "Підписатися";
$MESS["REMOVE_SUBSCRIBE_LABEL"] = "Відмінити;"
?>