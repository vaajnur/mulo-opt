<?php
return array (
  'utf_mode' => 
  array (
    'value' => true,
    'readonly' => true,
  ),
  'cache_flags' => 
  array (
    'value' => 
    array (
      'config_options' => 3600,
      'site_domain' => 3600,
    ),
    'readonly' => false,
  ),
  'cookies' => 
  array (
    'value' => 
    array (
      'secure' => true,
      'http_only' => false,
    ),
    'readonly' => false,
  ),
  'exception_handling' => 
  array (
    'value' => 
    array (
      'debug' => true,
      'handled_errors_types' => 4437,
      'exception_errors_types' => 4437,
      'ignore_silence' => false,
      'assertion_throws_exception' => true,
      'assertion_error_type' => 256,
      'log' => ['settings' => ['file' => 'error-log.txt', 'log_size' => 100000]]
    ),
    'readonly' => false,
  ),
  'connections' => 
  array (
    'value' => 
    array (
      'default' => 
      array (
        'className' => '\\Bitrix\\Main\\DB\\MysqliConnection',
        'host' => 'localhost',
        'database' => 'sitemanager',
        'login' => 'bitrix0',
        'password' => ')VD[Jp)+26H(&lxW4w%&',
        'options' => 2,
      ),
    ),
    'readonly' => true,
  ),

    'cache' =>
        array (
            'value' =>
                array (
                    'sid' => '$_SERVER["DOCUMENT_ROOT"]."#01"',
                    'type' => 'memcache',
                    'memcache' =>
                        array (
                            'host' => '127.0.0.1',
                        ),
                ),
                'readonly' => false,
    ),
    'analytics_counter'  => array(
  'value' => array(
     'enabled' => false,
  ),
),

/*  'cache' => 
  array (
    'value' => 
    array (
      'type' => 
      array (
        'class_name' => 'CPHPCacheMemcacheCluster',
        'extension' => 'memcache',
        'required_file' => 'modules/cluster/classes/general/memcache_cache.php',
      ),
    ),
    'readonly' => false,
  ),*/
);
