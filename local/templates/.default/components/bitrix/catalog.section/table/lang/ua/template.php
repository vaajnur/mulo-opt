<?
$MESS ["NEW_HEADING"] = " нові надходження";
$MESS ["ADDCART"] = " в кошик";
$MESS ["ADDSKU"] = " уточнити";
$MESS ["ADDCOMPARE"] = " порівняти";
$MESS ["ADDCOMPARED"] = " у списку порівняння";
$MESS ["AVAILABLE"] = " в наявності";
$MESS ["NOAVAILABLE"] = " Недоступно";
$MESS ["FROM"] = " від ";
$MESS ["ON_ORDER"] = " під замовлення";
$MESS ["GET_ALL_PRODUCT"] = " всі пропозиції";
$MESS ["ADDCART_LABEL"] = " в кошик";
$MESS ["FASTBACK_LABEL"] = " купити в 1 клік";
$MESS ["WISHLIST_LABEL"] = " у вибране";
$MESS ["COMPARE_LABEL"] = " до порівняння";

$MESS ["SHOW_MORE"] = " показати ще";
$MESS ["SHOWS"] = " Показано";
$MESS ["FROM"] = " з";
$MESS ["CATALOG_ART_LABEL"] = " Артикул: ";
$MESS ["CATALOG_PRICE_LABEL"] = " Ціна: ";

$MESS ["EMPTY_HEADING"] = " в даному розділі поки порожньо";
$MESS ["EMPTY_TEXT"] = 'ви можете повернутися на <a href="'.SITE_DIR.'catalog/" > сторінку каталогу</a> або скористатися навігацією або пошуком по сайту.';
$MESS ["MAIN_PAGE"] = " Головна сторінка";
$MESS ["TABLE_REQUEST_PRICE_LABEL"] = " Ціна за запитом";
$MESS ["TABLE_REQUEST_PRICE_BUTTON_LABEL"] = " запросити";