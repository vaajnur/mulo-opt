<div class="global-information-block-cn">
	<div class="global-information-block-hide-scroll">
		<div class="global-information-block-hide-scroll-cn">
			<div class="information-heading">
				Есть вопросы?
			</div>
			<div class="information-text">
				свяжитесь с нами удобным Вам способом
			</div>
			<div class="information-list">
				<div class="information-list-item">
					<div class="tb">
						<div class="information-item-icon tc">
 <img src="/bitrix/templates/dresscode/images/cont1.png">
						</div>
						<div class="tc">
							+38 (063) 6900672⁠<br>
						</div>
					</div>
				</div>
				<div class="information-list-item">
					<div class="tb">
						<div class="information-item-icon tc">
 <img src="/bitrix/templates/dresscode/images/cont2.png">
						</div>
						<div class="tc">
 <a href="mailto:mysoapoptom@gmail.com">mysoapoptom@gmail.com</a><br>
						</div>
					</div>
				</div>
				<div class="information-list-item">
					<div class="tb">
						<div class="information-item-icon tc">
 <img src="/bitrix/templates/dresscode/images/cont3.png">
						</div>
						<div class="tc">
							 1-й Песчаный тупик, 1, Кременчуг,<br>
							 Полтавская область, Украина<br>
						</div>
					</div>
				</div>
				<div class="information-list-item">
					<div class="tb">
						<div class="information-item-icon tc">
 <img src="/bitrix/templates/dresscode/images/cont4.png">
						</div>
						<div class="tc">
							 Пн-Пт : с 9:00 до 18:00<br>
							 Сб, Вс : выходной<br>
							 Call Center: Пн-Вс :<br>
							 с 8:00 до 23:00
						</div>
					</div>
				</div>
			</div>
			<div class="information-feedback-container">
 <a href="<?=SITE_DIR?>callback/" class="information-feedback">Обратная связь</a>
			</div>
		</div>
	</div>
</div>
<br>