<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Competition, raffle prizes from the online store Soap-Opt.");
$APPLICATION->SetPageProperty("keywords", "a contest, promotion, prize draw");
$APPLICATION->SetPageProperty("description", "the contest Rules \"unboxing\" from the online store Soap-Opt.");
$APPLICATION->SetTitle("Competition - unpack");
?><p class="box-container">
</p>
<div style="text-align:center">
<figure class="caption" style="display:inline-block"><img width="738" src="https://xn----utbcjbgv0e.com.ua/image/data/konkurs/raspakovka.jpg" height="327" alt=""> <figcaption></figcaption> </figure>
</div>
<h3 style= "text-align: justify; margin: 0px 0px 0.5em; padding: 0px; letter-spacing: -0.015em; line-height: 1.4em; font-size: 32px; font-family: 'Open Sans', sans-serif; text-align: center; ">Unpack your product and win prizes!</h3>
<h3 style="text-align: justify;margin: 0px 0px 0.5em; padding: 0px 0px 15px; letter-spacing: -0.015em; line-height: 1.4em; font-size: 32px; font-family:'Open Sans', sans-serif;text-align: center; color: #c91616; ">100% guaranteed promo code UAH for each unpacking, as well as the main prize - a promo code for 500 UAH to the author of the best video!*&nbsp;</h3>
<h3 style="text-align: justify;margin: 0px 0px 0.5em; padding: 0px 0px 15px; letter-spacing: -0.015em; line-height: 1.4em; font-size: 32px; font-family:'Open Sans', sans-serif;text-align: center; color: #c91616; ">New promotion: for the submitted video master class - coupon for 150 UAH !</h3>
<br>
<p style="margin:0px; padding:10px 15px; list-style:none outside none; border-color:#7cb030; font-size:22px; border-radius:10px; font-style:italic; text-align:center; background-color:#baeb72">
<span style= "color: #800000; line-height: 1.6;">To participate in the contest, you must:</span>
</p>
<p style="margin: 0px 0px 15px; padding: 0px; line-height: 1.4em; font-size: 16px; font-family: 'Open Sans', sans-serif; background-color: rgba(255, 255, 255, 0.952941);">
<span style= " font-size: 1em; line-height: 1.4em; display: table-cell;">1.</span><span style= "font-size: 1em; line-height: 1.4em; display: table-cell;">Buy the product in the store Soap-wholesale</span>
</p>
<p style="margin: 0px 0px 15px; padding: 0px; line-height: 1.4em; font-size: 16px; font-family: 'Open Sans', sans-serif; background-color: rgba(255, 255, 255, 0.952941);">
<span style= " font-size: 1em; line-height: 1.4em; display: table-cell;">2.</span><span style="font-size: 1em; line-height: 1.4em; display: table-cell;">Make a video of unpacking this product</span>
</p>
<p style="margin: 0px 0px 15px; padding: 0px; line-height: 1.4em; font-size: 16px; font-family: 'Open Sans', sans-serif; background-color: rgba(255, 255, 255, 0.952941);">
<span style="font-size: 1em; line-height: 1.4em; display: table-cell;">3.&nbsp;</span><span style="font-size: 1em; line-height: 1.4em; display: table-cell;">Demonstrate in detail the packaging, configuration and appearance of the product</span>
</p>
<p style="margin: 0px 0px 15px; padding: 0px; line-height: 1.4em; font-size: 16px; font-family: 'Open Sans', sans-serif; background-color: rgba(255, 255, 255, 0.952941);">
<span style= " font-size: 1em; line-height: 1.4em; display: table-cell;">4.</span><span style= "font-size: 1em; line-height: 1.4em; display: table-cell;">Tell your impressions about the service and delivery of the online store Мыло-опт.сом.иа</span>
</p>
<p style="margin: 0px 0px 15px; padding: 0px; line-height: 1.4em; font-size: 16px; font-family: 'Open Sans', sans-serif; background-color: rgba(255, 255, 255, 0.952941);">
<span style="font-size: 1em; line-height: 1.4em; display: table-cell;">5.&nbsp;</span><span style="font-size: 1em; line-height: 1.4em; display: table-cell;">Upload the video to your YouTube channel and send the link via a special form<br>
<br>
<b>Win-win video master class contest!</b><br>
You can send us a video master class on making soap, bath bombs, cream, tonic and other cosmetic products from the products that you purchased on our website. For the video master class sent-a coupon for 150 UAH.</span>
</p>
<p style="margin:0px; padding:10px 15px; list-style:none outside none; border-color:#7cb030; font-size:22px; border-radius:10px; font-style:italic; text-align:center; background-color:#baeb72">
<span style= "color: #800000; line-height: 1.6;">Video requirements:</span>
</p>
<p style="margin: 0px 0px 15px; padding: 0px; line-height: 1.4em; font-size: 16px; font-family: 'Open Sans', sans-serif; background-color: rgba(255, 255, 255, 0.952941);">
<span style="font-size: 1em; line-height: 1.4em; display: table-cell;">1.&nbsp;</span><span style="font-size: 1em; line-height: 1.4em; display: table-cell;">Video quality from 480p</span>
</p>
<p style="margin: 0px 0px 15px; padding: 0px; line-height: 1.4em; font-size: 16px; font-family: 'Open Sans', sans-serif; background-color: rgba(255, 255, 255, 0.952941);">
<span style= " font-size: 1em; line-height: 1.4em; display: table-cell;">2.</span><span style= "font-size: 1em; line-height: 1.4em; display: table-cell;">Voice accompaniment and clear sound — an additional plus to the author</span>
</p>
<p style="margin: 0px 0px 15px; padding: 0px; line-height: 1.4em; font-size: 16px; font-family: 'Open Sans', sans-serif; background-color: rgba(255, 255, 255, 0.952941);">
<span style="font-size: 1em; line-height: 1.4em; display: table-cell;">3.&nbsp;</span><span style="font-size: 1em; line-height: 1.4em; display: table-cell;">One video — one unboxing</span>
</p>
<p style="margin: 0px 0px 15px; padding: 0px; line-height: 1.4em; font-size: 16px; font-family: 'Open Sans', sans-serif; background-color: rgba(255, 255, 255, 0.952941);">
<span style= " font-size: 1em; line-height: 1.4em; display: table-cell;">4.</span><span style= "font-size: 1em; line-height: 1.4em; display: table-cell;">Link to the product in the description of the video on YouTube</span>
</p>
<p style="margin: 0px 0px 15px; padding: 0px; line-height: 1.4em; font-size: 16px; font-family: 'Open Sans', sans-serif; background-color: rgba(255, 255, 255, 0.952941);">
<span style="font-size: 1em; line-height: 1.4em; display: table-cell;">5.</span><span style= " font-size: 1em; line-height: 1.4em; display: table-cell;">The video must not be vertical</span>
</p>
<p style="margin: 0px 0px 15px; padding: 0px; line-height: 1.4em; font-size: 16px; font-family: 'Open Sans', sans-serif; background-color: rgba(255, 255, 255, 0.952941);">
<span style= " font-size: 1em; line-height: 1.4em; display: table-cell;">6.</span><span style= "font-size: 1em; line-height: 1.4em; display: table-cell;">The product link should be in the first place in the description under your video</span>
</p>
<p style="margin: 0px 0px 1em; padding: 0px; line-height: 1.1em; font-size: 24px; font-family: 'Open Sans', sans-serif; color: #d67d17; background-color: rgba(255, 255, 255, 0.952941);">
For high-quality video-guaranteed promo code for a discount of 100 UAH;
</p>
<p style="margin: 0px 0px 1em; padding: 0px; line-height: 1.1em; font-size: 24px; font-family: 'Open Sans', sans-serif; color: #d67d17; background-color: rgba(255, 255, 255, 0.952941);">
For high-quality video of the master class - 150 UAH
</p>
<p style="margin: 0px 0px 15px; padding: 0px; line-height: 1.4em; font-size: 16px; font-family: 'Open Sans', sans-serif; background-color: rgba(255, 255, 255, 0.952941);">
<span style= " line-height: 21.0022 px;"> The author of each video, which the editors of the site consider to be of high quality, will receive a promo code to hisemail address</span><br style= " line-height: 21.0022 px;">
<span style= "line-height: 21.0022 px;">for 100 UAH for video unpacking of the product or 150 UAH for video master class.&nbsp;</span>Only one coupon can be used per order, and the coupons are not combined.
</p>
<p style="margin: 0px 0px 1em; padding: 0px; line-height: 1.1em; font-size: 24px; font-family: 'Open Sans', sans-serif; color: #295bb4; background-color: rgba(255, 255, 255, 0.952941);">
Every 100 videos, a promo code for 500 UAH is played
</p>
<p>
<?$APPLICATION->IncludeComponent(
"bitrix:form.result.new",
"",
Array(
"CACHE_TIME" => "3600",
"CACHE_TYPE" => "A",
"CHAIN_ITEM_LINK" => "",
"CHAIN_ITEM_TEXT" => "",
"EDIT_URL" => "result_edit.php",
"IGNORE_CUSTOM_TEMPLATE" => "N",
"LIST_URL" => "result_list.php",
"SEF_MODE" => "N",
"SUCCESS_URL" => "",
"USE_EXTENDED_ERRORS" => "N",
"VARIABLE_ALIASES" => Array("RESULT_ID"=>"RESULT_ID","WEB_FORM_ID"=>"WEB_FORM_ID"),
"WEB_FORM_ID" => "3"
)
);?>
</p>
<p style="margin:0px; padding:10px 15px; list-style:none outside none; border-color:#7cb030; font-size:22px; border-radius:10px; font-style:italic; text-align:center; background-color:#baeb72">
<span style= "color:#800000;">CONTEST VIDEO</span>
</p>
<b> </b>
<table border="1" cellpadding="1" cellspacing="1" style="width:500px;" align="center">
<tbody>
<tr>
<td colspan="1" style="text-align: center;">
VIDEO #294<br>
</td>
<td colspan="1" style="text-align: center;">
VIDEO #295<br>
</td>
<td colspan="1" style="text-align: center;">
VIDEO #296<br>
</td>
</tr>
<tr>
<td colspan="1" style="text-align: center;">
<iframe title= "Soap making products from #unboxing #319 "width= "310" height="250" src= " //www.youtube.com/embed/ePjiAknWexE?feature=oembed" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="">
</iframe>
</td>
<td colspan="1" style="text-align: center;">
		 <iframe title="#Мыло_ручной_ #косметика ? Для чего все эти компоненты? "width =" 310 " height =" 250 " src="//www.youtube.com/embed/VSW8_doRct8?feature=oembed" frameborder="0" allow="акселерометр; автозапуск; зашифрованный носитель; гироскоп; картинка в картинке" allowfullscreen="">
		</iframe>
	</td>
	<td colspan="1" style="text-align: center;">
		 <iframe title="Полный стол товаров для мыла ручной работы / / # Распаковка 296" width = " 310 " height =" 250 " src="//www.youtube.com/embed/xGW-wNJwgWo?feature=oembed" frameborder="0" allow="акселерометр; автозапуск; зашифрованный носитель; гироскоп; картинка в картинке" allowfullscreen="">
		</iframe>
	</td>
</tr>
<tr>
	<td colspan="1" style="text-align: center;">
		 ВИДЕО № 297 <br>
	</td>
	<td colspan="1" style="text-align: center;">
		 ВИДЕО № 298 <br>
	</td>
	<td colspan="1" style="text-align: center;">
		 ВИДЕО № 299 <br>
	</td>
</tr>
<tr>
	<td colspan="1" style="text-align: center;">
		 <iframe title="Товары для цветов из мыла / / Распаковываем посылку из Мыло Опт" width = " 310 " height =" 250 " src="//www.youtube.com/embed/w8bJJzX1WSc?feature=oembed" frameborder="0" allow="акселерометр; автозапуск; зашифрованный носитель; гироскоп; картинка в картинке" allowfullscreen="">
		</iframe>
	</td>
	<td colspan="1" style="text-align: center;">
		 <iframe title="Все для # diy # crafts - #мыловарение / / Распаковка №298 "width =" 310 " height = "250" src="//www.youtube.com/embed/3tfYVOFrx2U?feature=oembed" frameborder="0" allow="акселерометр; автозапуск; зашифрованный носитель; гироскоп; картинка в картинке" allowfullscreen="">
		</iframe>
	</td>
	<td colspan="1" style="text-align: center;">
		 <iframe title="#SPA и #DIY все в одной коробке / / Распаковка № 299 "width =" 310 " height = "250" src="//www.youtube.com/embed/hOQSr0cqXe4?feature=oembed" frameborder="0" allow="акселерометр; автозапуск; зашифрованный носитель; гироскоп; картинка в картинке" allowfullscreen="">
		</iframe>
	</td>
</tr>
<tr>
	<td colspan="1" style="text-align: center;">
		 ВИДЕО № 300 <br>
	</td>
	<td colspan="1" style="text-align: center;">
		 ВИДЕО № 301 <br>
	</td>
	<td colspan="1" style="text-align: center;">
		 ВИДЕО № 302 <br>
	</td>
</tr>
<tr>
	<td colspan="1" style="text-align: center;">
		 <iframe title="#Распаковка № 300 посылки # soapcrafter // Мыло ручной работы "width =" 310 " height =" 250 " src="//www.youtube.com/embed/YOF2zDJfryw?feature=oembed" frameborder="0" allow="акселерометр; автозапуск; зашифрованный носитель; гироскоп; картинка в картинке" allowfullscreen="">
		</iframe>
	</td>
	<td colspan="1" style="text-align: center;">
		 <iframe title="#Распаковка № 301-заказ от магазина мыловарения "width =" 310 " height =" 250 " src ="//www.youtube.com/embed/Fj4_XYUbigo?feature=oembed" frameborder="0" allow="акселерометр; автозапуск; зашифрованный носитель; гироскоп; картинка в картинке" allowfullscreen="">
		</iframe>
	</td>
	<td colspan="1" style="text-align: center;">
		 <iframe title="Довольный # покупатель с заказом из магазина Мыло Опт" width = " 310 " height=" 250" src="//www.youtube.com/embed/Gm9owYJ_Z80?feature=oembed" frameborder="0" allow="акселерометр; автозапуск; зашифрованный носитель; гироскоп; картинка в картинке" allowfullscreen="">
		</iframe>
	</td>
</tr>
<tr>
	<td colspan="1" style="text-align: center;">
		 ВИДЕО № 303 <br>
	</td>
	<td colspan="1" style="text-align: center;">
		 ВИДЕО № 304 <br>
	</td>
	<td colspan="1" style="text-align: center;">
		 ВИДЕО № 305 <br>
	</td>
</tr>
<tr>
	<td colspan="1" style="text-align: center;">
		 <iframe title="#Киев # unboxing № 303 заказ полезных товаров от Мыло Опт "width =" 310 " height =" 250 " src ="//www.youtube.com/embed/tzL6NCetp4c?feature=oembed" frameborder="0" allow="акселерометр; автозапуск; зашифрованный носитель; гироскоп; картинка в картинке" allowfullscreen="">
		</iframe>
	</td>
	<td colspan="1" style="text-align: center;">
		 <iframe title="Все для свечного дела и # DIY / / Распаковка № 304 "width =" 310 " height =" 250 " src="//www.youtube.com/embed/RqbcMtzJxsU?feature=oembed" frameborder="0" allow="акселерометр; автозапуск; зашифрованный носитель; гироскоп; картинка в картинке" allowfullscreen="">
		</iframe>
	</td>
	<td colspan="1" style="text-align: center;">
		 <iframe title="#Мыловар Кокетка Грі - # распаковка № 305 "width =" 310 " height = "250" src = "//www.youtube.com/embed/2q1i6Vx23TE?feature=oembed" frameborder="0" allow="акселерометр; автозапуск; зашифрованный носитель; гироскоп; картинка в картинке" allowfullscreen="">
		</iframe>
	</td>
</tr>
</tbody>
</table>
<p style="text-align: justify;">
</p>
<p span="">
	<!--noindex--><a rel="nofollow" href="https://мыло-опт.com.ua/konkurs-raspakovka/"></a><!--/noindex--></p>
<p style="text-align: center;">
	<!--noindex--><a rel="nofollow" href="https://мыло-опт.com.ua/konkurs-raspakovka/"><span style="font-family: Arial, Helvetica; font-size: 14pt;">1</span></a><!--/noindex--><span style="font-family: Arial, Helvetica; font-size: 14pt;">&nbsp; &nbsp;</span> 
	<!--noindex--><a rel="nofollow" href="https://мыло-опт.com.ua/konkurs-raspakovka-page-2/"><span style="font-family: Arial, Helvetica; font-size: 14pt;">2</span></a><!--/noindex--><span style="font-family: Arial, Helvetica; font-size: 14pt;"></span><span style="font-family: Arial, Helvetica; font-size: 14pt;"> </span> 
	<!--noindex--><a rel="nofollow" href="https://мыло-опт.com.ua/konkurs-raspakovka-page-3/"><span style="font-family: Arial, Helvetica; font-size: 14pt;">3</span></a><!--/noindex--><span style="font-family: Arial, Helvetica; font-size: 14pt;">&nbsp; &nbsp;</span><a href="https://мыло-опт.com.ua/konkurs-raspakovka-page-4/"><span style="font-family: Arial, Helvetica; font-size: 14pt;">4</span></a><span style="font-family: Arial, Helvetica; font-size: 14pt;">&nbsp;&nbsp;</span><span style="font-family: Arial, Helvetica; размер шрифта: 14pt;"> </span><a href="https://мыло-опт.com.ua/konkurs-raspakovka-page-5/"><span style="font-family: Arial, Helvetica; font-size: 14pt;">5</span></a><span style="font-family: Arial, Helvetica; font-size: 14pt;"></span><span style="font-family: Arial, Helvetica; font-size: 14pt;"> </span><a href="https://мыло-опт.com.ua/konkurs-raspakovka-page-6/"><span style="font-family: Arial, Helvetica; font-size: 14pt;">6</span></a><span style="font-family: Arial, Helvetica; font-size: 14pt;"></span><span style="font-family: Arial, Helvetica; font-size: 14pt;"> </span> 
	<!--noindex--><a rel="nofollow" href="https://мыло-опт.com.ua/konkurs-raspakovka-page-7/"><span style="font-family: Arial, Helvetica; font-size: 14pt;">7</span></a><!--/noindex--><span style="font-family: Arial, Helvetica; font-size: 14pt;"></span><span style="font-family: Arial, Helvetica; font-size: 14pt;"> </span><span style="font-family: Arial, Helvetica; font-size: 14pt;">
	<!--noindex--><a rel="nofollow" href="https://мыло-опт.com.ua/konkurs-raspakovka-page-8/">8</a><!--/noindex-->&nbsp;&nbsp;</span><span style="font-family: Arial, Helvetica; font-size: 14pt;"> </span> 
	<!--noindex--><a rel="nofollow" href="https://мыло-опт.com.ua/konkurs-raspakovka-page-9/"><span style="font-family: Arial, Helvetica; font-size: 14pt;">9</span></a><!--/noindex--><span style="font-family: Arial, Helvetica; font-size: 14pt;"></span><span style="font-family: Arial, Helvetica; font-size: 14pt;"> </span> <span style="font-family: Arial, Helvetica; font-size: 14pt;">
	<!--noindex--><a rel="nofollow" href="https://мыло-опт.com.ua/konkurs-raspakovka-page-10/">10</a><!--/noindex-->&nbsp;</span> 
	<!--noindex--><a rel="nofollow" href="https://мыло-опт.com.ua/konkurs-raspakovka-page-11/"><span style="font-family: Arial, Helvetica; font-size: 14pt;">11</span></a><!--/noindex--><!--noindex--><a rel="nofollow" href="https://мыло-опт.com.ua/konkurs-raspakovka-page-12/"><span style="font-family: Arial, Helvetica; font-size: 14pt;"> 12</span></a><!--/noindex--><!--noindex--><a rel="nofollow" href="https://мыло-опт.com.ua/konkurs-raspakovka-page-13/"><span style="font-family: Arial, Helvetica; размер шрифта: 14pt;"> 13</span></a><!--/noindex--><!--noindex--><a rel="nofollow" href="https://мыло-опт.com.ua/konkurs-raspakovka-page-14/"><span style="font-family: Arial, Helvetica; font-size: 14pt;"> 14</span></a><!--/noindex--></p>
<p>
</p>
<p style="text-align: justify;">
 <span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;">Лучшие видео отзывы будут размещены на Youtube канале магазина "Мыло-опт" </span></span>
</p>
<p style="text-align: justify;">
	 После передачи видео , права на видео материалы принадлежат компании Мыло Опт. Убедительная просьба не накладывать свою музыку на видео материалы.
</p>
<p>
 <span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;">* Бонусные средства могут быть использованы на покупку товаров в интернет-магазине "Мыло-опт" в течении двух месяцев с момента получения бонусного кода. </span> </span>
</p>
<p style="text-align: justify;">
 <span стиль="шрифт-размер:14px;"><span стиль="семейство шрифтов:Arial,шрифт Helvetica,без засечек;">Персональные данные пользователей принявших участие в конкурсе не предоставляются третьим лицам, но сохраняются администраторами интернет-магазина «Мыло-опт» для зачисления выигранных участником конкурса бонусных средств на личный счет пользователя Сайта и для предоставления услуг продажи товаров, представленных на Сайте. Организатор конкурса &nbsp; оставляет за собой право использовать полученные от участников акции видеоматериалы в маркетинговых целях. </span></span>
</p>
<div class="buttons">
 <a href="https://xn----utbcjbgv0e.com.ua/en/" class="button-cont-right">Продолжить <i class = "icon-circle-arrow - right"></i></a><br>
</div>
<p>
</p>
 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>