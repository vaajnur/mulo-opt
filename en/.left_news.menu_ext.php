<? 
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); 
global $APPLICATION;
$aMenuLinksExt = $APPLICATION->IncludeComponent(
	"dresscode:menu.sections", 
	"", 
	array(
		"IBLOCK_TYPE" => "info",
		"IBLOCK_ID" => "9",
		"DEPTH_LEVEL" => "3",
		"CACHE_TYPE" => "N",
		"CACHE_TIME" => "0",
		"IS_SEF" => "N",
		"ID" => "",
		"SECTION_URL" => "/news/#CODE#/"
	),
	false
); 
$aMenuLinks = $aMenuLinksExt; 
?>