<span class="heading">Contacts</span>
<ul class="information">
<li><span class="label">Phone:</span> <span class="inf"> 8 800 500-60-70</span></li>
<li><span class="label">Email:</span> <span class="inf"><a href="mailto:sale@dresscode.ru">sale@dresscode.ru</a></span></li>
<li><span class="label">Address:</span> <span class="inf">Moscow, Kalinin Avenue < br />house 5, room 55</span></li>
<li><span class="label"></span><span class="inf"><a href="<?=SITE_DIR?>contacts/" class="goContact">View on map</a></span></li>
</ul>