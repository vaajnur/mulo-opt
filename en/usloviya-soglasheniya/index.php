<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "the terms of the agreement online store Soap opt.");
$APPLICATION->SetPageProperty("keywords", "the terms of the agreement, General terms, conditions, ordering, shipping.");
$APPLICATION->SetPageProperty("description", "agreement online store Soap-Wholesale, which govern the ordering, sod...");
$APPLICATION->SetTitle("the agreement");
?><h1 class= "style-1" >Terms of Agreement</h1>
<div class="box-container">
<h2>1. General provisions</h2>
<p style="text-align: justify;">
<span style= "font-size:14px;" > <span style= " font-family:arial, helvetica,sans-serif;" > 1.1. This Agreement is valid between the Buyer and the site "Soap - WHOLESALE" in the person of the store manager at the time of the order.</span></span>
</p>
<p style="text-align: justify;">
<span style= "font-size:14px;" > <span style= " font-family:arial, helvetica,sans-serif;" > 1.2. At the time of ordering, the Buyer agrees to the contract stipulated in the Agreement of our contract. The focus of our contract for customers is the location of the order through our website "Soap-WHOLESALE".</span></span>
</p>
<p style="text-align: justify;">
<span style= "font-size:14px;" > <span style= " font-family:arial, helvetica,sans-serif;" > 1.3.Any individual or legal entity that is able to pay and accept the cost of the product can make a purchase in the Online store.</span></span>
</p>
<p style="text-align: justify;">
<span style= "font-size:14px;" > <span style= " font-family:arial, helvetica,sans-serif;" > 1.4. Dear customers, please note that quality certificates for products are issued only to wholesale customers after payment of the order, upon individual request. No passports are issued for retail items. The deadline for requesting a quality passport is no more than 14 days after the buyer completes and pays for the order.</span></span>
</p>
<p style="text-align: justify;">
<span style= "font-size:14px;" > <span style= "font-family:arial, helvetica,sans-serif;" >1.5. Please note that all photos presented on this site are intended for informational purposes, more detailed information about the product can be found in the product description on the product page.</span></span>
</p>
<p style="text-align: justify;">
<span style= "font-size:14px;" > <span style= " font-family:arial, helvetica, sans-serif;" > 1.6. The texts of product descriptions are compiled from free sources,</span></span> <span style="font-family: arial, helvetica, sans-serif; font-size: 14px;">product documents, recipes proposed by the authors and users of the site.</span><span style= " font-family: arial, helvetica, sans-serif; font-size: 14px;">Information contained in the product description, the given methods of use,</span><span style= " font-family: arial, helvetica, sans-serif; font-size: 14px; "> the listed product properties are not exhaustive, they are for informational purposes only.</span> <span style= " font-family: arial, helvetica, sans-serif; font-size: 14px;" > character.</span>
</p>
<p style="text-align: justify;">
<span style= " font-family: arial, helvetica, sans-serif; font-size: 14px;">1.7. The functionality of adding products to the Favorites tab is available only to authorized users of the site.</span>
</p>
<p style="text-align: justify;">
<span style= " font-family: arial, helvetica, sans-serif; font-size: 14px;" > 1.8. Please note: The expiration dates indicated on the website may not coincide with the dates indicated on the label of the delivered product because due to high demand, information about the expiration dates of a new batch of goods is updated faster than the shipment of goods ordered from the previous batch or vice versa.</span>
</p>
<p style="text-align: justify;">
<span style= " font-family: arial, helvetica, sans-serif; font-size: 14px;">1.9. Information about using the tool and online consultation is for informational purposes and general recommendations. Our company is not responsible for the use of the product at home.</span>
</p>
<h2>2. Terms of placing an Order</h2>
<p style="text-align: justify;">
<span style= "font-size:14px;" > <span style= " font-family:arial, helvetica,sans-serif;" > 2.1. When placing an Order, it is advisable to register in our store, it is also possible to place an order without registration, with payment upon receipt of the goods.</span></span>
</p>
<p style="text-align: justify;">
<span style= "font-size:14px;" > <span style= " font-family:arial, helvetica,sans-serif;" > 2.2. The customer can place an order in the following way: order by phone or place it independently on the website. We accept orders online around the clock. Order processing is carried out during working hours from 9:00 to 18:00 from Monday to Friday.</span></span>
</p>
<p style="text-align: justify;">
<span style= "font-size:14px;" > <span style= "font-family:arial, helvetica, sans-serif;" >2.3. Before placing an order, it is mandatory to familiarize the Customer with the main</span></span> <span style="font-family: arial, helvetica, sans-serif; font-size: 14px;">product characteristics that are indicated under the description.</span>
</p>
<p style="text-align: justify;">
<span style= "font-size:14px;" > <span style= " font-family:arial, helvetica,sans-serif;" > 2.4. After the Order is placed, we provide the Customer with information about the availability of the product and the estimated delivery time. The terms of formation, dispatch and delivery depend on: the transfer of money to the account, the number of items of Goods ordered by the client, as well as on the logistics of the Transport Company. Refunds after payment for the goods on the order are not carried out, make all decisions before paying for the order, carefully read the terms of the agreement and shipment.</span></span>
</p>
<p style="text-align: justify;">
<span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;">2.5 The Store has the right to refuse the buyer to accept / process the order without giving any reasons.</span></span>
</p>
<p style="text-align: justify;">
<span style= "font-size:14px;" > <span style= "font-family:arial, helvetica,sans-serif;" >2.6 You can specify your wishes for the color of the container in the comments to the order and we will definitely listen to them if this color is available.</span></span>
</p>
<h2>3. Delivery</h2>
<p style="text-align: justify;">
<span style= "font-size:14px;" > <span style= " font-family:arial, helvetica,sans-serif;" > 3.1. It is carried out by all means convenient for the Customer with their subsequent payment. </span></span>
</p>
<p style="text-align: justify;">
<span style= "font-size:14px;" > <span style= " font-family:arial, helvetica,sans-serif;" > 3.2. In Ukraine, delivery to the place of shipment of the goods is free of charge. A detailed description of the methods and conditions of delivery can be found on the page "Payment and delivery".</span></span>
</p>
<p style="text-align: justify;">
<span style= "font-size:14px;" > <span style= "font-family:arial, helvetica,sans-serif;" >3.3. The delivery time of the order depends on your region and address, as well as the work of the delivery service.&nbsp;</span></span>
</p>
<p style="text-align: justify;">
<span style= "font-size:14px;" > <span style= "font-family:arial, helvetica,sans-serif;" >3.4. Legal entities are shipped within 2-3 business days for 100 % prepayment, after the money is credited to the company's current account. Please note that it may take from 3 to 5 business days for money to be credited to your bank account by bank transfer.&nbsp;</span></span>
</p>
<p style="text-align: justify;">
<span style= "font-size:14px;" > <span style= "font-family:arial, helvetica,sans-serif;" >3.5 Glass and beech baskets are sent in a separate place and their delivery is paid by the buyer.</span></span>
</p>
<p style="text-align: justify;">
<span style= "font-size:14px;" > <span style= "font-family:arial, helvetica,sans-serif;" >3.6. When you receive the goods on hand from the delivery services, you must carefully check the integrity and availability of the goods on the transport company. </span></span>
</p>
<p style="text-align: justify;">
<span style= "font-size:14px;" > <span style= "font-family:arial, helvetica,sans-serif;" >3.7. With free delivery, all cargo is automatically insured for the minimum amount, according to the terms of the transport company agreement. If you want to insure the goods for the full price of the order, you must notify us about this, in which case the delivery will be carried out at the expense of the buyer.&nbsp;</span></span>
</p>
<p style="text-align: justify;">
<span style= "font-size:14px;" > <span style= "font-family:arial, helvetica,sans-serif;" >3.8 In the event of a shortage or damage to the cargo, a claim report is drawn up with the employee of the new mail at the branch of the transport company. Without this document, claims are not considered.&nbsp;</span></span>
</p>
<p style="text-align: justify;">
<span style= "font-size:14px;" > <span style= " font-family:arial,helvetica,sans-serif;">3.9 Attention! All silicone molds, molds and stamps are made to order, which may cause delays in shipping.&nbsp;</span></span>Refunds after payment of the goods that are made to order after payment is not carried out, all decisions are made before paying for the order, carefully read the terms of the agreement and shipment.
</p>
<p style="text-align: justify;">
<span style= "font-size:14px;" > <span style= "font-family:arial, helvetica,sans-serif;" >3.10 All products are packaged in containers only for transportation, not for storage.</span></span>
</p>
<p style="text-align: justify;">
<span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> <strong> We are responsible for the goods before loading to the warehouse of the delivery service, for further fate The goods are the responsibility of the organization delivering the goods to the destination. &nbsp; </strong> The goods are delivered in containers and packaging for transportation. Basic &nbsp; </span> </span> <span style = "font-family: arial, helvetica, sans-serif; font-size: 14px;"> product characteristics are indicated under each product and are not additionally duplicated &nbsp; </span> <span style = "font-family: arial, helvetica, sans-serif; font-size: 14px;"> on label. </span>
</p>
<h2> 4. Payment </h2>
<p style = "text-align: justify;">
 <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> 4.1. The price for the ordered Product is indicated on the website per unit. </span> </span>
</p>
<p style = "text-align: justify;">
 <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> 4.2. There is a flexible system of discounts for wholesale buyers. </span> </span>
</p>
<p style = "text-align: justify;">
 <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> Types of bonuses and discounts are subject to change. </span> </span>
</p>
<p style = "text-align: justify;">
 <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> 4.3. Payment is made by bank transfer or cash. By bank transfer, it is necessary to make 100% payment to the account of the "Soap-opt.com.ua" store or send payment to the company details indicated in the invoice and inform us in writing about the payment, &nbsp; indicating the exact amount and order number. & Nbsp ; Please note that crediting money to &nbsp; legal &nbsp; persons by bank transfer to a current account can take from 3 to &nbsp; 5 business days. </span> </span>
</p>
<p>
 <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> 4.4. The invoice is valid for 24 hours. </span> </span>
</p>
<p style = "text-align: justify;">
 <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> The goods of our store fall under the regulation of 03.19.94 N 172, in this regard, we we do not make an exchange and return of goods, &nbsp; in case of force majeure, it is carried out at the expense of the buyer. Goods purchased at promotional prices, custom made or at a discount cannot be returned or exchanged. &nbsp; </span> </span>
</p>
<p style = "text-align: justify;">
 <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> <strong> Please note </strong> that under the terms of our cooperation with <strong> New mail </strong> parcels for shipment are accepted in the Open form, to ensure its integral and safe transportation. &nbsp; <br>
We kindly ask you to check the parcels upon receipt, both paid and cash on delivery orders. We ask you to record all discrepancies in the consignment note in the act of acceptance of the goods, &nbsp; so that in case of any questions we can immediately understand this situation. &nbsp; </span> </span>
</p>
<h2> Claims and termination of the agreement. </h2>
<p>
 <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> Claims for the goods are accepted by phone and using electronic &nbsp; </span> </ span> <span style = "font-family: arial, helvetica, sans-serif; font-size: 14px;"> messages, mail correspondence. The procedure for filing and considering claims of individuals &nbsp; </span> <span style = "font-family: arial, helvetica, sans-serif; font-size: 14px;"> and / or legal entities, and termination of the agreement is carried out in accordance with the norms &nbsp; </span> <span style = "font-family: arial, helvetica, sans-serif; font-size: 14px;"> Civil, Commercial Codes of Ukraine and the Law of Ukraine "On Rights &nbsp; </span> <span style =" font -family: arial, helvetica, sans-serif; font-size: 14px; "> consumers". Promotional goods cannot be returned or exchanged. </span>
</p>
<p style = "text-align: justify;">
 <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> We hope for your understanding &nbsp; and &nbsp; assistance! </span> </span>
</p>
<div class = "buttons">
<div class = "right">
 <a href="https://xn----utbcjbgv0e.com.ua/en/" class="button-cont-right"> Continue <i class = "icon-circle-arrow-right"> </i> </a>
</div>
</div>
</div>
 <br> <? require($_SERVER["DOCUMENT_ROOT"]. "/bitrix/footer.php");?>