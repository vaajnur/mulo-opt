<?
require ($_SERVER["DOCUMENT_ROOT"]. "/bitrix/header.php");
$APPLICATION->SetPageProperty ("title", "Magnets for real soapmakers");
$APPLICATION->SetPageProperty ("keywords", "Magnets for real soapmakers");
$APPLICATION->SetPageProperty ("description", "Magnets for real soapmakers");
$APPLICATION->SetTitle ("Magnets for real soapmakers");
?> <h1 class = "style-1"> Real soapmaker magnets </h1>
<div class = "box-container" style = "max-width: 100%; overflow: auto;">
<table align = "center" cellpadding = "1" cellspacing = "1" style = "width: 800px;">
<tbody>
<tr>
<td>
 <figure class = "caption" style = "float: left"> <img width = "235" alt = "Good luck 2017!" src = "https://xn----utbcjbgv0e.com.ua/image/data/magniti/magnit17.jpg" height = "235"> <figcaption> Lucky 2017! </figcaption> </figure>
</td>
<td>
 <figure class = "caption" style = "float: left"> <img width = "235" alt = "Happy 2017!" src = "https://xn----utbcjbgv0e.com.ua/image/data/magniti/magnit16.jpg" height = "236"> <figcaption> Get lost in happiness 2017! </figcaption> </figure>
</td>
<td>
 <figure class = "caption" style = "float: left"> <img width = "234" alt = "Love 2017!" src = "https://xn----utbcjbgv0e.com.ua/image/data/magniti/magnit15.jpg" height = "234"> <figcaption> Fall into love 2017! </figcaption> </figure>
</td>
</tr>
<tr>
<td>
 <figure class = "caption" style = "float: left"> <img width = "234" alt = "Everything for the bath" src = "https://xn----utbcjbgv0e.com.ua/image/data/magniti/magnit13.jpg " height =" 231 "> <figcaption> All for the bath </figcaption> </figure>
</td>
<td>
 <figure class = "caption" style = "float: left"> <img width = "177" alt = "Magnet - Happy New Year with new soap" src = "https://xn----utbcjbgv0e.com.ua/image/data/magniti/magnit7.jpg" height =" 174 "> <figcaption> Magnet - With new soap in the new year </figcaption> </figure>
</td>
<td>
 <figure class = "caption" style = "float: left"> <img width = "234" src = "https://xn----utbcjbgv0e.com.ua/image/data/magniti/magnit12.jpg" height = "234" alt = "SoapCreation"> <figcaption> SoapCaption </figcaption> </figure>
</td>
</tr>
<tr>
<td>
 <figure class = "caption" style = "float: left"> <img width = "234" alt = "We love soap too!" src = "https://xn----utbcjbgv0e.com.ua/image/data/magniti/magnit11.jpg" height = "235"> <figcaption> We love soap too </figcaption> </figure>
</td>
<td>
 <figure class = "caption" style = "float: left"> <img width = "239" alt = "We all love soap!" src = "https://xn----utbcjbgv0e.com.ua/image/data/magniti/magnit10.jpg" height = "235"> <figcaption> We all love soap </figcaption> </figure>
</td>
<td>
 <figure class = "caption" style = "float: left"> <img width = "234" alt = "Soap for beloved ladies" src = "https://xn----utbcjbgv0e.com.ua/image/data/magniti/magnit9.jpg " height =" 235 "> <figcaption> Soap for beloved ladies </figcaption> </figure>
</td>
</tr>
<tr>
<td>
 <figure class = "caption" style = "float: left"> <img width = "239" alt = "My favorite soap" src = "https://xn----utbcjbgv0e.com.ua/image/data/magniti/magnit8.jpg " height =" 238 "> <figcaption> My Favorite Soap </figcaption> </figure>
</td>
<td>
 <figure class = "caption" style = "float: left"> <img width = "233" alt = "Magnet - I love making soap" src = "https://xn----utbcjbgv0e.com.ua/image/data/magniti/magnit1.jpg " height =" 236 "> <figcaption> Magnet - I love making soap </figcaption> </figure>
</td>
<td>
 <figure class = "caption" style = "float: left"> <img width = "232" alt = "Magnet - I like soap" src = "https://xn----utbcjbgv0e.com.ua/image/data/magniti/magnit2.jpg " height =" 169 "> <figcaption> Magnet - I like soap </figcaption> </figure>
</td>
</tr>
<tr>
<td>
 <figure class = "caption" style = "float: left"> <img width = "240" alt = "Magnet - Bubbles" src = "https://xn----utbcjbgv0e.com.ua/image/data/magniti/magnit3.jpg " height =" 175 "> <figcaption> Soap Bubbles Magnet </figcaption> </figure>
</td>
<td>
 <figure class = "caption" style = "float: left"> <img width = "234" alt = "Magnet - Kittens like to wash too" src = "https://xn----utbcjbgv0e.com.ua/image/data/magniti/magnit4.jpg " height =" 170 "> <figcaption> Magnet - Kittens like to wash too </figcaption> </figure>
</td>
<td>
 <figure class = "caption" style = "float: left"> <img width = "235" alt = "Magnet - Made by hand" src = "https://xn----utbcjbgv0e.com.ua/image/data/magniti/magnit5.jpg" height =" 170 "> <figcaption> Magnet - Made by hand </figcaption> </figure>
</td>
</tr>
<tr>
<td>
<img width = "188" alt = "Sweet Life Magnet (1) .jpg" src = "/upload/medialibrary/160/16073979c15393973dde4f2d3be10e22.jpg" height = "240" title = "Sweet Life Magnet (1). jpg "align =" middle "> <br>
Magnet - Sweet life <br>
</td>
<td>
 <figure class = "caption" style = "float: left"> <img width = "240" alt = "Magnet - Moidodyr" src = "https://xn----utbcjbgv0e.com.ua/image/data/magniti/magnit6.jpg" height =" 174 "> <figcaption> Magnet - Moidodyr </figcaption> </figure>
</td>
<td>
<img width = "240" alt = "NG2018.jpg" src = "/upload/medialibrary/7f7/7f7ade7e5795bf32fa4d47554eca8c7d.jpg" height = "240" title = "NG2018.jpg" align = "middle"> <br >
Magnet - The wonderful world of soap makingi <br>
</td>
</tr>
<tr>
<td>
<img width = "240" alt = "Magnetic 3 (1) .jpg" src = "/upload/medialibrary/0fd/0fd78a5600c1be19c5bdd1f0d7e60dc7.jpg" height = "240" title = "Magnetic 3 (1) .jpg" align = "middle"> <br>
Magnet - This foam is just a miracle! <br>
</td>
<td>
<img width = "240" alt = "NG2018 (1) .jpg" src = "/upload/medialibrary/c9e/c9e60a955fc73d0f75bc2987803ac0fa.jpg" height = "240" title = "NG2018 (1) .jpg" align = "middle"> <br>
Magnet - Happy Soap Year! <br>
</td>
<td>
</td>
</tr>
</tbody>
</table>
<div class = "buttons">
<div class = "right">
  <a href="https://xn----utbcjbgv0e.com.ua/" class="button-cont-right"> Continue <i class = "icon-circle-arrow-right"> </i> </a>
</div>
</div>
</div>
  <br> <? require ($_SERVER["DOCUMENT_ROOT"]. "/bitrix/footer.php");?>