<?
require ($_SERVER ["DOCUMENT_ROOT"]. "/bitrix/header.php");
$APPLICATION->SetPageProperty ("title", "Soap-wholesale Shop Security Policy");
$APPLICATION->SetPageProperty ("keywords", "Security Policy");
$APPLICATION->SetPageProperty ("description", "Security Policy");
$APPLICATION->SetTitle ("Security Policy of the Soap-wholesale online store");
?> <h1 class = "style-1"> Security Policy of the online store Soap-wholesale </h1>
<div class = "box-container">
<p style = "text-align: justify;">
 <span style = "font-family: arial, helvetica, sans-serif;"> <span style = "font-size: 14px;"> Soap-opt company pays great attention to the security and protection of personal data of users of our website. Visitors have the opportunity to view a large number of site pages, while not giving any information about themselves. We want to offer you security principles that explain in an accessible way how your personal data will be collected. As well as their use, which you will learn during registration. We recommend that you carefully read the text of this privacy statement. </span> </span>
</p>
<p style = "text-align: justify;">
 <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> Managers of the Soap-opt.com.ua online store guarantee full provision of information received from registered users. </span> </span>
</p>
<p style = "text-align: justify;">
 <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> Information provided during registration is stored in a private database. Our shop "Soap - OPT com.ua" guarantees confidentiality while using orders of your information. </span> </span>
</p>
<p style = "text-align: justify;">
 <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> Your data is necessary for us to contact, as well as to deliver goods according to the specified contact information ... In addition, after registering on the site, you get full access to information and news of our store, such as promotions, sales, new goods, bonus programs and much more. </span> </span>
</p>
<p style = "text-align: justify;">
 <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> If you have any questions, or any difficulties with your orders - write to the email address. mail &nbsp; </span> </span>

 <a href="mailto:mysoapoptom@gmail.com"> mysoapoptom@gmail.com </a> &nbsp; we can quickly find a solution to your questions!
</p>
<p style = "text-align: justify;">
 <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> Objects posted on our site become the property of the Soap-wholesale online store. It is prohibited to use such objects without the consent of the administration of the online store. </span> </span>
</p>
<h2> Personal information and security </h2>
<ol>
<li>
<p style = "text-align: justify;">
 <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> Soap-opt.com.ua gives a 100% guarantee that the information you provided will never be shared with third parties. </span> </span>
</p>
 </li>
<li>
<p style = "text-align: justify;">
 <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> Soap-opt.com.ua may request registration to provide personal information. The information will be used to process the order in the online store. </span> </span>
</p>
 </li>
<li>
<p style = "text-align: justify;">
 <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> You can change or update your personal data, if necessary, you can delete it at your convenience time, in the "Personal Account" section. </span> </span>
</p>
 </li>
<li>
<p style = "text-align: justify;">
 <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> Information on this site is for informational purposes only. </span> </span>
</p>
 </li>
</ol>
<p style = "text-align: justify;">
 <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> Cooperating with us, you will gain the security and reliability that is so difficult to find in our time. We have been working in the market of Ukraine and Russia for many years, which allowed us to create a base of grateful clients, far beyond the borders of Ukraine and the former CIS. </span> </span>
</p>
<div class = "buttons">
<div class = "right">
 <a href="https://xn----utbcjbgv0e.com.ua/" class="button-cont-right"> Continue <i class = "icon-circle-arrow-right"> </i> </a>
</div>
</div>
</div>
 <br>
<div style = "text-align: center;">
</div>
 &nbsp; &nbsp; <br> <? require ($_SERVER ["DOCUMENT_ROOT"]. "/bitrix/footer.php");?>