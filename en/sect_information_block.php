<div class="global-information-block-cn">
	<div class="global-information-block-hide-scroll">
		<div class="global-information-block-hide-scroll-cn">
			<div class="information-heading">
				Any questions?
			</div>
			<div class="information-text">
				contact us in a convenient way
			</div>
			<div class="information-list">
				<div class="information-list-item">
					<div class="tb">
						<div class="information-item-icon tc">
 <img src="/bitrix/templates/dresscode/images/cont1.png">
						</div>
						<div class="tc">
							+38 (063) 6900672⁠<br>
						</div>
					</div>
				</div>
				<div class="information-list-item">
					<div class="tb">
						<div class="information-item-icon tc">
 <img src="/bitrix/templates/dresscode/images/cont2.png">
						</div>
						<div class="tc">
 <a href="mailto:mysoapoptom@gmail.com">mysoapoptom@gmail.com</a><br>
						</div>
					</div>
				</div>
				<div class="information-list-item">
					<div class="tb">
						<div class="information-item-icon tc">
 <img src="/bitrix/templates/dresscode/images/cont3.png">
						</div>
						<div class="tc">
							 1st Sandy dead end, 1, Kremenchug,<br>
							 Poltava region, Ukraine<br>
						</div>
					</div>
				</div>
				<div class="information-list-item">
					<div class="tb">
						<div class="information-item-icon tc">
 <img src="/bitrix/templates/dresscode/images/cont4.png">
						</div>
						<div class="tc">
							 Mon-Fri: from 9: 00 to 18: 00<br>
							 Sat, Sun: weekend<br>
							 Call Center: Mon-Sun: <br>
							 from 8:00 to 23: 00
						</div>
					</div>
				</div>
			</div>
			<div class="information-feedback-container">
 <a href="<?=SITE_DIR?>callback/" class="information-feedback" >feedback</a>
			</div>
		</div>
	</div>
</div>
<br>