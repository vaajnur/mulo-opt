<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->setTitle ("Personal Data Processing Agreement");?>

<h1>Personal data Processing Agreement</h1>
<h3>By submitting their personal data, the Buyer agrees to the processing, storage and use of their personal data on the basis of Federal Law No. 152-FZ" On Personal Data " of 27.07.2006 for the following purposes::</h3>

<ul>
	<li>User registration on the site</li>
	<li>Implementation of customer support</li>
	<li>Getting information about marketing events by the User</li>
	<li>Fulfillment of obligations by the Seller to the Buyer</li>
	<li>Conducting audits and other internal research to improve the quality of the services provided.</li>
</ul>


<h3>Personal data refers to any personal information that allows you to identify the Buyer, such as:</h3>

<ul>
	<li>Last Name, First Name, Patronymic</li>
	<li>Date of birth</li>
	<li>Contact phone number</li>
	<li>Email address</li>
	<li>Postal address</li>
</ul>

<p>Personal data of Buyers is stored exclusively on electronic media and processed using automated systems, except in cases where non-automated processing of personal data is necessary in connection with the implementation of legal requirements.
The Seller undertakes not to transfer the received personal data to third parties, except for the following cases:</p>

<p>At the request of the authorized state authorities of the Russian Federation only on the grounds and in accordance with the procedure established by the legislation of the Russian Federation</p>
<p>Strategic partners who work with the Seller to provide products and services, or those who help the Seller to sell products and services to consumers. We provide third parties with the minimum amount of personal data necessary only to provide the required service or conduct the necessary transaction.</p>
<p>The Seller reserves the right to make unilateral changes to these rules, provided that the changes do not contradict the current legislation of the Russian Federation. Changes to the terms of these terms will take effect after they are published on the Website.</p>

<br />

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>