<?
require ($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "FAQ, customer assistance.");
$APPLICATION->SetPageProperty("description", "Answers to frequently asked questions, help the buyer, questions and answers about working with the store.");
$APPLICATION->SetTitle ("Questions and Answers");
?> <h1 class = "style-1"> Questions and Answers </h1>
<div class = "box-container">
<h2> How to make an order for Soap-wholesale </h2>
<p style = "text-align: justify;">
 <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> Due to numerous requests and suggestions from our customers, we have shot a video for you, how easy it is to make an order in the online store Soap-wholesale. &nbsp; </span> </span>
</p>
<p style = "text-align: justify;">
<iframe allowfullscreen = "" frameborder = "0" height = "250" src = "//www.youtube.com/embed/PFX5CocaRnE" width = "310"> </iframe>
</p>
<h2> Registration on the Soap-wholesale website </h2>
<p style = "text-align: justify;">
 <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> You asked in letters to us that we would shoot a video - How to register on website of the online store Soap-wholesale. The video is small but very informative. Everything is clear and understandable. This video will help new customers register faster and receive discounts and bonuses from the store. &nbsp; </span> </span>
</p>
<p style = "text-align: justify;">
<iframe allowfullscreen = "" frameborder = "0" height = "250" src = "//www.youtube.com/embed/D2fDIIC61m0" width = "310"> </iframe>
</p>
<h2 style = "text-align: justify;"> Wholesale Sections </h2>
<p style = "text-align: justify;">
 <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> For the convenience of searching for goods, the catalog has been changed, this video will help you find wholesale sections of goods in the new catalog . </span> </span>
</p>
<p style = "text-align: justify;">
<iframe allowfullscreen = "" frameborder = "0" height = "250" src = "//www.youtube.com/embed/Iox6aO3uRS8" width = "310"> </iframe>
</p>
<h3 style = "text-align: justify;"> Account password recovery </h3>
By numerous requests, we offer you instructions for recovering your password from your personal account. <br>
<iframe title = "Password recovery from cabinet" width = "310" height = "250" src = "//www.youtube.com/embed/pV4A2VcUlSQ?feature=oembed" frameborder = "0" allowfullscreen = "">
</iframe> <br>
<div class = "buttons">
<div class = "right">
 <a href="https://xn----utbcjbgv0e.com.ua/en/" class="button-cont-right"> Continue <i class = "icon-circle-arrow-right"> </i> </a>
</div>
</div>
</div>
 <br> <? require ($_SERVER["DOCUMENT_ROOT"]. "/bitrix/footer.php");?>