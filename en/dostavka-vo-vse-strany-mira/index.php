<?
require ($_SERVER ["DOCUMENT_ROOT"]. "/bitrix/header.php");
$APPLICATION->SetPageProperty ("title", "Worldwide shipping information.");
$APPLICATION->SetPageProperty ("keywords", "delivery to other countries, worldwide delivery, tariffs.");
$APPLICATION->SetPageProperty ("description", "Tariffs for delivery to all countries of the World weighing from 100 grams to 2 kg");
$APPLICATION->SetTitle ("Delivery to all countries of the World");
?> <h1 class = "style-1"> Worldwide delivery </h1>

<div class = "box-container">
    <h3> Tariffs for delivery to all countries of the World weighing from 100 grams to 2 kg </h3>

<p style = "text-align: justify;"> <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> Soap-wholesale company provides for its international buyers the rates for the delivery of your orders worldwide. For international shipments with commodity attachments up to 2 kg, the type of shipment is "Small package", weight from 1 to 2 kg, tariffs for small packages are gradated as follows: </span> </span> </p>

<p style = "text-align: justify;"> <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> 0 grams - 100 grams , 100 grams - 250 grams, 250 grams - 500 grams, 500 grams - 1 kg, 1 kg - 2 kg. </span> </span> </p>

<h3> Detailed information on tariffs: &nbsp; </h3>

<p style = "text-align: justify;"> <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> International shipping rates accepted in dollars, our company pays for the delivery of your orders from Ukraine in hryvnia at the rate of PrivatBank, therefore the cost in hryvnia may change (the rate of PrivatBank changes every business day). </span> </span> </p>

<p style = "text-align: justify;"> <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> Type of shipment "Small packet "up to 2 kg, Single tariff in dollars to all countries: &nbsp; </span> </span> </p>

<table border = "1" cellpadding = "1" cellspacing = "1">
<thead>
<tr>
<th scope = "col"> <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> Package weight </span> </span> </th>
<th scope = "col"> <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> Land delivery type </span> </span > </th>
<th scope = "col"> <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> Type of air delivery </span> </span > </th>
<th scope = "col"> <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> Currency </span> </span> </th>
</tr>
</thead>
<tbody>
<tr>
<td style = "text-align: center;"> <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> 0 - 100 grams </span> </span> </td>
<td style = "text-align: center;"> <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> 3.99 $ </span > </span> </td>
<td style = "text-align: center;"> <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> 4.32 $ &nbsp; </span> </span> </td>
<td style = "text-align: center;"> <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> US Dollar </span > </span> </td>
</tr>
<tr>
<td style = "text-align: center;"> <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> 100 - 200 grams </span> </span> </td>
<td style = "text-align: center;"> <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> 5.38 $ </span > </span> </td>
<td style = "text-align: center;"> <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> 6.18 $ &nbsp; </span> </span> </td>
<td style = "text-align: center;"> <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> US Dollar </span > </span> </td>
</tr>
<tr>
<td style = "text-align: center;"> <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> 250 - 500 grams </span> </span> </td>
<td style = "text-align: center;"> <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> 7.96 $ </span > </span> </td>
<td style = "text-align: center;"> <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> 9.44 $ &nbsp; </span> </span> </td>
<td style = "text-align: center;"> <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> US Dollar </span > </span> </td>
</tr>
<tr>
<td style = "text-align: center;"> <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> 500 - 1000 grams </span> </span> </td>
<td style = "text-align: center;"> <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> 11.86 $ </span > </span> </td>
<td style = "text-align: center;"> <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> 14.50 $ &nbsp; </span> </span> </td>
<td style = "text-align: center;"> <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> US Dollar </span > </span> </td>
</tr>
<tr>
<td style = "text-align: center; "> <span style =" font-size: 14px; "> <span style =" font-family: arial, helvetica, sans-serif; "> 1 - 2 kg &nbsp; </span> </span> </td>
<td style = "text-align: center;"> <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> 21.11 $ </span > </span> </td>
<td style = "text-align: center;"> <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> 22.11 $ &nbsp; </span> </span> </td>
<td style = "text-align: center;"> <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> US Dollar </span > </span> </td>
</tr>
</tbody>
</table>

<h3> Tariffs for delivery to all countries of the World weighing from 2 kg </h3>

<p style = "text-align: justify;"> <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> Below is a table of shipping rates by countries of the World for all economic zones. </span> </span> </p>

<h3 style = "text-align: justify;"> <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> An example of calculating your cargo for Russia. </span> </span> </h3>

<p style = "text-align: justify;"> <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> For Russia for registration of the parcel $ 15.70 (see the table) plus for 1 kg by land transport $ 2.15 or by air transport $ 3.50 (rounding up to 100 grams + package weight from 100-500 grams depending on the size of your order) . &nbsp; </span> </span> </p>

<table border = "1" cellpadding = "1" cellspacing = "1">
<thead>
<tr>
<th scope = "col"> &nbsp; </th>
<th colspan = "3" rowspan = "1" scope = "col"> <strong> <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif ; "> Rate, USD &nbsp; USA </span> </span> </strong> </th>
</tr>
</thead>
<tbody>
<tr>
<td colspan = "1" rowspan = "2" style = "text-align: center;"> <strong> <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif; "> Country of destination </span> </span> </strong> </td>
<td colspan = "1" rowspan = "2" style = "text-align: center;"> <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans -serif; "> for a parcel </span> </span> </td>
<td colspan = "2" rowspan = "1" style = "text-align: center;"> <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans -serif; "> for 1 kg </span> </span> </td>
</tr>
<tr>
<td style = "text-align: center;"> <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> by air </span> </span> </td>
<td style = "width: 50px; text-align: center;"> <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> ground (combined SAL) transport </span> </span> </td>
</tr>
<tr>
<td style = "text-align: justify;"> <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> Azerbaijan </span> </span> </td>
<td style = "text-align: justify;"> <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> 12.00 </span> </span> </td>
<td style = "text-align: justify;"> <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> 2.80 </span> </span> </td>
<td style = "text-align: justify;"> <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> 2.60 </span> </span> </td>
</tr>
<tr>
<td style = "text-align: justify;"> <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> Belarus </span> </span> </td>
<td style = "text-align: justify;"> <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> 9.20 </span> </span> </td>
<td style = "text-align: justify;"> <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> 2.50 </span> </span> </td>
<td style = "text-align: justify;"> <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> 2.00 </span> </span> </td>
</tr>
<tr>
<td style = "text-align: justify;"> <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> Armenia </span> </span> </td>
<td style = "text-align: justify;"> <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> 13.70 </span> </span> </td>
<td style = "text-align: justify;"> <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> 2.45 </span> </span> </td>
<td style = "text-align: justify;"> <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> 2.05 </span> </span> </td>
</tr>
<tr>
<td style = "text-align: justify;"> <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> Georgia </span> </span> </td>
<td style = "text-align: justify;"> <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> 11.20 </span> </span> </td>
<td style = "text-align: justify;"> <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> 3.00 </span> </span> </td>
<td style = "text-align: justify;"> <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif; "> 2.80 </span> </span> </td>
</tr>
<tr>
<td style = "text-align: justify;"> <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> Kazakhstan </span> </span> </td>
<td style = "text-align: justify;"> <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> 9.90 </span> </span> </td>
<td style = "text-align: justify;"> <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> 4.45 </span> </span> </td>
<td style = "text-align: justify;"> <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> 2.50 </span> </span> </td>
</tr>
<tr>
<td style = "text-align: justify;"> <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> Kyrgyzstan </span> </span> </td>
<td style = "text-align: justify;"> <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> 9.90 </span> </span> </td>
<td style = "text-align: justify;"> <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> 3.30 </span> </span> </td>
<td style = "text-align: justify;"> <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> 2.30 </span> </span> </td>
</tr>
<tr>
<td style = "text-align: justify;"> <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> Moldova </span> </span> </td>
<td style = "text-align: justify;"> <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> 13.00 </span> </span> </td>
<td style = "text-align: justify;"> <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> 2.80 </span> </span> </td>
<td style = "text-align: justify;"> <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> 2.50 </span> </span> </td>
</tr>
<tr>
<td style = "text-align: justify;"> <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> Tajikistan </span> </span> </td>
<td style = "text-align: justify;"> <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> 8.70 </span> </span> </td>
<td style = "text-align: justify;"> <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> 3.25 </span> </span> </td>
<td style = "text-align: justify;"> <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> 2.20 </span> </span> </td>
</tr>
<tr>
<td style = "text-align: justify;"> <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> Turkmenistan </span> </span> </td>
<td style = "text-align: justify;"> <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> 7.75 </span> </span> </td>
<td style = "text-align: justify;"> <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> 2.85 </span> </span> </td>
<td style = "text-align: justify;"> <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> 1.95 </span> </span> </td>
</tr>
<tr>
<td style = "text-align: justify;"> <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> Uzbekistan </span> </span> </td>
<td style = "text-align: justify;"> <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> 14.00 </span> </span> </td>
<td style = "text-align: justify;"> <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> 3.60 </span> </span> </td>
<td style = "text-align: justify;"> <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> 2.65 </span> </span> </td>
</tr>
<tr>
<td style = "text-align: justify;"> <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> Russia </span> </span> </td>
<td style = "text-align: justify;"> <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> 15.70 </span> </span> </td>
<td style = "text-align: justify;"> <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> 3.50 </span> </span> </td>
<td style = "text-align: justify;"> <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> 2.15 </span> </span> </td>
</tr>
<tr>
<td style = "text-align: justify;"> <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> Eastern Europe </span > </span> </td>
<td style = "text-align: justify;"> <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> 13.70 </span> </span> </td>
<td style = "text-align: justify;"> <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> 2.50 </span> </span> </td>
<td style = "text-align: justify;"> <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> 2.10 </span> </span> </td>
</tr>
<tr>
<td style = "text-align: justify;"> <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> Central, Northern Europe </span> </span> </td>
<td style = "text-align: justify;"> <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> 16.50 </span> </span> </td>
<td style = "text-align: justify;"> <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> 2.80 </span> </span> </td>
<td style = "text-align: justify;"> <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> 2.20 </span> </span> </td>
</tr>
<tr>
<td style = "text-align: justify;"> <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> Western Europe </span > </span> </td>
<td style = "text-align: justify;"> <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> 16.40 </span> </span> </td>
<td style = "text-align: justify;"> <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> 3.30 </span> </span> </td>
<td style = "text-align: justify;"> <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> 2.10 </span> </span> </td>
</tr>
<tr>
<td style = "text-align: justify;"> <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> Central Asia, Middle East </span></span> </td>
<td style = "text-align: justify;"> <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> 14.00 </span> </span> </td>
<td style = "text-align: justify;"> <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> 2.60 </span> </span> </td>
<td style = "text-align: justify;"> <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> 2.00 </span> </span> </td>
</tr>
<tr>
<td style = "text-align: justify;"> <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> North America </span > </span> </td>
<td style = "text-align: justify;"> <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> 10.80 </span> </span> </td>
<td style = "text-align: justify;"> <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> 5.80 </span> </span> </td>
<td style = "text-align: justify;"> <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> 3.30 </span> </span> </td>
</tr>
<tr>
<td style = "text-align: justify;"> <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> East Asia </span > </span> </td>
<td style = "text-align: justify;"> <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> 12.00 </span> </span> </td>
<td style = "text-align: justify;"> <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> 7.10 </span> </span> </td>
<td style = "text-align: justify;"> <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> 3.60 </span> </span> </td>
</tr>
<tr>
<td style = "text-align: justify;"> <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> Africa, South and Central America </span> </span> </td>
<td style = "text-align: justify;"> <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> 14.50 </span> </span> </td>
<td style = "text-align: justify;"> <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> 7.00 </span> </span> </td>
<td style = "text-align: justify;"> <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> 5.50 </span> </span> </td>
</tr>
<tr>
<td style = "text-align: justify;"> <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> Australia and Oceania </span> </span> </td>
<td style = "text-align: justify;"> <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> 10.40 </span> </span> </td>
<td style = "text-align: justify;"> <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> 11.50 </span> </span> </td>
<td style = "text-align: justify;"> <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> 6.00 </span> </span> </td>
</tr>
</tbody>
</table>

<p style = "text-align: justify;"> <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> You can declare value for sending (it is impossible to insure, for a small package it is impossible), then to the cost for the parcel and the mass is added plus $ 3.60 for insurance (the maximum amount of insurance that can be specified is equivalent to 150 euros). &nbsp; </span> </span> </p>

<p style = "text-align: justify;"> <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> Company Soap-wholesale always tries to reduce the cost of delivering your orders to your home. Register in our online store of natural ingredients for cosmetics and get up to 10% discount on your orders for regular customers. </span> </span> </p>

<p style = "text-align: justify;"> <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;">those with us to your health! </span> </span> </p>
     <div class = "buttons">
       <div class = "right"> <a href="https://xn----utbcjbgv0e.com.ua/en/" class="button-cont-right"> Continue <i class = "icon-circle-arrow -right "> </i> </a> </div>
     </div>
   </div> <? require ($_SERVER ["DOCUMENT_ROOT"]. "/bitrix/footer.php");?>