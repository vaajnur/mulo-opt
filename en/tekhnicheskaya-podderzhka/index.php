<?
require ($_SERVER ["DOCUMENT_ROOT"]. "/bitrix/header.php");
$APPLICATION-> SetPageProperty ("title", "Technical support of the Soap-wholesale store");
$APPLICATION-> SetPageProperty ("keywords", "Technical Support");
$APPLICATION-> SetPageProperty ("description", "Technical support");
$APPLICATION-> SetTitle ("Technical support of the Soap-wholesale online store");
?> <h1 class = "style-1"> Technical support for the Soap-wholesale online store </h1>
<div class = "box-container">
<p>
 <span style = "font-size: 14px;"> <strong style = "text-align: justify; line-height: 1.6em;"> Dear customers! </strong> </span>
</p>
<p style = "text-align: justify;">
 <span style = "font-size: 14px;"> Please pay attention, the correct operation of the store directly depends on the state of your equipment, experience shows that you, dear soap makers, are not programmers, and not everyone knows how to service your home appliances, therefore , so as not to ask questions and not to write that "the basket does not work", "the price in the basket is not updated when the quantity is updated", etc. please read the recommendations: </span>
</p>
<p style = "text-align: justify;">
 <span style = "font-size: 14px;"> 1. Do not use Internet Explouer for orders, since correct operation depends on the add-ons of the browser itself, we recommend Opera, Mozilla and others. </span>
</p>
<p style = "text-align: justify;">
 <span style = "font-size: 14px;"> 2. Install and keep up-to-date Antivirus programs and databases ... as a recommendation for example AVG Antivirus Free. </span>
</p>
<p style = "text-align: justify;">
 <span style = "font-size: 14px;"> 3. If, nevertheless, the work is incorrect, use the form &nbsp; <strong> <a href="https://ylo-opt.com.ua/callback/" target="_blank"> <span style = "color: # 008000;" > WRITE TO US </span> </a> </strong> &nbsp; in the text &nbsp; indicate the name, patronymic of the contact person, contact phone number, e-mail, what technical point does not work for you on the site, in more detail ... "BASKET DOES NOT WORK" are not accepted ... </span>
</p>
<p style = "text-align: justify;">
 <span style = "font-size: 14px;"> Also, if possible, install the <strong> TIMEVIEWER </strong> program for our technician to access your computer to resolve this issue. </span>
</p>
<p style = "text-align: justify;">
 <span style = "font-size: 14px;"> 4. If you are ready for all the points above, please contact technical support. &nbsp; </span>
</p>
<p style = "text-align: justify;">
 <span style = "font-size: 14px;"> 5. If not ready, applications will not be considered ... </span>
</p>
<p>
&nbsp;
</p>
<div class = "buttons">
<div class = "right">
 <a href="https://xn----utbcjbgv0e.com.ua/en/" class="button-cont-right"> Continue <i class = "icon-circle-arrow-right"> </i> </a>
</div>
</div>
</div>
 <br>
<? require ($_SERVER ["DOCUMENT_ROOT"]. "/bitrix/footer.php");?>