<?define("INDEX_PAGE", "Y");?>
 <?define("MAIN_PAGE", true);?> <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Products for soap making and home cosmetics. Soap base, essential oils, extracts, peptides, flavors, colors, fragrances. Wholesale prices, delivery.");
$APPLICATION->SetPageProperty("og:image", "/bitrix/templates/dresscode/images/logo.png");
$APPLICATION->SetTitle("Shop of soap making and components for natural cosmetics");?>
<?$APPLICATION->IncludeComponent(
	"innova:slider", 
	"template3", 
	array(
		"AUTOPLAY" => "false",
		"AUTOPLAY_SPEED" => "3000",
		"BTN_COLOR" => "#FFFFFF",
		"CACHE_TYPE" => "A",
		"COMPONENT_TEMPLATE" => "template3",
		"HEIGHT" => "450",
		"HEIGHT_MOBILE" => "200",
		"HIDE_TEXT" => "Y",
		"IBLOCK_ID" => "2",
		"SLIDER_COLOR" => "rgba(0, 0, 0, 0)",
		"SPEED" => "500",
		"STRETCH_TYPE" => "1",
		"TEXT_ALIGN" => "center",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO"
	),
	false
);?>
<!--<div id="promoBlock">
	 <?$APPLICATION->IncludeComponent(
	"dresscode:slider",
	"promoSlider",
	Array(
		"CACHE_TIME" => "3600000",
		"CACHE_TYPE" => "A",
		"COMPONENT_TEMPLATE" => "promoSlider",
		"IBLOCK_ID" => "2",
		"IBLOCK_TYPE" => "slider",
		"PICTURE_HEIGHT" => "450",
		"PICTURE_WIDTH" => "1200"
	)
);?>
</div>-->

<!-- Candidate!!! -->
<?$APPLICATION->IncludeComponent(
	"dresscode:offers.product",
	".default",
	Array(
		"CACHE_TIME" => "3600000",
		"CACHE_TYPE" => "Y",
		"CATALOG_ITEM_TEMPLATE" => ".default",
		"COMPONENT_TEMPLATE" => ".default",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO",
		"CONVERT_CURRENCY" => "N",
		"ELEMENTS_COUNT" => "16",
		"HIDE_MEASURES" => "N",
		"HIDE_NOT_AVAILABLE" => "N",
		"IBLOCK_ID" => "10",
		"IBLOCK_TYPE" => "catalog",
		"PICTURE_HEIGHT" => "200",
		"PICTURE_WIDTH" => "220",
		"PRODUCT_PRICE_CODE" => array(),
		"PROP_NAME" => "OFFERS",
		"PROP_VALUE" => array(0=>"_9",),
		"SORT_PROPERTY_NAME" => "SORT",
		"SORT_VALUE" => "ASC"
	),
false,
Array(
	'ACTIVE_COMPONENT' => 'Y'
)
);?>
<!-- Candidate!!!12 -->
<?$APPLICATION->IncludeComponent(
	"dresscode:pop.section",
	".default",
	Array(
		"CACHE_TIME" => "3600000",
		"CACHE_TYPE" => "Y",
		"COMPONENT_TEMPLATE" => ".default",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO",
		"ELEMENTS_COUNT" => "10",
		"IBLOCK_ID" => "10",
		"IBLOCK_TYPE" => "catalog",
		"PICTURE_HEIGHT" => "100",
		"PICTURE_WIDTH" => "120",
		"POP_LAST_ELEMENT" => "Y",
		"PROP_NAME" => "UF_POPULAR",
		"PROP_VALUE" => "Y",
		"SELECT_FIELDS" => array(0=>"NAME",1=>"SECTION_PAGE_URL",2=>"DETAIL_PICTURE",3=>"UF_IMAGES",4=>"UF_MARKER",5=>"",),
		"SORT_PROPERTY_NAME" => "7",
		"SORT_VALUE" => "DESC"
	),
false,
Array(
	'ACTIVE_COMPONENT' => 'N'
)
);?>
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	".default",
	Array(
		"AREA_FILE_RECURSIVE" => "Y",
		"AREA_FILE_SHOW" => "sect",
		"AREA_FILE_SUFFIX" => "simplyText",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => ""
	),
false,
Array(
	'ACTIVE_COMPONENT' => 'Y'
)
);?><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>