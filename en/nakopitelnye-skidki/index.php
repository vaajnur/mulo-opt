<?
require ($_SERVER ["DOCUMENT_ROOT"]. "/bitrix/header.php");
$APPLICATION-> SetPageProperty ("title", "Cumulative discounts for Soap-wholesale store.");
$APPLICATION-> SetPageProperty ("keywords", "cumulative discounts, store discounts, cumulative program.");
$APPLICATION-> SetPageProperty ("description", "System of cumulative discounts for all customers of the store. Cumulative discounts 3, 5, 7, 10% for all t ...");
$APPLICATION-> SetTitle ("Cumulative discounts of the Soap-wholesale online store");
?> <h1 class = "style-1"> Cumulative discounts of the Soap-wholesale online store </h1>

<div class = "box-container">
    <p style = "text-align: justify;"> <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> We have <strong> cumulative discount system </strong>. Our buyers receive the following discounts based on the amount of their previous purchases: </span> </span> </p>

<p style = "text-align: justify;"> <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> <img alt = " "height =" 42 "src =" http://xn----utbcjbgv0e.com.ua/image/data/logotip/3procent.png "width =" 67 "> 3% discount, when purchasing goods worth more than UAH 300, within the last 180 days </span> </span> </p>

<p style = "text-align: justify;"> <img alt = "" height = "42" src = "http://xn----utbcjbgv0e.com.ua/image/data/logotip/5procent.png"width =" 67 "> <span style =" font-size: 14px; "> <span style =" font-family: arial, helvetica, sans-serif; "> 5% discount, when purchasing goods worth more than 1000 UAH, within the last 180 days </span> </span> </p>

<p style = "text-align: justify;"> <img alt = "" height = "42" src = "http://xn----utbcjbgv0e.com.ua/image/data/logotip/7procent.png"width =" 67 "> <span style =" font-size: 14px; "> <span style =" font-family: arial, helvetica, sans-serif; "> 7% discount, when purchasing goods worth more than UAH 1500, during the last 180 days </span> </span> </p>

<p style = "text-align: justify;"> <img alt = "" height = "42" src = "http://xn----utbcjbgv0e.com.ua/image/data/logotip/10procent.png"width =" 67 "> <span style =" font-size: 14px; "> <span style =" font-family: arial, helvetica, sans-serif; "> 10% discount, when purchasing goods worth more than 2000 UAH, within the last 180 days </span> </span> </p>

<p style = "text-align: justify;"> &nbsp; </p>

<p style = "text-align: justify;"> <span style = "font-size: 14px;"> <span style = "font-family: arial, helvetica, sans-serif;"> <strong> Attention! </strong> Cumulative discount is calculated only from the amount of previous orders. It does not take into account the amount of the order that you are placing at a given time. Please note that goods purchased at a promotional, reduced price are not included in cumulative discounts. In order for the discount to work correctly, you must be a registered customer, purchases of the guest mode do not include the discount ... </span> </span> </p>
    <div class = "buttons">
      <div class = "right"> <a href="https://xn----utbcjbgv0e.com.ua/en/" class="button-cont-right"> Continue <i class = "icon-circle-arrow -right "> </i> </a> </div>
    </div>
  </div> <? require ($_SERVER ["DOCUMENT_ROOT"]. "/bitrix/footer.php");?>