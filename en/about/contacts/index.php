<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Ask questions");
?><h1>Contact information</h1>
 <?$APPLICATION->IncludeComponent(
	"bitrix:menu",
	"personal",
	Array(
		"ALLOW_MULTI_SELECT" => "N",
		"CHILD_MENU_TYPE" => "",
		"COMPONENT_TEMPLATE" => "personal",
		"DELAY" => "N",
		"MAX_LEVEL" => "1",
		"MENU_CACHE_GET_VARS" => array(),
		"MENU_CACHE_TIME" => "3600000",
		"MENU_CACHE_TYPE" => "A",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"ROOT_MENU_TYPE" => "about",
		"USE_EXT" => "N"
	)
);?>
<div class="box">
<div style="border-bottom:1px solid #CFDFEA;">
</div>
<div class="box-content">
<div class="box-html">
<p style="margin: 0px 0px 20px; color: #7e7e7e; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; line-height: 18px; text-align: justify;">
<span style="font-family:arial,helvetica,sans-serif;"><span style="font-size: 14px;">When placing an order, please use the online form <strong> ("Shopping Cart") </strong> & nbsp; <strong> <a href = «mailto:mysoapoptom@gmail.com" style="outline: none;" target="_blank">mysoapoptom@gmail.com</a></strong>. The e-mail of the Soap-Wholesale store is processed almost around the clock, so all your orders, questions, and requests will be processed and considered in the near future. We also ask you to notify us about the payment for the ordered goods by e-mail. </span></span>
</p>
<p style="margin: 0px 0px 20px; color: #7e7e7e; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; line-height: 18px; text-align: justify;">
<span style="font-family:arial,helvetica,sans-serif;"><span style="font-size: 14px;">If you have any urgent questions, as well as if you need to solve them urgently, you can contact our managers via the feedback form on the website or via the above email address. We try to ensure that every customer receives their order on time.</span></span>
</p>
<p style="margin: 0px 0px 20px; color: #7e7e7e; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; line-height: 18px; text-align: justify;">
<span style= "font-family:arial, helvetica,sans-serif;"><span style= "font-size: 14px;">Thank you for your understanding and cooperation!</span></span>
</p>
</div>
</div>
</div>
<ul class="contactList">
	<li>
	<table>
	<tbody>
	<tr>
		<td>
 <img alt="cont1.png" src="/bitrix/templates/dresscode/images/cont1.png" title="cont1.png">
		</td>
		<td>
			 +38 (063) 6900672
		</td>
	</tr>
	</tbody>
	</table>
 </li>
	 <?/*<li>
		<table>
		<tbody>
		<tr>
			<td>
				<img alt="cont2.png" src="<?=SITE_TEMPLATE_PATH?>/images/cont2.png" title="cont2.png">
			</td>
			<td>
 <a href="mailto:info@dw24.su">info@dw24.su</a><br>
 <a href="mailto:support@dw24.su">support@dw24.su</a><br>
			</td>
		</tr>
		</tbody>
		</table>
</li>*/?>
	<li>
	<table>
	<tbody>
	<tr>
		<td>
 <img alt="cont1.png" src="/bitrix/templates/dresscode/images/cont1.png" title="cont1.png">
		</td>
		<td>
			 ФАКС: +38 (095) 8113091
		</td>
	</tr>
	</tbody>
	</table>
 </li>
	<li>
	<table>
	<tbody>
	<tr>
		<td>
 <img alt="cont3.png" src="/bitrix/templates/dresscode/images/cont3.png" title="cont3.png">
		</td>
		<td>
			 1-й Песчаный тупик, 1, <br>
			 Кременчуг, Полтавская область, Украина&nbsp; &nbsp;
		</td>
	</tr>
	</tbody>
	</table>
 </li>
	 <?/*<li>
		<table>
		<tbody>
		<tr>
			<td>
 <img alt="cont4.png" src="<?=SITE_TEMPLATE_PATH?>/images/cont4.png" title="cont4.png">
			</td>
			<td>
				 Пн-Пт : с 10:00 до 20:00<br>
				 Сб, Вс : выходной<br>
			</td>
		</tr>
		</tbody>
		</table>
 </li>*/?>
</ul>
 <img width="485" alt="Мы на карте" src="/upload/medialibrary/6f2/map.jpg" height="291" title="Мы на карте"><br>
 <br>
 <br>
		<?$APPLICATION->IncludeComponent(
	"bitrix:form.result.new",
	".default",
	Array(
		"CACHE_TIME" => "360000",
		"CACHE_TYPE" => "Y",
		"CHAIN_ITEM_LINK" => "",
		"CHAIN_ITEM_TEXT" => "",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_URL" => "",
		"IGNORE_CUSTOM_TEMPLATE" => "N",
		"LIST_URL" => "",
		"SEF_MODE" => "N",
		"SUCCESS_URL" => "",
		"USE_EXTENDED_ERRORS" => "Y",
		"VARIABLE_ALIASES" => array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID",),
		"WEB_FORM_ID" => "10"
	)
);?><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php")?>