<?
require ($_SERVER["DOCUMENT_ROOT"]. "/bitrix/header.php");
$APPLICATION-> SetTitle ("About the store");
?> <h1> About the store </h1>
<? $APPLICATION-> IncludeComponent (
"bitrix: menu",
"personal",
array (
"COMPONENT_TEMPLATE" => "personal",
"ROOT_MENU_TYPE" => "about",
"MENU_CACHE_TYPE" => "A",
"MENU_CACHE_TIME" => "3600000",
"MENU_CACHE_USE_GROUPS" => "Y",
"MENU_CACHE_GET_VARS" => array (
),
"MAX_LEVEL" => "1",
"CHILD_MENU_TYPE" => "",
"USE_EXT" => "N",
"DELAY" => "N",
"ALLOW_MULTI_SELECT" => "N"
),
false
);?>
<div class = "global-block-container">
<div class = "global-content-block">
<div class = "bx_page">
We are glad to welcome you to the website of our company.
<p> Our company was founded in 1993, and our online store became one of the first stores that sell furniture on-line in the region. The company specializes in the wholesale and retail of furniture for both home and office and industrial premises, as well as various furniture fittings. </p>
<p> At the moment, we are a large company that owns an online store and has a single call-center in its network that regulates all the activities of the store, the sales department, the delivery service, a wide staff of qualified assemblers, our own warehouse with the constant availability of the necessary stock goods. </p>

<p> During this time, we have developed partnerships with leading manufacturers, which allow us to offer high quality products at competitive prices. </p>

<p> We can be proud that we have one of the widest assortments of furniture in the city and region. </p>

<p> <b> ON OUR WEBSITE AT YOUR SERVICES: </b> </p>

<ul>
<li> real and competitive prices; </li>

<li> the widest range of products; </li>

<li> high-quality product descriptions and images; </li>

<li> search for products in the store; </li>

<li> feedback system; </li>

<li> sale of only certified and legal goods; </li>

<li> warranty service for goods purchased from us; </li>

<li> buying goods from the comfort of your home or office; </li>

<li> quick coordination of the goods with the client to confirm the order; </li>

<li> exchange of goods of inadequate quality and much more. </li>
</ul>

<p> We are always happy to communicate with our clients. If you have any wishes, suggestions, comments regarding the operation of our online store - write to us, and we will gratefully take your opinion into account: </p>

<p> <b> E-MAIL </b>: <a href="mailto:sale@magazine.ru"> sale@magazine.ru </a> </p>
</div>
</div>
<div class = "global-information-block">
<? $APPLICATION-> IncludeComponent (
"bitrix: main.include",
".default",
array (
"COMPONENT_TEMPLATE" => ".default",
"AREA_FILE_SHOW" => "sect",
"AREA_FILE_SUFFIX" => "information_block",
"AREA_FILE_RECURSIVE" => "Y",
"EDIT_TEMPLATE" => ""
),
false
);?>
</div>
</div>
<? require ($_SERVER["DOCUMENT_ROOT"]. "/bitrix/footer.php");?>