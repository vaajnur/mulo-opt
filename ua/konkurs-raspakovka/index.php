<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty ("title", " Конкурс, розіграш призів від інтернет-магазину мило-Опт.");
$APPLICATION->SetPageProperty ("keywords", " конкурс, акція, розіграш призів.");
$APPLICATION->SetPageProperty ("description", " Правила конкурсу &quot;розпакування&quot; від інтернет-магазину мило-Опт.");
$APPLICATION->SetTitle ("Конкурс-розпакування");
?><p class="box-container">
</p>
<div style="text-align:center">
 <figure class="caption" style="display:inline-block"><img width="738" src="https://xn----utbcjbgv0e.com.ua/image/data/konkurs/raspakovka.jpg" height="327" alt=""> <figcaption></figcaption> </figure>
</div>
<h3 style="text-align: justify;margin: 0px 0px 0.5 em; padding: 0px; letter-spacing: -0.015 em; line-height: 1.4 em; font-size: 32px; font-family: &quot;Open Sans&quot;, sans-serif; text-align: center;">знімайте розпакування Вашого товару і вигравайте призи!</h3>
<h3 style="text-align: justify;margin: 0px 0px 0.5 em; padding: 0px 0px 15px; letter-spacing: -0.015 em; line-height: 1.4 em; font-size: 32px; font-family: &quot;Open Sans&quot;, sans-serif; text-align: center; color: #c91616;">гарантований промокод на 100 грн за кожну розпакування, а також головний приз - промокод на 500 грн автору кращого відео!*&nbsp;</h3>
<h3 style="text-align: justify;margin: 0px 0px 0.5 em; padding: 0px 0px 15px; letter-spacing: -0.015 em; line-height: 1.4 em; font-size: 32px; font-family: &quot;Open Sans&quot;, sans-serif; text-align: center; color: #c91616;">нова акція: за надісланий відео майстер клас - купон на 150 грн !</h3>
 <br>
<p style="margin:0px; padding:10px 15px; list-style:none outside none; border-color:#7cb030; font-size:22px; border-radius:10px; font-style:italic; text-align:center; background-color:#baeb72">
 <span style= "color: #800000; line-height: 1.6;" >для участі в конкурсі, необхідно:</span>
</p>
<p style="margin: 0px 0px 15px; padding: 0px; line-height: 1.4em; font-size: 16px; font-family: &quot;Open Sans&quot;, sans-serif; background-color: rgba(255, 255, 255, 0.952941);">
 <span style="font-size: 1em; line-height: 1.4em; display: table-cell;">1. </span><span style= " font-size: 1em; line-height: 1.4 em; display: table-cell;">купити товар в магазині мило-опт</span>
</p>
<p style="margin: 0px 0px 15px; padding: 0px; line-height: 1.4em; font-size: 16px; font-family: &quot;Open Sans&quot;, sans-serif; background-color: rgba(255, 255, 255, 0.952941);">
 <span style="font-size: 1em; line-height: 1.4em; display: table-cell;">2.</span><span style= " font-size: 1em; line-height: 1.4 em; display: table-cell;">зняти відеоролик з розпакуванням цього товару</span>
</p>
<p style="margin: 0px 0px 15px; padding: 0px; line-height: 1.4em; font-size: 16px; font-family: &quot;Open Sans&quot;, sans-serif; background-color: rgba(255, 255, 255, 0.952941);">
 <span style="font-size: 1em; line-height: 1.4em; display: table-cell;">3. </span><span style= " font-size: 1em; line-height: 1.4 em; display: table-cell;">детально продемонструвати упаковку, комплектацію і зовнішній вигляд товару</span>
</p>
<p style="margin: 0px 0px 15px; padding: 0px; line-height: 1.4em; font-size: 16px; font-family: &quot;Open Sans&quot;, sans-serif; background-color: rgba(255, 255, 255, 0.952941);">
 <span style="font-size: 1em; line-height: 1.4em; display: table-cell;">4. </span><span style="font-size: 1em; line-height: 1.4 em; display: table-cell;">розповісти враження про сервіс і доставку інтернет-магазину Мыло-опт.сом.иа</span>
</p>
<p style="margin: 0px 0px 15px; padding: 0px; line-height: 1.4em; font-size: 16px; font-family: &quot;Open Sans&quot;, sans-serif; background-color: rgba(255, 255, 255, 0.952941);">
 <span style="font-size: 1em; line-height: 1.4em; display: table-cell;">5. </span><span style="font-size: 1em; line-height: 1.4 em; display: table-cell;">викласти ролик на свій YouTube-канал і надіслати посилання через спеціальну форму<br>
 <br>
 безпрограшний конкурс відео майстер класів!</b><br>
	 Ви можете надіслати нам відео-майстер клас з виготовлення мила, бомбочок для ванної, крему, тоніка та іншої косметичної продукції з товарів, які придбали на нашому сайті. За надісланий відео майстер клас - купон на 150 грн.</span>
</p>
<p style="margin:0px; padding:10px 15px; list-style:none outside none; border-color:#7cb030; font-size:22px; border-radius:10px; font-style:italic; text-align:center; background-color:#baeb72">
 <span style= "color: #800000; line-height: 1.6;" >вимоги до відеороликів:</span>
</p>
<p style="margin: 0px 0px 15px; padding: 0px; line-height: 1.4em; font-size: 16px; font-family: &quot;Open Sans&quot;, sans-serif; background-color: rgba(255, 255, 255, 0.952941);">
 <span style="font-size: 1em; line-height: 1.4em; display: table-cell;">1. </span><span style="font-size: 1em; line-height: 1.4 em; display: table-cell;">якість відео від 480p</span>
</p>
<p style="margin: 0px 0px 15px; padding: 0px; line-height: 1.4em; font-size: 16px; font-family: &quot;Open Sans&quot;, sans-serif; background-color: rgba(255, 255, 255, 0.952941);">
 <span style="font-size: 1em; line-height: 1.4em; display: table-cell;">2. </span><span style= " font-size: 1em; line-height: 1.4 em; display: table-cell;">Голосовий супровід і чистий звук — додатковий плюс автору</span>
</p>
<p style="margin: 0px 0px 15px; padding: 0px; line-height: 1.4em; font-size: 16px; font-family: &quot;Open Sans&quot;, sans-serif; background-color: rgba(255, 255, 255, 0.952941);">
 <span style="font-size: 1em; line-height: 1.4em; display: table-cell;">3. </span><span style= " font-size: 1em; line-height: 1.4 em; display: table-cell;">одне відео — одна розпакування</span>
</p>
<p style="margin: 0px 0px 15px; padding: 0px; line-height: 1.4em; font-size: 16px; font-family: &quot;Open Sans&quot;, sans-serif; background-color: rgba(255, 255, 255, 0.952941);">
 <span style="font-size: 1em; line-height: 1.4em; display: table-cell;">4. </span><span style="font-size: 1em; line-height: 1.4 em; display: table-cell;">посилання на товар в описі відео на YouTube</span>
</p>
<p style="margin: 0px 0px 15px; padding: 0px; line-height: 1.4em; font-size: 16px; font-family: &quot;Open Sans&quot;, sans-serif; background-color: rgba(255, 255, 255, 0.952941);">
 <span style="font-size: 1em; line-height: 1.4em; display: table-cell;">5.</span> <span style= " font-size: 1em; line-height: 1.4 em; display: table-cell;">відео не повинно бути вертикальним</span>
</p>
<p style="margin: 0px 0px 15px; padding: 0px; line-height: 1.4em; font-size: 16px; font-family: &quot;Open Sans&quot;, sans-serif; background-color: rgba(255, 255, 255, 0.952941);">
 <span style="font-size: 1em; line-height: 1.4em; display: table-cell;">6. </span><span style= " font-size: 1em; line-height: 1.4 em; display: table-cell;">посилання на товар повинна бути на першому місці в описі під вашим відео</span>
</p>
<p style="margin: 0px 0px 1em; padding: 0px; line-height: 1.1em; font-size: 24px; font-family: &quot;Open Sans&quot;, sans-serif; color: #d67d17; background-color: rgba(255, 255, 255, 0.952941);">
	 За якісне відео-гарантований промокод на знижку 100 грн&nbsp;
</p>
<p style="margin: 0px 0px 1em; padding: 0px; line-height: 1.1em; font-size: 24px; font-family: &quot;Open Sans&quot;, sans-serif; color: #d67d17; background-color: rgba(255, 255, 255, 0.952941);">
	 За якісне відео майстер класу-150 грн
</p>
<p style="margin: 0px 0px 15px; padding: 0px; line-height: 1.4em; font-size: 16px; font-family: &quot;Open Sans&quot;, sans-serif; background-color: rgba(255, 255, 255, 0.952941);">
 <span style="line-height: 21.0022 px;">Автор кожного відеоролика, який редакція сайту вважатиме якісним, отримає на свою електронну адресу промокод</span> <br style="line-height: 21.0022 px;">
 <span style= " line-height: 21.0022 px;">на 100 грн за відео розпакування товару або 150 грн за відео майстер клас. </span>На одне замовлення можна використовувати тільки один купон, купони не об'єднуються.
</p>
<p style="margin: 0px 0px 1em; padding: 0px; line-height: 1.1em; font-size: 24px; font-family: &quot;Open Sans&quot;, sans-serif; color: #295bb4; background-color: rgba(255, 255, 255, 0.952941);">
	 Кожні 100 відео розігрується промокод на 500 грн
</p>
<p>
	 <?$APPLICATION->IncludeComponent(
	"bitrix:form.result.new",
	"",
	Array(
		"CACHE_TIME" => "3600",
		"CACHE_TYPE" => "A",
		"CHAIN_ITEM_LINK" => "",
		"CHAIN_ITEM_TEXT" => "",
		"EDIT_URL" => "result_edit.php",
		"IGNORE_CUSTOM_TEMPLATE" => "N",
		"LIST_URL" => "result_list.php",
		"SEF_MODE" => "N",
		"SUCCESS_URL" => "",
		"USE_EXTENDED_ERRORS" => "N",
		"VARIABLE_ALIASES" => Array("RESULT_ID"=>"RESULT_ID","WEB_FORM_ID"=>"WEB_FORM_ID"),
		"WEB_FORM_ID" => "9"
	)
);?>
</p>
<p style="margin:0px; padding:10px 15px; list-style:none outside none; border-color:#7cb030; font-size:22px; border-radius:10px; font-style:italic; text-align:center; background-color:#baeb72">
 <span style= "color:#800000;" >відео конкурсу</span>
</p>
 <b> </b>
<table border="1" cellpadding="1" cellspacing="1" style="width:500px;" align="center">
<tbody>
<tr>
	<td colspan="1" style="text-align: center;">
		 Відео №294<br>
	</td>
	<td colspan="1" style="text-align: center;">
		 Відео №295<br>
	</td>
	<td colspan="1" style="text-align: center;">
		 Відео №296<br>
	</td>
</tr>
<tr>
	<td colspan="1" style="text-align: center;">
		 <iframe title= "товари для миловаріння з #unboxing №319" width="310" height= "250"src=" //www.youtube.com/embed/ePjiAknWexE?feature=oembed "frameborder=" 0 "allow=" accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="">
		</iframe>
	</td>
	<td colspan="1" style="text-align: center;">
<iframe title=" # Мило_ручной_роботи або # косметика ? Для чого всі ці компоненти?" width="310" height="250" src="//www.youtube.com/embed/VSW8_doRct8?feature=oembed" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="">
</iframe>
</td>
<td colspan="1" style="text-align: center;">
<iframe title= "повний стіл товарів для мила ручної роботи // #розпакування 296" width="310" height="250" src="//www.youtube.com/embed/xGW-wNJwgWo?feature=oembed "frameborder=" 0 "allow=" accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="">
</iframe>
</td>
</tr>
<tr>
<td colspan="1" style="text-align: center;">
Відео №297<br>
</td>
<td colspan="1" style="text-align: center;">
Відео №298<br>
</td>
<td colspan="1" style="text-align: center;">
Відео №299<br>
</td>
</tr>
<tr>
<td colspan="1" style="text-align: center;">
<iframe title= "товари для квітів з мила / / розпаковуємо посилку з мило Опт "width=" 310 "height=" 250 "src=" //www.youtube.com/embed/w8bJJzX1WSc?feature=oembed "frameborder=" 0 "allow=" accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="">
</iframe>
</td>
<td colspan="1" style="text-align: center;">
<iframe title= "все для #diy #crafts - #миловаріння // розпакування №298" width="310" height= "250"src=" //www.youtube.com/embed/3tfYVOFrx2U?feature=oembed "frameborder=" 0 "allow=" accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="">
</iframe>
</td>
<td colspan="1" style="text-align: center;">
<iframe title="#SPA і #DIY все в одній коробці //розпакування №299"width=" 310 "height= "250"src=" //www.youtube.com/embed/hOQSr0cqXe4?feature=oembed "frameborder=" 0 "allow=" accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="">
</iframe>
</td>
</tr>
<tr>
<td colspan="1" style="text-align: center;">
Відео №300<br>
</td>
<td colspan="1" style="text-align: center;">
Відео №301<br>
</td>
<td colspan="1" style="text-align: center;">
Відео №302<br>
</td>
</tr>
<tr>
<td colspan="1" style="text-align: center;">
<iframe title="#розпакування №300 посилки #soapcrafter// мило ручної роботи"width=" 310 "height= "250"src=" //www.youtube.com/embed/YOF2zDJfryw?feature=oembed "frameborder=" 0 "allow=" accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="">
</iframe>
</td>
<td colspan="1" style="text-align: center;">
<iframe title="#розпакування №301 - замовлення від магазину миловаріння"width=" 310 "height= "250"src=" //www.youtube.com/embed/Fj4_XYUbigo?feature=oembed "frameborder=" 0 "allow=" accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="">
</iframe>
</td>
<td colspan="1" style="text-align: center;">
<iframe title=" задоволений #покупець із замовленням з магазину мило Опт"width=" 310"height=" 250 "src=" //www.youtube.com/embed/Gm9owYJ_Z80?feature=oembed "frameborder=" 0 "allow=" accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="">
</iframe>
</td>
</tr>
<tr>
<td colspan="1" style="text-align: center;">
Відео №303<br>
</td>
<td colspan="1" style="text-align: center;">
Відео №304<br>
</td>
<td colspan="1" style="text-align: center;">
Відео №305<br>
</td>
</tr>
<tr>
<td colspan="1" style="text-align: center;">
<iframe title="#Київ #unboxing №303 замовлення корисних товарів від мило Опт "width=" 310 "height= "250"src=" //www.youtube.com/embed/tzL6NCetp4c?feature=oembed "frameborder=" 0 "allow=" accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="">
</iframe>
</td>
<td colspan="1" style="text-align: center;">
<iframe title= "все для свічкової справи і #DIY// розпакування №304" width="310" height="250" src="//www.youtube.com/embed/RqbcMtzJxsU?feature=oembed "frameborder=" 0 "allow=" accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="">
</iframe>
</td>
<td colspan="1" style="text-align: center;">
<iframe title="#миловар Кокетка Грін - #unboxing №305"width=" 310 "height=" 250 "src=" //www.youtube.com/embed/2q1i6Vx23TE?feature=oembed "frameborder=" 0 "allow=" accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="">
</iframe>
</td>
</tr>
</tbody>
</table>
<p style="text-align: justify;">
</p>
<p span="">
<!-- noindex--><a rel= "nofollow" href="https://мыло-опт.com.ua/konkurs-raspakovka/" ></a><!--/noindex--></p>
<p style="text-align: center;">
<!-- noindex--><a rel= "nofollow" href="https://мыло-опт.com.ua/konkurs-raspakovka /" ><span style= " font-family: Arial, Helvetica; font-size: 14pt;">1</span></a><!--/noindex--><span style="font-family: Arial, Helvetica; font-size: 14pt;">&nbsp; &nbsp;</span>
<!-- noindex--><a rel= "nofollow" href="https://мыло-опт.com.ua/konkurs-raspakovka-page-2 /" ><span style= " font-family: Arial, Helvetica; font-size: 14pt;">2</span></a><!--/noindex--><span style="font-family: Arial, Helvetica; font-size: 14pt;">&nbsp;&nbsp;</span><span style="font-family: Arial, Helvetica; font-size: 14pt;"> </span>
<!-- noindex--><a rel= "nofollow" href="https://мыло-опт.com.ua/konkurs-raspakovka-page-3 /" ><span style= " font-family: Arial, Helvetica; font-size: 14pt;">3</span></a><!-- /noindex--><span style= " font-family: Arial, Helvetica; font-size: 14pt;">&nbsp;  </span><a href="https://мыло-опт.com.ua/konkurs-raspakovka-page-4/"><span style="font-family: Arial, Helvetica; font-size: 14pt;">4</span></a><span style= " font-family: Arial, Helvetica; font-size: 14pt;">&nbsp;&nbsp;</span><span style= " font-family: Arial, Helvetica; font-size: 14pt; " > </span><a href="https://мыло-опт.com.ua/konkurs-raspakovka-page-5/"><span style="font-family: Arial, Helvetica; font-size: 14pt;">5</span></a><span style="font-family: Arial, Helvetica; font-size: 14pt;">&nbsp;&nbsp;</span><span style="font-family: Arial, Helvetica; font-size: 14pt;"> </span><a href="https://мыло-опт.com.ua/konkurs-raspakovka-page-6/"><span style="font-family: Arial, Helvetica; font-size: 14pt;">6</span></a><span style="font-family: Arial, Helvetica; font-size: 14pt;">&nbsp;&nbsp;</span><span style="font-family: Arial, Helvetica; font-size: 14pt;"> </span>
<!-- noindex--><a rel= "nofollow" href="https://мыло-опт.com.ua/konkurs-raspakovka-page-7 /" ><span style= " font-family: Arial, Helvetica; font-size: 14pt;">7</span></a><!--/noindex--><span style="font-family: Arial, Helvetica; font-size: 14pt;">&nbsp;&nbsp;</span><span style="font-family: Arial, Helvetica; font-size: 14pt;"> </span><span style="font-family: Arial, Helvetica; font-size: 14pt;">
<!-- noindex--><a rel= "nofollow" href="https://мыло-опт.com.ua/konkurs-raspakovka-page-8/" >8</a><!-- / noindex-->&nbsp;  </span><span style="font-family: Arial, Helvetica; font-size: 14pt;" > </span>
<!-- noindex--><a rel= "nofollow" href="https://мыло-опт.com.ua/konkurs-raspakovka-page-9 /" ><span style= " font-family: Arial, Helvetica; font-size: 14pt;">9</span></a><!--/noindex--><span style="font-family: Arial, Helvetica; font-size: 14pt;">&nbsp;&nbsp;</span><span style="font-family: Arial, Helvetica; font-size: 14pt;"> </span> <span style="font-family: Arial, Helvetica; font-size: 14pt;">
<!-- noindex--><a rel= "nofollow" href="https://мыло-опт.com.ua/konkurs-raspakovka-page-10/" >10</a><!-- / noindex-->  </span>
<!-- noindex--><a rel= "nofollow" href="https://мыло-опт.com.ua/konkurs-raspakovka-page-11 /" ><span style= " font-family: Arial, Helvetica; font-size: 14pt;">11</span></a><!--/noindex--><!-- noindex--><a rel= "nofollow" href="https://мыло-опт.com.ua/konkurs-raspakovka-page-12 /" ><span style= " font-family: Arial, Helvetica; font-size: 14pt;"> 12</span></a><!--/noindex--><!-- noindex--><a rel= "nofollow" href="https://мыло-опт.com.ua/konkurs-raspakovka-page-13 /" ><span style= " font-family: Arial, Helvetica; font-size: 14pt;"> 13</span></a><!--/noindex--><!-- noindex--><a rel= "nofollow" href="https://мыло-опт.com.ua/konkurs-raspakovka-page-14 /" ><span style= " font-family: Arial, Helvetica; font-size: 14pt;"> 14</span></a><!--/noindex--></p>
<p>
</p>
<p style="text-align: justify;">
<span style= "font-size: 14px;" ><span style= " font-family: arial,helvetica, sans-serif;" > кращі відео відгуки будуть розміщені на Youtube каналі магазину "мило-опт" </span></span>
</p>
<p style="text-align: justify;">
Після передачі відео, права на відео матеріали належать компанії мило Опт. Переконливе прохання не накладати свою музику на відео матеріали.
</p>
<p>
<span style= " font-size:14px;"><span style=" font-family: arial,helvetica,sans-serif; «>* бонусні кошти можуть бути використані на покупку товарів в інтернет-магазині» мило-опт " протягом двох місяців з моменту отримання бонусного коду.</span></span>
</p>
<p style="text-align: justify;">
<span style="font-size:14px;"> <span style="font-family:arial,helvetica,sans-serif;">персональні дані користувачів які взяли участь у конкурсі не надаються третім особам, але зберігаються адміністраторами інтернет-магазину «мило-опт» для зарахування виграних учасником конкурсу бонусних коштів на особистий рахунок користувача сайту і для надання послуг продажу товарів, представлених на сайті. Організатор конкурсу залишає за собою право використовувати отримані від учасників акції відеоматеріали в маркетингових цілях.</span></span>
</p>
<div class="buttons">
<a href="https://xn----utbcjbgv0e.com.ua/ua/ "class="button-cont-right" >продовжити<i class= "icon-circle-arrow-right" ></i></a> <br>
</div>
<p>
</p>
<br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>