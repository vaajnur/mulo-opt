<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty ("title", " політика безпеки магазину");
$APPLICATION->SetPageProperty ("keywords", " політика безпеки");
$APPLICATION->SetPageProperty("description", " політика безпеки");
$APPLICATION->SetTitle ("політика безпеки інтернет магазину мило-опт");
?><h1 class= "style-1" >Політика безпеки інтернет магазину мило-опт</h1>
<div class="box-container">
<p style="text-align: justify;">
<span style= " font-family:arial,helvetica,sans-serif;"><span style="font-size: 14px;">компанія мило-опт велику увагу приділяємо безпеці і захисту особистих даних користувачів нашого сайту. Відвідувачі мають можливість переглядати велику кількість сторінок вузла, при цьому, не повідомляючи ніякої інформації про себе. Ми хочемо запропонувати вам принципи дотримання безпеки, що пояснюють доступним способом як буде здійснюється збір ваших особистих даних. А також їх використання, які ви дізнаєтеся під час реєстрації. Рекомендується уважно ознайомитися з текстом, даної заяви про дотримання безпеки ваших особистих даних.</span></span>
</p>
<p style="text-align: justify;">
<span style= "font-size: 14px;" ><span style= " font-family: arial, helvetica, sans-serif;">менеджери інтернет-магазину Мыло-опт.сом.иа гарантують повне забезпечення інформації, отриманої від користувачів які зареєстровані.</span></span>
</p>
<p style="text-align: justify;">
<span style= " font-size:14px;"><span style="font-family: arial,helvetica,sans-serif;" >інформація, зазначена під час реєстрації, зберігається в закритій базі даних. Наш магазин "мило-ОПТ com.ua" гарантує конфіденційність під час використання замовлень вашої інформації.</span></span>
</p>
<p style="text-align: justify;">
<span style= " font-size:14px;"><span style="font-family: arial,helvetica,sans-serif;" >Ваші дані необхідні нам для зв'язку, а так само щоб здійснювати доставку товару за вказаними контактними даними. До того ж після реєстрації на сайті Ви отримуєте повний доступ до інформації та новинах нашого магазину, таких як акції, розпродажі, надходження нового товару, бонусних програм і багато іншого.</span></span>
</p>
<p style="text-align: justify;">
<span style= " font-size:14px;"><span style="font-family: arial,helvetica,sans-serif;" >при виникненні питань, або будь - яких складнощів із замовленнями-пишіть на адресу ел. Пошти &nbsp;</span></span>

<a href="mailto:mysoapoptom@gmail.com" >mysoapoptom@gmail.com</a> &nbsp;Ми швидко зможемо знайти рішення з Ваших питань!
</p>
<p style="text-align: justify;">
<span style= " font-size:14px;"><span style="font-family: arial,helvetica,sans-serif;" >Об'єкти, розміщені на нашому сайті стають власністю інтернет-магазину мило-опт. Використовувати такі об'єкти заборонено, без узгодження з адміністрацією інтернет - магазину.</span></span>
</p>
<h2>особисті відомості та безпека</h2>
<ol>
<li>
<p style="text-align: justify;">
<span style= " font-size: 14px;"><span style= "font-family:arial,helvetica, sans-serif;" >Мыло-опт.сом.иа дає 100% гарантію, що інформація, яку Ви надали, ніколи не буде передана третім особам.</span></span>
</p>
</li>
<li>
<p style="text-align: justify;">
<span style= " font-size: 14px;"><span style= "font-family:arial,helvetica, sans-serif;" >Мыло-опт.сом.иа може запросити реєстрацію для надання особистих відомостей. Інформація буде використана при обробці замовлення в інтернет-магазині.</span></span>
</p>
</li>
<li>
<p style="text-align: justify;">
<span style= " font-size:14px;"><span style="font-family: arial,helvetica,sans-serif; ">свої особисті дані можна змінити або оновити, при необхідності їх можна видалити в будь-який для вас зручний час, в розділі"Особистий кабінет".</span></span>
</p>
</li>
<li>
<p style="text-align: justify;">
<span style= " font-size:14px;"><span style="font-family: arial,helvetica,sans-serif;" >відомості на сайті мають лише інформативний характер.</span></span>
</p>
</li>
</ol>
<p style="text-align: justify;">
<span style= " font-size:14px;"><span style="font-family: arial,helvetica,sans-serif;" >співпрацюючи з нами, Ви знайдете безпеку і надійність, яку так важко знайти в наш час. Ми на ринку України і Росії працюємо, багато років, що дозволило нам створити базу вдячних клієнтів, далеко за межами України і колишнього СНД.</span></span>
</p>
<div class="buttons">
<div class="right">
<a href="https://xn----utbcjbgv0e.com.ua/ua/" class= "button-cont-right" > продовжити<i class= "icon-circle-arrow-right" ></i></a>
</div>
</div>
</div>
<br>
<div style="text-align: center;">
</div>
&nbsp; &nbsp;<br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>