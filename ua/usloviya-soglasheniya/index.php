<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty ("title", " умови угоди інтернет магазину мило опт.");
$APPLICATION->SetPageProperty ("ключові слова", " умови угоди, Загальні положення, умови оформлення замовлення, доставка.");
$APPLICATION->SetPageProperty ("опис", " умови угоди з інтернет-магазином мило-Опт, які регулюють правила оформлення замовлень, сод...");
$APPLICATION->setTitle ("умови угоди");
?><h1 class= "style-1" >Умови угоди</h1>
<div class="box-container">
<h2>1. Загальні положення </h2>
<p style="text-align: justify;">
<span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;">1.1. Ця Угода отримує силу між Покупцем і сайтом "мило-ОПТ" в особі менеджера магазину під час здійснення замовлення. </span > </span>
</p>
<p style="text-align: justify;">
<span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;">1.2. Під час замовлення Покупець погоджується з договором, обумовленим в Угоді нашого договору. Акцентом нашого договору для клієнтів є розташування замовлення через наш сайт "мило-ОПТ". </span > </span>
</p>
<p style="text-align: justify;">
<span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;">1.3. Здійснювати закупівлю в інтернет-магазині може будь-яка фізична а також юридична особа, здатна оплачувати і приймати вартість товару. </span></span>
</p>
<p style="text-align: justify;">
<span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;">1.4. Шановні клієнти, звертаємо Вашу увагу, що паспорти якості на продукцію видаються тільки оптовим покупцям після оплати замовлення, при індивідуальному запиті. На роздрібні позиції паспорти не видаються. Термін запиту на паспорт якості не більше 14 днів після здійснення та оплати Замовлення Покупцем. </span > </span>
</p>
<p style="text-align: justify;">
<span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;">1.5.&nbsp;просимо звернути вашу увагу що всі представлені на цьому сайті фотографії, призначені для ознайомчих цілей, більш детальну інформацію про товар ви можете отримати в характеристиці на сторінці товару.</span></span>
</p>
<p style="text-align: justify;">
 <span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;">1.6. Тексты описаний к товарам составлены методом компиляции из свободных источников,&nbsp;</span></span><span style="font-family: arial, helvetica, sans-serif; font-size: 14px;">документов на товар, рецептов предложенных авторами и пользователями сайта.&nbsp;</span><span style="font-family: arial, helvetica, sans-serif; font-size: 14px;">Информация, содержащаяся в описании товара, приведенные способы использования,&nbsp;</span><span style="font-family: arial, helvetica, sans-serif; font-size: 14px;">перечисленные свойства товара не являются исчерпывающими, носят ознакомительный&nbsp;</span><span style="font-family: arial, helvetica, sans-serif; font-size: 14px;">характер.</span>
	</p>
<p style="text-align: justify;">
<span style="font-family: arial, helvetica, sans-serif; font-size: 14px;">1.7. Функціонал додавання товарів у вкладку "Вибране" доступний тільки авторизованим користувачам сайту.</span>
</p>
	<p style="text-align: justify;">
 <span style="font-family: arial, helvetica, sans-serif; font-size: 14px;">1.8. Обращаем Ваше внимание: Указанные на сайте сроки годности могут не совпдать со сроками указанными на этикетке доставленного товара т.к. в связи с большим спросом, информация о сроках годности новой партии товара обновляется быстрее, чем происходит отправка товара заказаннного из предыдущей партии или наоборот.</span>
	</p>
<p style="text-align: justify;">
<span style="font-family: arial, helvetica, sans-serif; font-size: 14px;">1.9. За використання продукту в домашніх умовах наша компанія відповідальності не несе.</span>
</p>
<h2>2. Умови оформлення замовлення</h2>
<p style="text-align: justify;">
<span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;">2.1. Під час оформлення замовлення бажано, зареєструватися в нашому магазині, так само можливе оформлення замовлення і без реєстрації, з оплатою за фактом отримання товару.</span></span>
</p>
<p style="text-align: justify;">
<span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;">2.2. Клієнт може замовлення оформити наступним способом: замовити по телефону або оформити самостійно на сайті. Онлайн замовлення на сайті ми приймаємо цілодобово. Обробка замовлення здійснюється в робочий час з 9: 00 до 18: 00 з Понеділка по п'ятницю. </span > </span>
</p>
<p style="text-align: justify;">
<span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;">2.3. Обов'язковим, до оформлення замовлення, є ознайомлення клієнта з основними </span > </span > <span style = "font-family: arial, helvetica, sans-serif; font-size: 14px;" > характеристиками товару, які вказані під описом.</span>
</p>
<p style="text-align: justify;">
<span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;">2.4. Після того як оформлено замовлення клієнту ми надаємо інформацію про наявність товару і орієнтовні терміни відправки товару. Терміни формування, відправки і доставки залежать від: зарахування грошей на рахунок, кількості позицій товару замовляється клієнтом, а так само від логістики транспортної компанії. Повернення коштів після оплати товару на замовлення не здійснюється, всі рішення приймайте до того як оплачувати замовлення, уважно читайте умови угоди і відвантаження. </span></span>
</p>
<p style="text-align: justify;">
<span style= " font-size:14px;"><span style="font-family: arial,helvetica,sans-serif;" >2.5 Магазин в праві відмовити Покупцеві в прийнятті / обробці замовлення без пояснення на те причин. </span > </span>
</p>
<p style="text-align: justify;">
<span style= " font-size:14px;"><span style="font-family: arial,helvetica,sans-serif;" >2.6 свої побажання за кольором тари ви можете вказувати в коментарі до замовлення і ми до них обов'язково прислухаємося якщо даний колір є в наявності. </span></span>
</p>
<h2>3. Доставка</h2>
<p style="text-align: justify;">
<span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;">3.1. Здійснюється всіма зручними для замовника засобами з подальшою їх оплатою. </span></span>
</p>
<p style="text-align: justify;">
<span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;">3.2. По Україні, доставка до місця відправки товару здійснюється безкоштовно. Детальний опис способів і умов доставки ви знайдете на сторінці "Оплата і доставка". </span > </span>
</p>
<p style="text-align: justify;">
<span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;">3.3. Терміни доставки замовлення залежать від вашого регіону і адреси, також роботи служби доставки. </span > </span>
</p>
<p style="text-align: justify;">
<span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;">3.4.&nbsp;юридичні особи відвантажуються протягом 2-3 робочих днів по 100% передоплаті, після зарахування грошей на розрахунковий рахунок компанії. Зверніть увагу що зарахування грошей банківським переказом на розрахунковий рахунок може займати від 3 до 5 робочих днів. &nbsp; </span > </span>
</p>
<p style="text-align: justify;">
<span style= " font-size:14px;"><span style="font-family: arial,helvetica,sans-serif;" >3.5&nbsp; скло і букові кошики відправляються окремим місцем і їх доставку оплачує покупець. </span > </span>
</p>
<p style="text-align: justify;">
<span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;">3.6. При отриманні товару на руки від служб доставки, необхідно ретельно перевірити цілість і наявність товару на транспортній компанії. </span></span>
</p>
<p style="text-align: justify;">
<span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;">3.7. Якщо ви бажаєте застрахувати товари на повну вартість замовлення, необхідно повідомити нас про це, в такому випадку доставка буде здійснюється за рахунок покупця. &nbsp; </span></span>
</p>
<p style="text-align: justify;">
<span style= " font-size:14px;"><span style="font-family: arial,helvetica,sans-serif;" >3.8&nbsp; у разі виявлення нестачі або пошкодженні вантажу складається акт претензії при співробітнику Нової пошти на відділенні транспортної компанії. Без даного документа претензії не розглядаються. &nbsp; </span></span>
</p>
<p style="text-align: justify;">
<span style= "font-size: 14px;" ><span style= " font-family: arial,helvetica,sans-serif;">3.9 увага! Всі силіконові форми, молди і штампи виготовляються під замовлення, у зв'язку з чим можуть виникати затримки з відвантаженням.& і nbsp;</Спан></Спан>повернення коштів після оплати товару який виготовляється під замовлення після оплати і nbsp; не здійснюється, всі рішення приймайте до того як оплачувати замовлення, уважно читайте умови угоди і відвантаження.
</p>
<p style="text-align: justify;">
<span style= " font-size:14px;"><span style="font-family: arial,helvetica,sans-serif;" >3.10 Весь товар розфасований в тару тільки для транспортування, а не для зберігання. </span > </span>
</p>
<p style="text-align: justify;">
	<span style= " font-size:14px;"><span style="font-family: arial,helvetica,sans-serif;" ><strong>ми несемо відповідальність за товар до навантаження на склад служби доставки, за подальшу долю товару відповідають організації доставляють товар в пункт призначення. </strong > товари поставляються в тарі та упаковці для транспортування. Основні &nbsp; </span > </span><span style="font-family: arial,helvetica, sans-serif; font-size: 14px;" > характеристики товару вказані під кожним товаром і додатково не дублюються &nbsp;</span > <span style = " font-family: arial, helvetica, sans-serif; розмір шрифту: 14px; " >на етикетці.</span>
	</p>
	<h2>4. Оплата</h2>
	<p style="text-align: justify;">
 <span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;">4.1. Ціна на замовляється товар вказана на сайті за одиницю. </span > </span>
	</p>
	<p style="text-align: justify;">
 <span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;">4.2. Діє гнучка система знижок для оптових покупців. </span > </span>
	</p>
	<p style="text-align: justify;">
 <span style= " font-size:14px;"><span style="font-family: arial,helvetica,sans-serif;" >види бонусів і знижок, можуть змінюватися. </span > </span>
	</p>
	<p style="text-align: justify;">
 <span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;">4.3. Оплата здійснюється безготівковим або готівковим розрахунком. За безготівковим розрахунком необхідно здійснити 100% оплату на рахунок магазину «Мыло-опт.сом.иа " або відправити оплату на зазначені в рахунку реквізити компанії і повідомити нам в письмовому вигляді Про здійснення оплати, і nbsp;із зазначенням точної суми і номера замовлення.&nbsp;Ви зарахування грошей що зверніть увагу &nbsp;Ви юридичним і &nbsp;особам банківським переказом на розрахунковий рахунок може займати від 3 до &nbsp;Ви 5 робочих днів. </Спан></Спан>
	</p>
	<p>
 <span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;">4.4. Рахунок на оплату дійсний протягом доби. </span > </span>
	</p>
	<p style="text-align: justify;">
 <span style= " font-size:14px;"><span style="font-family: arial,helvetica,sans-serif;" >Товар нашого магазину потрапляє під постанову від 19.03.94 N 172, у зв'язку з цим ми не проводимо обмін і повернення товару,&nbsp; у разі форс мажору здійснюється за рахунок покупця. Товари куплені за акційними цінами, виготовлений на замовлення або зі знижкою поверненню та обміну не підлягають. &nbsp; </span > </span>
	</p>
	<p style="text-align: justify;">
 <span style= " font-size: 14px;"><span style="font-family:arial,helvetica,sans-serif;" ><strong>звертаємо Вашу увагу </strong>, що за умовами нашої співпраці з <strong > Новою Поштою </strong> посилки для відвантаження приймаються у відкритому вигляді, для забезпечення його цілісного і благополучного транспортування.
		 Дуже просимо вас перевіряти посилки при отриманні, як оплачені так і накладені замовлення. Всі розбіжності по накладній просимо фіксувати в акті прийому вантажу, щоб у разі виникнення питань ми могли негайно розібратися в даній ситуації. &nbsp; </span></span>
	</p>
	<h2>претензії та розірвання угоди.</h2>
	<p>
 <span style= " font-size:14px;"><span style="font-family: arial,helvetica,sans-serif; ">претензії до товару приймаються в телефонному режимі і за допомогою електронних &nbsp; </span > </span > <span style = "font-family: arial, helvetica, sans-serif; font-size: 14px;" > повідомлень, поштового листування. Порядок подання та розгляду претензій фізичних &nbsp; </span > <span style = "font-family: arial, helvetica, sans-serif; font-size: 14px;" > та / або юридичних осіб,і розірвання угоди здійснюється згідно норм </span > <span style = "font - family: arial, helvetica, sans-serif; font-size: 14px; ">цивільного, господарського кодексів України та Закону України" про права </span > <span style = «font-family: arial, helvetica, sans-serif; font-size: 14px;">споживачів". Акційний товар поверненню та обміну не підлягає.</span>
	</p>
	<p style="text-align: justify;">
 <span style= "font-size: 14px;" ><span style= " font-family: arial,helvetica,sans-serif;">сподіваємося на ваше розуміння &nbsp; І &nbsp; сприяння!</span></span>
	</p>
	<div class="buttons">
		<div class="right">
 <a href="https://xn----utbcjbgv0e.com.ua /" class= "button-cont-right" > продовжити <i class = "icon-circle-arrow-right" ></i></a>
		</div>
	</div>
</div>
 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>