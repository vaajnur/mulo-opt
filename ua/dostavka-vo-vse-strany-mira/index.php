<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty ("title", " інформація про доставку в усі країни світу.");
$APPLICATION->SetPageProperty ("keywords", " доставка, в інші країни, доставка по всьому світу, тарифи.");
$APPLICATION->SetPageProperty ("description", " тарифи на доставку в усі країни світу вагою від 100 грам до 2 кг");
$APPLICATION->SetTitle ("Доставка в усі країни світу");
?><h1 class="style-1" > Доставка в усі країни світу</h1>

<div class="box-container">
<h3>Тарифи на доставку в усі країни світу вагою від 100 грам до 2 кг</h3>

<p style= " text-align:justify;"><span style="font-size:14px;"><span style="font-family: arial,helvetica,sans-serif;">компанія мило-опт надає для своїх міжнародних покупців тарифи на доставку Ваших замовлень по всьому світу. Для міжнародних відправок з товарними вкладеннями до 2 кг вид відправлення" дрібний пакет", вага від 1 до 2 кг тарифи на дрібні пакети градуються наступним чином:</span></span></p>

<p style= "text-align: justify;" ><span style= "font-size:14px;" ><span style= " font-family: arial, helvetica, sans-serif;">0 грам — 100 грам, 100 грам — 250 грам, 250 грам — 500 грам, 500 грам — 1 кг, 1 кг — 2 кг.</span></span></p>

<h3>детальна інформація за тарифами: &nbsp;</h3>

<p style= " text-align:justify;"><span style="font-size:14px;"><span style="font-family: arial,helvetica,sans-serif;">тарифи на міжнародні відправлення прийняті в доларах, наша компанія оплачує доставку Ваших замовлень з України в гривнях за курсом ПриватБанку, з цього вартість в гривнях може змінюватися (курс ПриватБанку змінюється кожен робочий день).</span></span></p>

<p style= "text-align: justify;" ><span style= "font-size:14px;" ><span style= "font-family: arial, helvetica, sans-serif;" >Вид відправлення "дрібний пакет" до 2 кг, єдиний тариф в доларах в усі країни: &nbsp;</span></span></p>

<table border="1" cellpadding="1" cellspacing="1">
<thead>
<tr>
<th scope="col"><span style="font-size:14px;"><span style= "font-family: arial, helvetica, sans-serif;" >вага посилки</span></span></th>
<th scope="col"><span style="font-size:14px;"><span style= "font-family: arial, helvetica, sans-serif;" >Вид доставки наземний</span></span></th>
<th scope="col"><span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;" >Вид доставки АВІА</span></span></th>
<th scope="col"><span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;" >Валюта</span></span></th>
</tr>
</thead>
<tbody>
<tr>
<td style= " text-align:center;"><span style="font-size: 14px;"><span style="font-family: arial, helvetica, sans-serif;" >0 – 100 грам</span></span></td>
<td style="text-align: center;"><span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;">3.99$</span></span></td>
<td style="text-align: center;"><span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;">4.32$&nbsp;</span></span></td>
<td style= " text-align:center;"><span style="font-size:14px;"><span style="font-family: arial,helvetica,sans-serif;" >Долар США</span></span></td>
</tr>
<tr>
<td style= " text-align:center;"><span style="font-size: 14px;"><span style="font-family: arial, helvetica, sans-serif;" >100 – 200 грам</span></span></td>
<td style="text-align: center;"><span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;">5.38$</span></span></td>
<td style="text-align: center;"><span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;">6.18$&nbsp;</span></span></td>
<td style= " text-align:center;"><span style="font-size:14px;"><span style="font-family: arial,helvetica,sans-serif;" >Долар США</span></span></td>
</tr>
<tr>
<td style= " text-align:center;"><span style="font-size: 14px;"><span style="font-family: arial, helvetica, sans-serif;" >250 – 500 грам</span></span></td>
<td style="text-align: center;"><span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;">7.96$</span></span></td>
<td style="text-align: center;"><span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;">9.44$&nbsp;</span></span></td>
<td style= " text-align:center;"><span style="font-size:14px;"><span style="font-family: arial,helvetica,sans-serif;" >Долар США</span></span></td>
</tr>
<tr>
<td style= " text-align:center;"><span style="font-size: 14px;"><span style="font-family: arial, helvetica, sans-serif;" >500 – 1000 грам</span></span></td>
<td style="text-align: center;"><span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;">11.86$</span></span></td>
<td style="text-align: center;"><span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;">14.50$&nbsp;</span></span></td>
<td style= " text-align:center;"><span style="font-size:14px;"><span style="font-family: arial,helvetica,sans-serif;" >Долар США</span></span></td>
</tr>
<tr>
<td style="text-align: center;"><span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;">1 – 2 кг&nbsp;</span></span></td>
<td style="text-align: center;"><span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;">21.11$</span></span></td>
<td style="text-align: center;"><span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;">22.11$&nbsp;</span></span></td>
<td style= " text-align:center;"><span style="font-size:14px;"><span style="font-family: arial,helvetica,sans-serif;" >Долар США</span></span></td>
</tr>
</tbody>
</table>

<h3>тарифи на доставку в усі країни світу вагою від 2 кг</h3>

<p style= " text-align:justify;"><span style="font-size:14px;"><span style="font-family: arial,helvetica,sans-serif;">нижче наводимо таблицю тарифів доставки по країнах світу для всіх економічних зон.</span></span></p>

<h3 style= " text-align:justify;"><span style="font-size:14px;"><span style="font-family: arial,helvetica,sans-serif;">приклад розрахунку вашого вантажу для Росії.</span></span></h3>

<p style="text-align: justify;"><span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;">для Росії за оформлення посилки 15,70$ (дивись в таблицю) плюс за 1 кг наземним транспортом 2,15$ або АВІА транспортом 3,50$ (округлення маси до 100 грам в більшу сторону+ вага упаковки від 100-500 грам в залежності від величини вашого замовлення).&nbsp;</span></span></p>

<table border="1" cellpadding="1" cellspacing="1">
<thead>
<tr>
<th scope="col">&nbsp;</th>
<th colspan= " 3 "rowspan=" 1 "scope= "col"><strong><span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;" >Тариф, дол.&nbsp;США</span></span></strong></th>
</tr>
</thead>
<tbody>
<tr>
<td colspan= " 1 "rowspan=" 2 "style=" text-align:center;"><strong><span style="font-size:14px;"><span style="font-family: arial, helvetica, sans-serif;" >країна призначення</span></span></strong></td>
<td colspan= " 1 "rowspan=" 2 "style=" text-align:center;"><span style="font-size:14px;"><span style="font-family: arial,helvetica,sans-serif;">за посилку</span></span></td>
<td colspan="2" rowspan="1" style="text-align: center;"><span style="font-size:14px;"><span style="font-family:arial, helvetica, sans-serif;" >за 1 кг</span></span></td>
</tr>
<tr>
<td style= " text-align:center;"><span style="font-size:14px;"><span style="font-family: arial,helvetica,sans-serif;"><span > </span></td>
<td style= " width: 50px; text-align:center;"><span style="font-size:14px;"><span style="font-family: arial,helvetica,sans-serif;" >наземним (комбінованим SAL) транспортом</span></span></td>
</tr>
<tr>
<td style= "text-align: justify;" ><span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;" >Азербайджан</span></span></td>
<td style="text-align: justify;"><span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;">12,00</span></span></td>
<td style="text-align: justify;"><span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;">2,80</span></span></td>
<td style="text-align: justify;"><span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;">2,60</span></span></td>
</tr>
<tr>
<td style= "text-align: justify;" ><span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;" >Білорусь</span></span></td>
<td style="text-align: justify;"><span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;">9,20</span></span></td>
<td style="text-align: justify;"><span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;">2,50</span></span></td>
<td style="text-align: justify;"><span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;">2,00</span></span></td>
</tr>
<tr>
<td style= " text-align:justify;"><span style="font-size:14px;"><span style="font-family: arial,helvetica,sans-serif;" >Вірменія</span></span></td>
<td style="text-align: justify;"><span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;">13,70</span></span></td>
<td style="text-align: justify;"><span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;">2,45</span></span></td>
<td style="text-align: justify;"><span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;">2,05</span></span></td>
</tr>
<tr>
<td style= " text-align:justify;"><span style="font-size:14px;"><span style="font-family: arial,helvetica,sans-serif;" >Грузія</span></span></td>
<td style="text-align: justify;"><span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;">11,20</span></span></td>
<td style="text-align: justify;"><span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;">3,00</span></span></td>
			<td style="text-align: justify;"><span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;">2,80</span></span></td>
		</tr>
<tr>
<td style= " text-align:justify;"><span style="font-size:14px;"><span style="font-family: arial,helvetica,sans-serif;" >Казахстан</span></span></td>
<td style="text-align: justify;"><span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;">9,90</span></span></td>
<td style="text-align: justify;"><span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;">4,45</span></span></td>
<td style="text-align: justify;"><span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;">2,50</span></span></td>
</tr>
<tr>
<td style= "text-align: justify;" ><span style= "font-size:14px;" ><span style= "font-family: arial, helvetica, sans-serif;" >Киргизстан</span></span></td>
<td style="text-align: justify;"><span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;">9,90</span></span></td>
<td style="text-align: justify;"><span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;">3,30</span></span></td>
<td style="text-align: justify;"><span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;">2,30</span></span></td>
</tr>
<tr>
<td style= " text-align:justify;"><span style="font-size:14px;"><span style="font-family: arial,helvetica,sans-serif;" >Молдова</span></span></td>
<td style="text-align: justify;"><span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;">13,00</span></span></td>
<td style="text-align: justify;"><span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;">2,80</span></span></td>
<td style="text-align: justify;"><span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;">2,50</span></span></td>
</tr>
<tr>
<td style= "text-align: justify;" ><span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;" >Таджикистан</span></span></td>
<td style="text-align: justify;"><span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;">8,70</span></span></td>
<td style="text-align: justify;"><span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;">3,25</span></span></td>
<td style="text-align: justify;"><span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;">2,20</span></span></td>
</tr>
<tr>
<td style= "text-align: justify;" ><span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;" >Туркменістан</span></span></td>
<td style="text-align: justify;"><span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;">7,75</span></span></td>
<td style="text-align: justify;"><span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;">2,85</span></span></td>
<td style="text-align: justify;"><span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;">1,95</span></span></td>
</tr>
<tr>
<td style= "text-align: justify;" ><span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;" >Узбекистан</span></span></td>
<td style="text-align: justify;"><span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;">14,00</span></span></td>
<td style="text-align: justify;"><span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;">3,60</span></span></td>
<td style="text-align: justify;"><span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;">2,65</span></span></td>
</tr>
<tr>
<td style= " text-align:justify;"><span style="font-size:14px;"><span style="font-family: arial,helvetica,sans-serif;" > Росія</span></span></td>
<td style="text-align: justify;"><span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;">15,70</span></span></td>
<td style="text-align: justify;"><span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;">3,50</span></span></td>
<td style="text-align: justify;"><span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;">2,15</span></span></td>
</tr>
<tr>
<td style= " text-align:justify;"><span style="font-size: 14px;"><span style="font-family: arial, helvetica, sans-serif;" >Східна Європа</span></span></td>
<td style="text-align: justify;"><span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;">13,70</span></span></td>
<td style="text-align: justify;"><span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;">2,50</span></span></td>
<td style="text-align: justify;"><span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;">2,10</span></span></td>
</tr>
<tr>
<td style= "text-align: justify;" ><span style= "font-size:14px;" ><span style= "font-family: arial, helvetica, sans-serif;" >Центральна, Північна Європа</span></span></td>
<td style="text-align: justify;"><span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;">16,50</span></span></td>
<td style="text-align: justify;"><span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;">2,80</span></span></td>
<td style="text-align: justify;"><span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;">2,20</span></span></td>
</tr>
<tr>
<td style= " text-align:justify;"><span style="font-size: 14px;"><span style="font-family: arial, helvetica, sans-serif;" >Західна Європа</span></span></td>
<td style="text-align: justify;"><span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;">16,40</span></span></td>
<td style="text-align: justify;"><span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;">3,30</span></span></td>
<td style="text-align: justify;"><span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;">2,10</span></span></td>
</tr>
<tr>
<td style= " text-align:justify;"><span style="font-size: 14px;"><span style="font-family: arial, helvetica, sans-serif;" >Центральна Азія, Близький Схід</span></span></td>
<td style="text-align: justify;"><span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;">14,00</span></span></td>
<td style="text-align: justify;"><span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;">2,60</span></span></td>
<td style="text-align: justify;"><span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;">2,00</span></span></td>
</tr>
<tr>
<td style= " text-align:justify;"><span style="font-size: 14px;"><span style="font-family: arial, helvetica, sans-serif;" >Північна Америка</span></span></td>
<td style="text-align: justify;"><span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;">10,80</span></span></td>
<td style="text-align: justify;"><span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;">5,80</span></span></td>
<td style="text-align: justify;"><span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;">3,30</span></span></td>
</tr>
<tr>
<td style= " text-align:justify;"><span style="font-size: 14px;"><span style="font-family: arial, helvetica, sans-serif;" >Східна Азія</span></span></td>
<td style="text-align: justify;"><span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;">12,00</span></span></td>
<td style="text-align: justify;"><span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;">7,10</span></span></td>
<td style="text-align: justify;"><span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;">3,60</span></span></td>
</tr>
<tr>
<td style= " text-align:justify;"><span style="font-size: 14px;"><span style="font-family: arial, helvetica, sans-serif;" >Африка, Південна і Центральна Америка</span></span></td>
<td style="text-align: justify;"><span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;">14,50</span></span></td>
<td style="text-align: justify;"><span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;">7,00</span></span></td>
<td style="text-align: justify;"><span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;">5,50</span></span></td>
</tr>
<tr>
<td style= " text-align:justify;"><span style="font-size: 14px;"><span style="font-family: arial, helvetica, sans-serif;" >Австралія та Океанія</span></span></td>
<td style="text-align: justify;"><span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;">10,40</span></span></td>
<td style="text-align: justify;"><span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;">11,50</span></span></td>
<td style="text-align: justify;"><span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;">6,00</span></span></td>
</tr>
</tbody>
</table>

<p style= " text-align:justify;"><span style="font-size:14px;"><span style="font-family: arial,helvetica,sans-serif;">для посилки можна оголосити цінність (застрахувати, для дрібного пакету не можна), тоді до вартості за посилку і маса додається плюс 3,60$ за страховку (максимальна сума страховки яку можна вказати еквівалентна 150 євро).&nbsp;</span></span></p>

<p style= " text-align:justify;"><span style="font-size:14px;"><span style="font-family: arial,helvetica,sans-serif;">компанія мило-опт завжди намагається зменшити вартість доставки Ваших замовлень до вас додому. Реєструйтеся в нашому інтернет магазині натуральних інгредієнтів для косметики і отримуйте до 10% знижку на Ваші замовлення для постійних покупців.</span></span></p>

<p style= "text-align: justify;" ><span style= "font-size:14px;" ><span style="font-family:arial,helvetica,sans-serif;" >Милотворите з нами на здоров'я!</span></span></p>
    <div class="buttons">
      <div class="right"><a href="https://xn----utbcjbgv0e.com.ua/ua/" class="button-cont-right">Продолжить<i class="icon-circle-arrow-right"></i></a></div>
    </div>
  </div><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>