<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Задайте питання");
?><h1>Контактна інформація</h1>
 <?$APPLICATION->IncludeComponent(
	"bitrix:menu",
	"personal",
	Array(
		"ALLOW_MULTI_SELECT" => "N",
		"CHILD_MENU_TYPE" => "",
		"COMPONENT_TEMPLATE" => "personal",
		"DELAY" => "N",
		"MAX_LEVEL" => "1",
		"MENU_CACHE_GET_VARS" => array(),
		"MENU_CACHE_TIME" => "3600000",
		"MENU_CACHE_TYPE" => "A",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"ROOT_MENU_TYPE" => "about",
		"USE_EXT" => "N"
	)
);?>
<div class="box">
	<div style="border-bottom:1px solid #CFDFEA;">
	</div>
	<div class="box-content">
		<div class="box-html">
			<p style="margin: 0px 0px 20px; color: #7e7e7e; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; line-height: 18px; text-align: justify;">
 <span style="font-family:arial,helvetica,sans-serif;"><span style="font-size: 14px;">При оформленні замовлення просимо вас користуватися &nbsp;онлайн формою &nbsp;<strong>("кошик")</strong> &nbsp;або нашою електронною поштою &nbsp;<strong><a href="mailto:mysoapoptom@gmail.com" style="outline: none;" target="_blank">mysoapoptom@gmail.com⁠</a></strong>. Електронна пошта магазину мило-Опт обробляється практично в цілодобовому режимі, тому всі Ваші замовлення, питання, побажання будуть оброблені і розглянуті найближчим часом. Так само велике прохання повідомляти про оплату замовленого товару також по електронній пошті.</span></span>
			</p>
			<p style="margin: 0px 0px 20px; color: #7e7e7e; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; line-height: 18px; text-align: justify;">
 <span style="font-family:arial,helvetica,sans-serif;"><span style="font-size: 14px;">У разі виникнення невідкладних питань, а також при необхідності їх термінового вирішення, ви можете зв'язуватися з нашими менеджерами через форму зворотного зв'язку на сайті або через вище зазначену електронну пошту. Ми намагаємося що б кожен клієнт вчасно отримав своє замовлення.</span></span>
			</p>
			<p style="margin: 0px 0px 20px; color: #7e7e7e; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; line-height: 18px; text-align: justify;">
 <span style="font-family:arial,helvetica,sans-serif;"><span style="font-size: 14px;">Дякуємо за ваше розуміння та співпрацю!</span></span>
			</p>
		</div>
	</div>
</div>
<ul class="contactList">
	<li>
	<table>
	<tbody>
	<tr>
		<td>
 <img alt="cont1.png" src="/bitrix/templates/dresscode/images/cont1.png" title="cont1.png">
		</td>
		<td>
			 +38 (063) 6900672
		</td>
	</tr>
	</tbody>
	</table>
 </li>
	 <?/*<li>
		<table>
		<tbody>
		<tr>
			<td>
				<img alt="cont2.png" src="<?=SITE_TEMPLATE_PATH?>/images/cont2.png" title="cont2.png">
			</td>
			<td>
 <a href="mailto:info@dw24.su">info@dw24.su</a><br>
 <a href="mailto:support@dw24.su">support@dw24.su</a><br>
			</td>
		</tr>
		</tbody>
		</table>
</li>*/?>
	<li>
	<table>
	<tbody>
	<tr>
		<td>
 <img alt="cont1.png" src="/bitrix/templates/dresscode/images/cont1.png" title="cont1.png">
		</td>
		<td>
			 ФАКС: +38 (095) 8113091
		</td>
	</tr>
	</tbody>
	</table>
 </li>
	<li>
	<table>
	<tbody>
	<tr>
		<td>
 <img alt="cont3.png" src="/bitrix/templates/dresscode/images/cont3.png" title="cont3.png">
		</td>
		<td>1-й Піщаний тупик, 1, <br>
Кременчук, Полтавська область, Україна&nbsp; &nbsp;</td>
	</tr>
	</tbody>
	</table>
 </li>
	 <?/*<li>
		<table>
		<tbody>
		<tr>
			<td>
 <img alt="cont4.png" src="<?=SITE_TEMPLATE_PATH?>/images/cont4.png" title="cont4.png">
			</td>
			<td>
				 Пн-Пт : с 10:00 до 20:00<br>
				 Сб, Вс : выходной<br>
			</td>
		</tr>
		</tbody>
		</table>
 </li>*/?>
</ul>
 <img width="485" alt="Мы на карте" src="/upload/medialibrary/6f2/map.jpg" height="291" title="Мы на карте"><br>
 <br>
 <br>
		<?$APPLICATION->IncludeComponent(
	"bitrix:form.result.new",
	".default",
	Array(
		"CACHE_TIME" => "360000",
		"CACHE_TYPE" => "Y",
		"CHAIN_ITEM_LINK" => "",
		"CHAIN_ITEM_TEXT" => "",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_URL" => "",
		"IGNORE_CUSTOM_TEMPLATE" => "N",
		"LIST_URL" => "",
		"SEF_MODE" => "N",
		"SUCCESS_URL" => "",
		"USE_EXTENDED_ERRORS" => "Y",
		"VARIABLE_ALIASES" => array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID",),
		"WEB_FORM_ID" => "4"
	)
);?><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php")?>