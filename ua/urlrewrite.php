<?
$arUrlRewrite = array(
	array(
		"CONDITION" => "#^={\$arResult[\"FOLDER\"].\$arResult[\"URL_TEMPLATES\"][\"smart_filter\"]}\\??(.*)#",
		"RULE" => "&\$1",
		"ID" => "bitrix:catalog.smart.filter",
		"PATH" => "/bitrix/templates/dresscode/components/bitrix/catalog/.default/section.php",
	),
	array(
		"CONDITION" => "#^/bitrix/services/ymarket/#",
		"RULE" => "",
		"ID" => "",
		"PATH" => "/bitrix/services/ymarket/index.php",
		"SORT" => "100",
	),
	array(
		"CONDITION" => "#^/stssync/calendar/#",
		"RULE" => "",
		"ID" => "bitrix:stssync.server",
		"PATH" => "/bitrix/services/stssync/calendar/index.php",
		"SORT" => "100",
	),
	array(
		"CONDITION" => "#^/personal/order/#",
		"RULE" => "",
		"ID" => "bitrix:sale.personal.order",
		"PATH" => "/personal/order/index.php",
		"SORT" => "100",
	),
	array(
		"CONDITION" => "#^/ua/catalog/#",
		"RULE" => "",
		"ID" => "bitrix:catalog",
		"PATH" => "/ua/catalog/index.php",
	),
	array(
		"CONDITION" => "#^/collection/#",
		"RULE" => "",
		"ID" => "bitrix:news",
		"PATH" => "/collection/index.php",
	),
	array(
		"CONDITION" => "#^/ua/brands/#",
		"RULE" => "",
		"ID" => "bitrix:news",
		"PATH" => "/ua/brands/index.php",
	),
	array(
		"CONDITION" => "#^/ua/survey/#",
		"RULE" => "",
		"ID" => "bitrix:news",
		"PATH" => "/ua/survey/index.php",
	),
	array(
		"CONDITION" => "#^/ua/stock/#",
		"RULE" => "",
		"ID" => "bitrix:news",
		"PATH" => "/ua/stock/index.php",
	),
	array(
		"CONDITION" => "#^/services/#",
		"RULE" => "",
		"ID" => "bitrix:news",
		"PATH" => "/services/index.php",
		"SORT" => "100",
	),
	array(
		"CONDITION" => "#^/ua/news/#",
		"RULE" => "",
		"ID" => "bitrix:news",
		"PATH" => "/ua/news/index.php",
	),
	array(
		"CONDITION" => "#^/survey/#",
		"RULE" => "",
		"ID" => "bitrix:news",
		"PATH" => "/survey/index.php",
		"SORT" => "100",
	),
	array(
		"CONDITION" => "#^/stores/#",
		"RULE" => "",
		"ID" => "bitrix:catalog.store",
		"PATH" => "/stores/index.php",
		"SORT" => "100",
	),
	array(
		"CONDITION" => "#^/brands/#",
		"RULE" => "",
		"ID" => "bitrix:news",
		"PATH" => "/brands/index.php",
		"SORT" => "100",
	),
	array(
		"CONDITION" => "#^/stock/#",
		"RULE" => "",
		"ID" => "bitrix:news",
		"PATH" => "/stock/index.php",
		"SORT" => "100",
	),
	array(
		"CONDITION" => "#^/store/#",
		"RULE" => "",
		"ID" => "bitrix:catalog.store",
		"PATH" => "/store/index.php",
		"SORT" => "100",
	),
	array(
		"CONDITION" => "#^/forum/#",
		"RULE" => "",
		"ID" => "bitrix:forum",
		"PATH" => "/forum/index.php",
		"SORT" => "100",
	),
	array(
		"CONDITION" => "#^/blog/#",
		"RULE" => "",
		"ID" => "bitrix:news",
		"PATH" => "/blog/index.php",
	),
	array(
		"CONDITION" => "#^/news/#",
		"RULE" => "",
		"ID" => "bitrix:news",
		"PATH" => "/news/index.php",
	),
	array(
		"CONDITION" => "#^/#",
		"RULE" => "",
		"ID" => "bitrix:catalog",
		"PATH" => "/catalog/index.php",
	),
);

?>