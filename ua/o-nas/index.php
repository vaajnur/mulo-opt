<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Про нас-магазин мило-опт.");
$APPLICATION->SetPageProperty("keywords", "про магазин мило-опт, команда магазину.");
$APPLICATION->SetPageProperty("description", "Мило-опт - це великий асортимент товарів для виготовлення косметики та миловаріння! У нас постав...");
$APPLICATION->SetTitle("Про нас-інтернет магазин мило-опт");
?><h1><br>
 О нас - интернет магазин Мыло-опт</h1>
 <?$APPLICATION->IncludeComponent(
	"bitrix:menu",
	"personal",
	Array(
		"ALLOW_MULTI_SELECT" => "N",
		"CHILD_MENU_TYPE" => "",
		"COMPONENT_TEMPLATE" => "personal",
		"DELAY" => "N",
		"MAX_LEVEL" => "1",
		"MENU_CACHE_GET_VARS" => array(),
		"MENU_CACHE_TIME" => "3600000",
		"MENU_CACHE_TYPE" => "A",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"ROOT_MENU_TYPE" => "about",
		"USE_EXT" => "N"
	)
);?>
<div class="box-container">
	<p style="text-align: justify;">
	</p>
	<h1>Про нас-інтернет магазин мило-опт</h1>
 <span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;">Наш магазин, мило-опт - це великий асортимент товарів і величезний досвід миловаріння! У нас знайдеться все, що Вам необхідно – від мильної основи, до пакетиків з органзи для упаковки приготованого мила.</span></span>
	<p>
	</p>
	<p style="text-align: justify;">
 <span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;">У нас налагоджені зв'язки з виробниками з різних країн, де ми закуповуємо кращі товари для Вас - з усього світу!</span></span>
	</p>
	<p style="text-align: justify;">
 <span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;">Для нас не існує кордонів-ми здійснюємо доставку в усі куточки в будь-якій країні світу.</span></span>
	</p>
	<p style="text-align: justify;">
 <span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;">Ми не просто продаємо товар - ми навчаємо і радимо як досягати майстерності.</span></span>
	</p>
	<p style="text-align: justify;">
 <span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;">Зусилля команди інтернет-магазину спрямовані на задоволення побажань кожного нашого покупця. Нами рухає бажання догодити всім клієнтам, долучити кожного до чарівного і захоплюючого світу миловаріння з рецептами наших миловарів! Ми допомагаємо Вам вибирати необхідні косметичні компоненти, розповідаємо і показуємо, як їх застосувати. Ми пропонуємо не тільки якісну і ексклюзивну продукцію, ми пропонуємо-наш досвід, поради та консультації з виготовлення натуральної рукотворної косметики.</span></span>
	</p>
	<p style="text-align: justify;">
 <span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;">У нас можна придбати не тільки основи для мила, а й аромати, люфа, глітери, рідкі барвники, сухі і рідкі пігменти, цілющі екстракти, антиоксиданти, гідролати, скраби, пави, гліцерин, мінеральні солі, D-пантенол, базові ефірні масла, активні фітокомплекси і багато інших натуральні косметичні компоненти. В асортименті представлені інвентар та інструменти для миловаріння, валяння, виготовлення свічок і цукерок за найдоступнішими цінами на Україні.</span></span>
	</p>
 <figure class="caption" style="float:left"><img alt="Наша новорічна команда мило-опт" src="https://xn----utbcjbgv0e.com.ua/image/data/banner/kommanda_miloopt.jpg"> <figcaption>Наша новорічна команда мило-опт</figcaption> </figure>
	<p style="text-align: justify;">
 <span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;">Наш магазин-це ще й коло друзів і партнерів. Ми раді, що до нас з кожним роком, приєднується все більше однодумців - тих, для кого корисна натуральна косметика є невід'ємною життєвою потребою. Це наша творчість і наш бізнес. Нам приємно розмовляти однією мовою!</span></span>
	</p>
	<p style="text-align: justify;">
 <span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;">Інтернет-магазин "мило-опт" почав свою роботу в 2011 році. Ми-молода і активно розвивається команда. Ми постійно прагнемо покращувати наш магазин і вносимо щось нове, щоб Вам було зручніше і затишніше з нами.</span></span>
	</p>
	<p style="text-align: justify;">
 <span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;">Звичайно, найцікавіше ще попереду! Ми намагаємося працювати відповідно до ваших побажань і сподіваємося виправдати вашу довіру. Висловлюйте Ваші побажання щодо поліпшення сервісу і якості обслуговування, зручності користування магазином і змістовного наповнення сайту.</span></span>
	</p>
	<p style="text-align: justify;">
 <span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;">У вас є питання по виготовленню та застосуванню натуральної косметики-запитуйте!</span></span>
	</p>
	<p style="text-align: justify;">
 <span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;">Ви хочете розвиватися в колі однодумців-Ласкаво просимо!</span></span>
	</p>
	<div class="buttons">
		<div class="right">
 <a href="https://xn----utbcjbgv0e.com.ua/" class="button-cont-right">Продовживши<i class="icon-circle-arrow-right"></i></a>
		</div>
	</div>
</div>
 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>