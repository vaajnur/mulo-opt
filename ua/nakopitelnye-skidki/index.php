<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty ("title", " Накопичувальні знижки магазину мило-опт.");
$APPLICATION->SetPageProperty ("keywords", " накопичувальні знижки, знижки магазину, накопичувальна програма.");
$APPLICATION->SetPageProperty ("description", " система накопичувальних знижок для всіх клієнтів магазину. Накопичувальні знижки 3, 5, 7, 10% на всі Т...");
$APPLICATION->SetTitle ("Накопичувальні знижки інтернет магазину мило-опт");
?><h1 class= "style-1" >Накопичувальні знижки інтернет магазину мило-опт</h1>

<div class="box-container">
    <p style= " text-align:justify;"><span style="font-size:14px;"><span style="font-family: arial,helvetica,sans-serif;">у нашому магазині працює <strong>накопичувальна система знижок</strong>. Наші покупці отримують наступні знижки в залежності від суми, на яку були зроблені їх попередні покупки:</span></span > </p>

<p style= " text-align:justify;"><span style="font-size:14px;"><span style="font-family: arial,helvetica,sans-serif;" ><img alt="" height="42" src= "http://xn----utbcjbgv0e.com.ua/image/data/logotip/3procent.png"width="67" >знижка 3%, при купівлі товарів на суму більше 300 грн, протягом останніх 180 днів</span></span></p>

<p style= "text-align: justify;" ><img alt= ""height= "42"src=" http://xn----utbcjbgv0e.com.ua/image/data/logotip/5procent.png " width="67"><span style="font-size:14px;"><span style="font-family: arial, helvetica, sans-serif;" > знижка 5%, при купівлі товарів на суму більше 1000 грн, протягом останніх 180 днів</span></span></p>

<p style= "text-align: justify;" ><img alt= ""height= "42"src=" http://xn----utbcjbgv0e.com.ua/image/data/logotip/7procent.png " width="67"><span style="font-size:14px;"><span style="font-family: arial, helvetica, sans-serif;" > знижка 7%, при купівлі товарів на суму понад 1500 грн, протягом останніх 180 днів</span></span></p>

<p style= "text-align: justify;" ><img alt= ""height= "42"src=" http://xn----utbcjbgv0e.com.ua/image/data/logotip/10procent.png " width="67"><span style="font-size:14px;"><span style="font-family: arial, helvetica, sans-serif;" > знижка 10%, при купівлі товарів на суму понад 2000 грн, протягом останніх 180 днів</span></span></p>

<p style="text-align: justify;">&nbsp;</p>

<p style= "text-align: justify;" ><span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;" ><strong>увага!</strong> накопичувальна знижка розраховується тільки з суми попередніх замовлень. У ній не враховується сума замовлення, який ви оформляєте в даний момент часу. Зверніть увагу, товар, куплений за акційною, зниженою ціною не включається в накопичувальні знижки. Для того що б знижка працювала коректно ви повинні бути зареєстрованим покупцем, покупки гостьового режиму не враховують знижку...</span></span></p>
    <div class="buttons">
      <div class="right"><a href="https://xn----utbcjbgv0e.com.ua / "class="button-cont-right" >продовжити<i class= "icon-circle-arrow-right" ></i></a></div>
    </div>
  </div><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>